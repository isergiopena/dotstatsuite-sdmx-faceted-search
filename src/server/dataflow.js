import R from 'ramda';
import { decode } from './utils';

const isPhrase = R.prop('phrase');
const phraseValue = ({ term }) => `"${term}"`;
const termValue = ({ term }) => `*${term}*`;
const searchValue = () => (field, search) => (isPhrase(search) ? phraseValue(search) : termValue(search));
const getFieldName = lang => (target, name) => (target ? (R.is(Function, target) ? target(lang) : target) : name); // eslint-disable-line

const isFacet = R.prop('ID');

const getFacetToField = R.compose(
  R.fromPairs,
  R.map(x => [x.name, x.field]),
  R.filter(isFacet),
);

const getNameToField = R.compose(
  R.fromPairs,
  R.map(x => [x.name, x.field]),
  R.reject(isFacet),
);

const localizeField = (lang, { schema }) => {
  const nameToField = getNameToField(schema);
  return name => {
    const getLocalizedFieldName = getFieldName(lang);
    return getLocalizedFieldName(nameToField[name], name);
  };
};

const getWeight = ({ schema }) => {
  const nameToField = R.compose(
    R.fromPairs,
    R.map(x => [x.name, x]),
    R.reject(isFacet),
  )(schema);
  return name => R.prop('weight', nameToField[name]);
};

const localizeFields = (lang, { schema }, fields) => {
  const nameToField = R.compose(
    R.fromPairs,
    R.map(x => [x.name, x.field]),
    R.reject(isFacet),
  )(schema);
  const getLocalizedFieldName = getFieldName(lang);
  return R.map(name => getLocalizedFieldName(nameToField[name], name), fields);
};

const getFieldToName = getLocalizedFieldName =>
  R.compose(
    R.fromPairs,
    R.map(({ field, name }) => [getLocalizedFieldName(field, name), name]),
  );

const outDataflowsMap = (lang, config) => {
  const { schema } = config;
  const getLocalizedFieldName = getFieldName(lang);
  const fieldToName = getFieldToName(getLocalizedFieldName)(schema);
  return R.map(
    R.compose(
      R.fromPairs,
      R.map(([field, value]) => [fieldToName[field] || field, value]),
      R.toPairs,
    ),
  );
};

const outHighlightingMap = (lang, config) => {
  const { schema, facets = [] } = config;
  const hEncodedFacets = R.reduce(
    (acc, f) => {
      acc[f.name] = f.encoded;
      return acc;
    },
    {},
    facets,
  );
  const getLocalizedFieldName = getFieldName(lang);
  const fieldToName = getFieldToName(getLocalizedFieldName)(schema);
  return highlighting => {
    if (!highlighting) return;
    return R.map(
      h =>
        R.reduce(
          (acc, [fieldName, value]) => {
            if (R.isEmpty(value)) return acc;
            const encodedName = fieldToName[fieldName];
            const name = hEncodedFacets[encodedName] ? decode(encodedName) : encodedName;
            acc[name] = value;
            return acc;
          },
          {},
          R.toPairs(h),
        ),
      highlighting,
    );
  };
};

const localizeHighlighting = (lang, { schema }) => {
  const getLocalizedFieldName = getFieldName(lang);
  const nameToField = getNameToField(schema);
  return highlighting => {
    if (!highlighting || !highlighting.fields || !highlighting.fields.length) return;
    return { fields: R.map(name => getLocalizedFieldName(nameToField[name]))(highlighting.fields) };
  };
};

const localizeFacets = (lang, { schema }, facets = []) => {
  const nameToField = getFacetToField(schema);
  const getLocalizedFieldName = getFieldName(lang);
  const getLocalizedFacet = facet => ({
    ...facet,
    field: getLocalizedFieldName(nameToField[facet.name], facet.name),
  });
  return R.map(getLocalizedFacet, facets);
};

export const localizeConfig = (requestedLang, { defaultLocale = 'en', dataflows: config }) => {
  const { outFields, searchFields, schema } = config;
  const lang = requestedLang || defaultLocale;
  return {
    lang,
    schema,
    searchValue: searchValue(schema),
    localizeField: localizeField(lang, config),
    getWeight: getWeight(config),
    outFields: localizeFields(lang, config, outFields),
    baseSearchFields: searchFields,
    searchFields: localizeFields(lang, config, searchFields),
    configFacets: localizeFacets(lang, config, config.facets),
    outDataflowsMapper: outDataflowsMap(lang, config),
    outHighlightingMapper: outHighlightingMap(lang, config),
    localizeHighlighting: localizeHighlighting(lang, config),
  };
};
