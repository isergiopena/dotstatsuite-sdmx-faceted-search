import EventEmitter from 'events'
import debug from '../debug'

import {
  map,
  toUpper,
  includes,
  isNil,
  reduce,
  toPairs,
  forEach,
  keys,
  compose,
  propEq,
  filter,
  pluck,
  identity,
  find,
} from 'ramda'
import { INT_EXT, TEXTFIELD_EXT, TEXT_LIST_EXT, STRING_EXT, STRING_LIST_EXT } from '../schema/ext'
import { FACET_TYPE, ATTR_TYPE, PROP_TYPE } from '../schema/types'
import { AND, OR } from '../schema/utils'
import { encode } from '../utils'

function BuilderError(input, message, code) {
  Error.captureStackTrace(this, BuilderError)
  this.message = message
  this.code = code
  this.input = input
  this.name = 'BuilderError'
}

BuilderError.prototype = Object.create(Error.prototype)
BuilderError.prototype.constructor = BuilderError

const getLocales = keys
const updateLocales = (res, locales) => {
  res.locales = reduce(
    (acc, locale) => {
      acc[locale] = true
      return acc
    },
    res.locales,
    locales,
  )
  return res
}

const TREE_SEP = '|'
const makeIdValue = (id, label) => `${label}#${id}#`

const makeOrder = annotations => {
  if (!annotations) return () => 0
  const order = find(propEq('type', 'ORDER'), annotations)
  if (!order) return () => 0
  return locale => (order.texts ? Number(order.texts[locale]) : Number(order.title))
}

export const getTreeValues = ({ isCategory } = {}) => children => {
  const getChildrenValue = (res, children, paths = {}, level = 0) =>
    reduce(
      (res, { id, name, children, hasData, annotations }) => {
        const makeLocaleOrder = makeOrder(annotations)
        const newPaths = reduce(
          (newPaths, [locale, label]) => {
            const value = makeIdValue(id, label)
            newPaths[locale] = paths[locale] ? `${paths[locale]}${TREE_SEP}${value}` : `|${value}`
            const levelValue = `${level}${newPaths[locale]}`
            if (!res[locale]) res[locale] = [{ levelValue, hasData, order: makeLocaleOrder(locale) }]
            else res[locale].push({ levelValue, hasData, order: makeLocaleOrder(locale) })
            return newPaths
          },
          {},
          toPairs(name),
        )
        return children ? getChildrenValue(res, children, newPaths, level + 1) : res
      },
      res,
      children,
    )

  return compose(
    reduce((res, [locale, value]) => {
      res[locale] = compose(
        map(x => [x.levelValue, x.order]),
        isCategory ? identity : filter(propEq('hasData', true)),
      )(value)
      return res
    }, {}),
    toPairs,
  )(getChildrenValue({}, children))
}

const CATEGORY_SCHEME_ORIGIN = 'category'
const CONCEPT_ORIGIN = 'concept'
const DATASOURCE_ORIGIN = 'datasource'

const STRING = 'string'
const LIST = 'list'
const TREE = 'tree'
const makeAttr = ext => (name, locale) => `${name}_${locale}_${ext}`
const makeProp = ext => name => `${name}_${ext}`
const makeFacetAttr = makeAttr(STRING_LIST_EXT)

const getSubType = type => ({ list: LIST, tree: TREE }[type])
const declareFacet = (res, { name, _name, encoded, id, type, origin }) =>
  (res.schema[name] = {
    type: FACET_TYPE,
    name,
    _name,
    encoded,
    id,
    // subType: getSubType(type),
    origin,
    ext: STRING_LIST_EXT,
    localized: true,
    search: false,
    op: AND,
    highlight: false,
  })

const declareFacetAttr = (res, { name }) =>
  (res.schema[`__${name}__`] = {
    type: ATTR_TYPE,
    name,
    ext: TEXT_LIST_EXT,
    localized: true,
    search: true,
    highlight: true,
  })

const declareAttr = (res, name, field) => (res.schema[field.name || name] = { ...field, name: field.name || name })
const declareProp = (res, name, field) => (res.schema[field.name || name] = { ...field, name: field.name || name })

// const makeI18nId = name => `sfs.facet.${name}`;
//
// const addTranslation = (res, name, locale, value) => {
//   if (!res.i18n[locale]) res.i18n[locale] = {};
//   const existingValue = res.i18n[locale][makeI18nId(name)];
//   if (existingValue && existingValue !== value) {
//     // Todo
//     console.error('TRANSLATION ALREADY EXISTS !!!');
//   }
//   res.i18n[locale][makeI18nId(name)] = value;
// };

// const registerI18n = (res, name, names) => {
//   forEach(([locale, value]) => addTranslation(res, name, locale, value), toPairs(names));
// };

const addOrders = (res, locale, valuesOrders) => {
  if (!res.orders[locale]) res.orders[locale] = {}
  for (const [value, order] of valuesOrders) {
    if (order) res.orders[locale][value] = order
  }
}

const addFacets = (res, { id, type, name }, values, origin) => {
  const data = {}

  forEach(([locale, value]) => (data[locale] = { name: value }), toPairs(name))

  forEach(([locale, valuesOrders]) => {
    if (data[locale]) data[locale].values = pluck(0, valuesOrders)
    else data[locale] = { name: id, values: pluck(0, valuesOrders) }
    addOrders(res, locale, valuesOrders)
  }, toPairs(values))

  forEach(([locale, { name, values }]) => {
    const hashedName = encode(name)
    res.dataflow[makeFacetAttr(hashedName, locale)] = values
    declareFacet(res, { name: hashedName, _name: name, encoded: true, id: hashedName, type, origin })

    res.dataflow[makeAttr(TEXT_LIST_EXT)(hashedName, locale)] = values
    declareFacetAttr(res, { name: hashedName })
  }, toPairs(data))
}

const extractId = ({ input, fields }, res) => {
  const name = 'id'
  const field = fields[name]
  if (!input.fields[name]) throw new BuilderError(input, `dataflow  has no ID: ${JSON.stringify(input)}`)
  if (res.dataflow[name]) throw new BuilderError(input, `dataflow '${input.fields[name]}' already has an ID`)
  res.dataflow[name] = input.fields[name]
  declareAttr(res, name, field)
}

const extractFacet = (res, parent) => input => {
  if (!input.id) throw new BuilderError(input, `dataflow '${parent.id}' has a dimension without an ID`)
  if (!input.children) return
  const values = getTreeValues()(input.children)
  addFacets(res, { id: input.id, name: input.name, type: TREE }, values, CONCEPT_ORIGIN)
}

const extractCategory = (res, { excludedCategorySchemeFacets }) => input => {
  if (!input.children) return
  const values = getTreeValues({ isCategory: true })(input.children)
  if (!includes(toUpper(input.id), excludedCategorySchemeFacets))
    addFacets(res, { id: input.id, name: input.name, type: TREE }, values, CATEGORY_SCHEME_ORIGIN)
}

const cast = (field, value) => {
  if (field.ext === INT_EXT) return Number(value)
  return value
}

const extractAttr = (res, fields) => ([name, values]) => {
  const field = fields[name]
  if (!field || field.type !== ATTR_TYPE) return
  const locales = getLocales(values)
  updateLocales(res, locales)
  forEach(
    ([locale, value]) => (res.dataflow[makeAttr(field.ext)(field.name || name, locale)] = cast(field, value)),
    toPairs(values),
  )
  declareAttr(res, name, field)
}

const makeNameStringAttr = (input, fields, res) => {
  const sname = 'sname'
  fields[sname] = {
    type: ATTR_TYPE,
    name: sname,
    id: 'SNAME',
    ext: STRING_EXT,
    localized: true,
  }
  extractAttr(res, fields)([sname, input.fields.name])
}

const makeDimensionNamesAttr = (input, fields, res) => {
  const name = 'dimensions'
  const field = fields[name]
  const data = {}

  for (const { name: names, valuesCount } of input.dimensions) {
    if (!(valuesCount > 1)) continue
    for (const [locale, name] of toPairs(names)) {
      if (data[locale]) data[locale].push(name)
      else data[locale] = [name]
    }
  }

  for (const { name: names } of input.timeDimensions) {
    for (const [locale, name] of toPairs(names)) {
      if (data[locale]) data[locale].push(name)
      else data[locale] = [name]
    }
  }

  for (const locale in data) {
    res.dataflow[makeAttr(TEXT_LIST_EXT)(name, locale)] = data[locale]
  }

  declareAttr(res, name, field)
}

const extractProp = (res, fields) => ([name, value]) => {
  if (isNil(value)) return
  const field = fields[name]
  if (!field || field.type !== PROP_TYPE) return
  res.dataflow[makeProp(field.ext)(field.name || name)] = value
  declareProp(res, name, field)
}

const makeDataSourceFacet = (input, res) => {
  const name = 'datasourceId'
  res.dataflow[makeProp(STRING_EXT)(name)] = input.fields.datasourceId
  res.dataflow[makeProp(TEXTFIELD_EXT)(name)] = input.fields.datasourceId
  res.schema[name] = {
    type: FACET_TYPE,
    name,
    _name: name,
    encoded: false,
    id: name.toUpperCase(),
    // subType: STRING,
    ext: STRING_EXT,
    localized: false,
    op: OR,
    out: true,
    search: false,
    origin: DATASOURCE_ORIGIN,
  }
  res.schema[`__${name}__`] = {
    type: ATTR_TYPE,
    name,
    ext: TEXTFIELD_EXT,
    localized: false,
    search: true,
  }
}

const extractFacets = ({ input }, res) => {
  makeDataSourceFacet(input, res)
  if (!input.dimensions) return
  forEach(extractFacet(res, input), input.dimensions)
}

const extractCategories = ({ input }, res, { excludedCategorySchemeFacets = [] } = {}) => {
  if (!input.categories) return
  forEach(
    extractCategory(res, { excludedCategorySchemeFacets: map(toUpper, excludedCategorySchemeFacets) }),
    input.categories,
  )
}

const extractAttrs = ({ input, fields }, res) => {
  makeDimensionNamesAttr(input, fields, res)
  forEach(extractAttr(res, fields), toPairs(input.fields))
  makeNameStringAttr(input, fields, res)
}

const extractProps = ({ input, fields }, res) => {
  forEach(extractProp(res, fields), toPairs(input.fields))
}

const transpile = (em, fields, options) =>
  function*({ dataflows, loading }) {
    for (const dataflow of dataflows) {
      const req = { fields, input: dataflow }
      const res = {
        meta: dataflow.meta,
        loading,
        dataflow: { type_s: 'dataflow' },
        schema: {},
        locales: {},
        orders: {},
      }
      try {
        extractAttrs(req, res)
        extractProps(req, res)
        extractId(req, res)
        extractFacets(req, res)
        extractCategories(req, res, options)
        em.emit('dataflow', res)
        yield res
      } catch (error) {
        debug.error(error)
        const message = `Cannot transpile dataflow: ${error.message}`
        em.emit('error', error, { ...dataflow.meta, message })
      }
    }
  }

const Builder = (fields, options) => {
  const em = new EventEmitter()
  const run = transpile(em, fields, options)
  run.on = (...params) => em.on(...params)
  return run
}

export default Builder
