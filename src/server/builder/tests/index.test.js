import Builder, { getTreeValues } from '..'
import { encode } from '../../utils'
import { INT_EXT, STRING_EXT, TEXTFIELD_EXT } from '../../schema/ext.js'
import { PROP_TYPE, ID_TYPE, ATTR_TYPE } from '../../schema/types.js'
import DataflowParser from '../../sdmx/dataflow'
import params from '../../init/params'

const ss = encode

const mockedDate = new Date(2017, 11, 10)
global.Date = class {
  constructor() {
    return mockedDate
  }
}
global.Date.now = () => mockedDate
const { fields } = params
const data = require('../../tests/mocks/sdmx/dataflow_sample_abs.js')

describe('Dataflow Builder', () => {
  it('should transpile', async done => {
    const builder = Builder(fields)
    const outSchema = {
      id: {
        name: 'id',
        type: 'ID',
        out: true,
        search: false,
      },
      indexationDate: {
        type: 'PROP',
        ext: 's',
        out: true,
        name: 'indexationDate',
      },
      name: {
        type: 'ATTR',
        ext: 't',
        search: true,
        out: true,
        highlight: true,
        localized: true,
        weight: 2,
        name: 'name',
      },
      sname: {
        ext: 's',
        id: 'SNAME',
        localized: true,
        name: 'sname',
        type: ATTR_TYPE,
      },
      description: {
        type: 'ATTR',
        ext: 't',
        search: true,
        out: true,
        highlight: true,
        localized: true,
        name: 'description',
      },
      dataflowId: { type: 'PROP', ext: 's', out: true, search: true, name: 'dataflowId', highlight: true },
      version: { type: 'PROP', ext: 's', out: true, search: true, name: 'version' },
      agencyId: { type: 'PROP', ext: 's', out: true, search: true, name: 'agencyId', highlight: true },
      datasourceId: {
        type: 'FACET',
        name: 'datasourceId',
        _name: 'datasourceId',
        encoded: false,
        id: 'DATASOURCEID',
        // subType: 'string',
        ext: 's',
        localized: false,
        op: 'OR',
        search: false,
        out: true,
        origin: 'datasource',
      },
      gorder: {
        type: PROP_TYPE,
        ext: INT_EXT,
        out: false,
        name: 'gorder',
      },
      lorder: {
        type: ATTR_TYPE,
        ext: INT_EXT,
        out: false,
        name: 'lorder',
        localized: true,
      },

      dimensions: {
        ext: 'txt',
        highlight: true,
        id: 'DIMENSIONS',
        localized: true,
        name: 'dimensions',
        out: true,
        search: true,
        // subType: "string",
        type: 'ATTR',
      },

      [ss('ABS 123(Top)ics وحدة')]: {
        type: 'FACET',
        name: ss('ABS 123(Top)ics وحدة'),
        _name: 'ABS 123(Top)ics وحدة',
        encoded: true,
        id: ss('ABS 123(Top)ics وحدة'),
        // subType: 'tree',
        ext: 'ss',
        localized: true,
        op: 'AND',
        search: false,
        origin: 'category',
        highlight: false,
      },
      [ss('Measure')]: {
        type: 'FACET',
        name: ss('Measure'),
        _name: 'Measure',
        encoded: true,
        id: ss('Measure'),
        // subType: 'tree',
        ext: 'ss',
        localized: true,
        op: 'AND',
        search: false,
        origin: 'concept',
        highlight: false,
      },
      [ss('Index')]: {
        type: 'FACET',
        id: ss('Index'),
        name: ss('Index'),
        _name: 'Index',
        encoded: true,
        search: false,
        // subType: 'tree',
        ext: 'ss',
        localized: true,
        op: 'AND',
        origin: 'concept',
        highlight: false,
      },
      [ss('Ajustment Type')]: {
        type: 'FACET',
        id: ss('Ajustment Type'),
        name: ss('Ajustment Type'),
        _name: 'Ajustment Type',
        encoded: true,
        // subType: 'tree',
        search: false,
        ext: 'ss',
        localized: true,
        op: 'AND',
        origin: 'concept',
        highlight: false,
      },
      [ss('Region')]: {
        type: 'FACET',
        name: ss('Region'),
        _name: 'Region',
        encoded: true,
        id: ss('Region'),
        // subType: 'tree',
        ext: 'ss',
        search: false,
        localized: true,
        op: 'AND',
        origin: 'concept',
        highlight: false,
      },
      [ss('Frequency')]: {
        type: 'FACET',
        name: ss('Frequency'),
        _name: 'Frequency',
        encoded: true,
        id: ss('Frequency'),
        // subType: 'tree',
        ext: 'ss',
        localized: true,
        search: false,
        op: 'AND',
        origin: 'concept',
        highlight: false,
      },
      __datasourceId__: {
        type: 'ATTR',
        name: 'datasourceId',
        ext: 't',
        localized: false,
        search: true,
      },

      [`__${ss('ABS 123(Top)ics وحدة')}__`]: {
        type: 'ATTR',
        name: ss('ABS 123(Top)ics وحدة'),
        ext: 'txt',
        localized: true,
        search: true,
        highlight: true,
      },
      [`__${ss('Measure')}__`]: {
        type: 'ATTR',
        name: ss('Measure'),
        ext: 'txt',
        localized: true,
        search: true,
        highlight: true,
      },
      [`__${ss('Index')}__`]: {
        type: 'ATTR',
        name: ss('Index'),
        search: true,
        ext: 'txt',
        localized: true,
        highlight: true,
      },
      [`__${ss('Ajustment Type')}__`]: {
        type: 'ATTR',
        name: ss('Ajustment Type'),
        search: true,
        ext: 'txt',
        localized: true,
        highlight: true,
      },
      [`__${ss('Region')}__`]: {
        type: 'ATTR',
        name: ss('Region'),
        ext: 'txt',
        search: true,
        localized: true,
        highlight: true,
      },
      [`__${ss('Frequency')}__`]: {
        type: 'ATTR',
        name: ss('Frequency'),
        ext: 'txt',
        localized: true,
        search: true,
        highlight: true,
      },
    }
    builder.on('error', e => done(e))
    builder.on('dataflow', ({ dataflow, schema }) => {
      expect(dataflow).toMatchSnapshot()
      expect(schema).toEqual(outSchema)
      expect(dataflow.dimensions_en_txt).toEqual(['Measure', 'Index', 'Ajustment Type', 'Region', 'Time period'])
      expect(dataflow.dimensions_fr_txt).toEqual(['Période de temps'])
      done()
    })

    const parser = DataflowParser(data, { datasourceId: 'oecd', dimensionValuesLimit: 1000 })
    const dataflow = parser.dataflows()[0]
    /* eslint-disable no-unused-vars */
    /* eslint-disable no-empty */
    for await (const d of builder({ dataflows: [dataflow], loading: { loadingId: 1 } })) {
    }
  })

  it('should get tree values', () => {
    const children = [
      {
        id: 1,
        name: { en: 1 },
        hasData: true,
        children: [
          {
            id: 11,
            name: { en: 11 },
            hasData: false,
            children: [{ id: 111, name: { en: 111 }, hasData: false, children: [] }],
          },
          {
            id: 12,
            name: { en: 12 },
            hasData: true,
            children: [{ id: 121, name: { en: 121 }, hasData: true, children: [] }],
          },
          {
            id: 13,
            name: { en: 13 },
            hasData: false,
            children: [{ id: 131, name: { en: 131 }, hasData: true, children: [] }],
          },
        ],
      },
      {
        id: 2,
        name: { en: 2 },
        hasData: false,
        children: [
          {
            id: 21,
            name: { en: 21 },
            hasData: true,
            children: [{ id: 211, name: { en: 211 }, hasData: false, children: [] }],
          },
          {
            id: 22,
            name: { en: 22 },
            hasData: false,
            children: [{ id: 221, name: { en: 221 }, hasData: true, children: [] }],
          },
          {
            id: 23,
            name: { en: 23 },
            hasData: false,
            children: [{ id: 231, name: { en: 231 }, hasData: false, children: [] }],
          },
        ],
      },
    ]
    const values = {
      en: [
        ['0|1#1#', 0],
        ['1|1#1#|12#12#', 0],
        ['2|1#1#|12#12#|121#121#', 0],
        ['2|1#1#|13#13#|131#131#', 0],
        ['1|2#2#|21#21#', 0],
        ['2|2#2#|22#22#|221#221#', 0],
      ],
    }
    const cvalues = {
      en: [
        ['0|1#1#', 0],
        ['1|1#1#|11#11#', 0],
        ['2|1#1#|11#11#|111#111#', 0],
        ['1|1#1#|12#12#', 0],
        ['2|1#1#|12#12#|121#121#', 0],
        ['1|1#1#|13#13#', 0],
        ['2|1#1#|13#13#|131#131#', 0],
        ['0|2#2#', 0],
        ['1|2#2#|21#21#', 0],
        ['2|2#2#|21#21#|211#211#', 0],
        ['1|2#2#|22#22#', 0],
        ['2|2#2#|22#22#|221#221#', 0],
        ['1|2#2#|23#23#', 0],
        ['2|2#2#|23#23#|231#231#', 0],
      ],
    }
    expect(getTreeValues({ isCategory: false })(children)).toEqual(values)
    expect(getTreeValues({ isCategory: true })(children)).toEqual(cvalues)
  })
})
