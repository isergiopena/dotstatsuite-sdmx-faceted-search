import { pick, keys } from 'ramda';
import Loader from '..';

jest.mock('request', () => ({ url }, cb) => {
  const categoryscheme = require('../../tests/mocks/sdmx/categoryscheme');
  const dataflow = require('../../tests/mocks/sdmx/dataflow');
  const dataflowWithoutCC = require('../../tests/mocks/sdmx/dataflow_without_cc');

  if (/categoryscheme/.test(url))
    return setTimeout(() => cb(null, { statusCode: 200 }, JSON.stringify(categoryscheme)), 0);

  // without CC and with data
  if (/dataflow\/OECD\/DF_AIR_EMISSIONS/.test(url))
    return setTimeout(() => cb(null, { statusCode: 200 }, JSON.stringify(dataflowWithoutCC('DF_AIR_EMISSIONS'))), 0);
  if (/data\/OECD,DF_AIR_EMISSIONS/.test(url))
    return setTimeout(
      () => cb(null, { statusCode: 200, headers: { 'content-range': 'values 0-999/10000' } }, JSON.stringify({})),
      0,
    );

  // without CC and without data
  if (/dataflow\/OECD\.CFE\/DF_DOMESTIC_TOURISM/.test(url))
    return setTimeout(() => cb(null, { statusCode: 200 }, JSON.stringify(dataflowWithoutCC('DF_DOMESTIC_TOURISM'))), 0);
  if (/data\/OECD\.CFE,DF_DOMESTIC_TOURISM/.test(url))
    return setTimeout(
      () => cb(null, { statusCode: 200, headers: { 'content-range': 'values 0-0/0' } }, JSON.stringify({})),
      0,
    );

  // without CC and without data + 404 SDMX/NSI convention for no data
  if (/dataflow\/OECD\.SDD\/NAMAIN_T0101_A/.test(url))
    return setTimeout(() => cb(null, { statusCode: 200 }, JSON.stringify(dataflowWithoutCC('NAMAIN_T0101_A'))), 0);
  if (/data\/OECD\.SDD,NAMAIN_T0101_A/.test(url))
    return setTimeout(
      () => cb(null, { statusCode: 404 }, 'No data found'), // yes, body is not in JSON in SDMX/NSI...
      0,
    );

  // external
  if (/ilo\.org\/sdmx-test/.test(url))
    return setTimeout(() => cb(null, { statusCode: 200 }, JSON.stringify(dataflow('DF_TOURISM_ENT_EMP', 'ILOCS1'))), 0);

  // with CC
  setTimeout(() => cb(null, { statusCode: 200 }, JSON.stringify(dataflow())), 0);
});

const tenants = {
  spaces: {
    test1: {
      url: 'https://www.oecd.org/sdmx/rest',
    },
    test2: {
      url: 'https://www.oecd2.org/sdmx/rest',
    },
  },
  datasources: {
    OECD: {
      dataSpaceId: 'test1',
      indexed: true,
      dataqueries: [
        {
          agencyId: 'OECD',
          categorySchemeId: 'OECDCS1',
          version: '1.0',
        },
        {
          agencyId: 'OECD',
          categorySchemeId: 'OECDCS1',
          version: '1.0',
        },
      ],
    },
    OECD2: {
      dataSpaceId: 'test2',
      indexed: false,
      dataqueries: [
        {
          agencyId: 'OECD',
          categorySchemeId: 'OECDCS1',
          version: '1.0',
        },
      ],
    },
  },
};

describe('Dataflow Loader', () => {
  it('should load', async done => {
    const loader = Loader({ dimensionValuesLimit: 1000 });
    let loaded = 0;
    let rejected = 0;
    let dsExpected = 0;
    let csExpected = 0;
    let errors = 0;

    loader.on('error', error => {
      if (error.code === 404) errors++;
      else done(error);
    });

    loader.on('datasource:loaded', loading => {
      const res = { datasourceId: keys(tenants.datasources)[0] };
      expect(pick(['datasourceId'], loading)).toEqual(res);
      dsExpected++;
    });

    loader.on('categoryscheme:loaded', loading => {
      const res = { baseUrl: tenants.spaces.test1.url, datasourceId: keys(tenants.datasources)[0] };
      expect(loading.url).toEqual(
        'https://www.oecd.org/sdmx/rest/categoryscheme/OECD/OECDCS1/1.0/?references=dataflow',
      );
      expect(pick(['baseUrl', 'datasourceId'], loading)).toEqual(res);
      csExpected++;
    });

    loader.on('dataflow:loaded', () => loaded++);
    loader.on('dataflow:rejected', () => rejected++);

    loader.on('done', data => {
      // external
      expect(rejected).toEqual(6);
      expect(errors).toEqual(4); // 404 SDMX/NSI convention for no data (rejected)
      expect(data.dataflows[0].categories[0].id).toEqual('OECDCS1');
      expect(data.dataflows.length).toEqual(2);
      expect(loaded).toEqual(240);
      expect(dsExpected).toEqual(1);
      expect(csExpected).toEqual(2);
      done();
    });

    /* eslint-disable no-unused-vars */
    /* eslint-disable no-empty */
    for await (const d of loader(tenants, 1)) {
    }
  }, 15000);
});
