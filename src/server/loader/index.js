import debug from '../debug'
import {
  assoc,
  prop,
  values,
  concat,
  find,
  propEq,
  path,
  filter,
  forEach,
  toPairs,
  map,
  compose,
  reduce,
  append,
  includes,
} from 'ramda'
import EventEmitter from 'events'
import request from 'request'
import { stripBorm } from './utils'
import DataflowParser from '../sdmx/dataflow'
import { ACCEPT_HEADER, LANGS, greaterThan, rlabel, parseDataRange } from '../sdmx/utils'
import { HTTPError } from '../utils/errors'

const getDataflowIds = dataflow => ({ id: dataflow.id, version: dataflow.version, agencyId: dataflow.agencyID })
const getDataflows = path(['data', 'dataflows'])

const hasExternalUrl = prop('isExternalReference')
const getExternalUrl = ({ isExternalReference, links = [] }) => {
  if (!isExternalReference) return
  const link = find(propEq('rel', 'external'), links)
  if (!link) return
  return link.href
}

const getInternalUrl = (dataflow, baseUrl) => {
  const { agencyId, id, version } = getDataflowIds(dataflow)
  return `${baseUrl}/dataflow/${agencyId}/${id}/${version}`
}

const makeDataflowUrl = getter => (dataflow, baseUrl) => {
  const url = getter
    ? getter(dataflow, baseUrl)
    : hasExternalUrl(dataflow)
    ? getExternalUrl(dataflow)
    : getInternalUrl(dataflow, baseUrl)
  return `${url}/?references=all&detail=referencepartial`
}

const makeDataflowDataUrl = (dataflow, baseUrl) => {
  const { agencyId, id, version } = getDataflowIds(dataflow)
  return `${baseUrl}/data/${agencyId},${id},${version}/all`
}

const makeCategorySchemeUrl = ({ baseUrl, agencyId, categorySchemeId, version }) =>
  `${baseUrl}/categoryscheme/${agencyId}/${categorySchemeId}/${version}/?references=dataflow`

const makeRequest = (url, ctx = {}, headers = {}) =>
  new Promise((resolve, reject) => {
    debug.trace(`loader ${ctx.retry ? 're-' : ''} loading "${url}"`)
    const t0 = new Date()
    request(
      { url, headers: { Accept: ACCEPT_HEADER.structure, 'Accept-Language': LANGS, ...headers } },
      (err, res, body) => {
        if (!err && includes(res.statusCode, [200, 206])) {
          // 206 for range request
          try {
            debug.info(`loader:: "${url}" loaded in ${new Date() - t0}ms`)
            resolve({ data: JSON.parse(stripBorm(body)), ctx: { ...ctx, t0, url }, headers: res.headers })
          } catch (err) {
            reject(err)
          }
        } else reject(err || new HTTPError(res.statusCode, `Cannot load '${url}'`))
      },
    )
  })

const Loader = ({ dimensionValuesLimit } = {}) => {
  const em = new EventEmitter()

  const loadDataflowHasData = (dataflow, loading) => {
    const url = makeDataflowDataUrl(dataflow, loading.baseUrl)
    return makeRequest(url, {}, { ...loading.headers, Accept: ACCEPT_HEADER.data, Range: 'values=0-0' })
      .then(res => {
        const { total } = parseDataRange(res)
        return total // null, undefined, false, 0
      })
      .catch(error => {
        em.emit('error', error, loading)
        return false
      })
  }

  const loadDataflow = (dataflowRef, loadingOptions) => {
    const logger = 'loader.loadDataflow'
    const artefact = rlabel({ ...loadingOptions, ...dataflowRef, resourceType: 'dataflow' })
    const loading = { ...loadingOptions, ...dataflowRef, artefact, logger, url }

    // url is internal or external depending on the dataflowRef
    const url = makeDataflowUrl()(dataflowRef, loading.baseUrl)
    loading.url = url
    const urls = [url]
    const isExternalDataflow = hasExternalUrl(dataflowRef)
    if (isExternalDataflow) {
      // internal categorisation of external dataflowRef is required
      const internalUrl = makeDataflowUrl(getInternalUrl)(dataflowRef, loading.baseUrl)
      urls.push(internalUrl)
    }

    return Promise.all(map(url => makeRequest(url, {}, loading.headers), urls))
      .then(async ([{ data, ctx }, internal]) => {
        const builderOptions = isExternalDataflow ? assoc('externalUrl', url, loading) : loading
        const parser = DataflowParser(data, { ...builderOptions, dimensionValuesLimit })
        const dataflows = await reduce(
          (promise, dataflow) =>
            promise.then(async acc => {
              dataflow.meta = loading
              if (isExternalDataflow) {
                const internalParser = DataflowParser(internal.data, { ...loading, dimensionValuesLimit })
                // sdmx is about the question, we request a single dataflow
                dataflow['categories'] = path([0, 'categories'], internalParser.dataflows())
              }
              if (dataflow.hasEmptyConstraints) {
                const message = `Dataflow has empty constraints`
                em.emit('dataflow:rejected', { dataflow, delay: new Date() - ctx.t0, loading: { ...loading, message } })
                return acc
              }
              if (!dataflow.hasNoContraints) return append(dataflow, acc)
              if (await loadDataflowHasData(dataflowRef, loading)) return append(dataflow, acc)
              const message = `Dataflow has no constraints`
              em.emit('dataflow:rejected', { dataflow, delay: new Date() - ctx.t0, loading: { ...loading, message } })
              return acc
            }),
          Promise.resolve([]),
          parser.dataflows(),
        )
        forEach(dataflow => em.emit('dataflow:loaded', { dataflow, loading, delay: new Date() - ctx.t0 }), dataflows)
        return dataflows
      })
      .catch(error => {
        const message = `Cannot load ${url}`
        em.emit('error', error, { ...loading, message })
        return []
      })
  }

  const loadDataflowsFromQuery = (ref, loadingOptions) => {
    const loading = { ...ref, ...loadingOptions }
    const url = makeCategorySchemeUrl(loading)
    loading.url = url
    const artefact = rlabel({ ...loading, id: loading.categorySchemeId, resourceType: 'categoryscheme' })
    em.emit('categoryscheme:loading', loading)
    return makeRequest(url, {}, loading.headers)
      .then(({ data }) => {
        const dataflowsRefs = getDataflows(data)
        if (!dataflowsRefs) return []
        em.emit('categoryscheme:loaded', loading)
        return reduce(
          (promise, dataflowRef) =>
            promise.then(async acc => concat(acc, await loadDataflow(dataflowRef, loadingOptions))),
          Promise.resolve([]),
          dataflowsRefs,
        )
      })
      .catch(error => {
        const message = `Cannot load ${url}`
        em.emit('error', error, { ...loading, message, logger: 'loader.loadDataflowsFromQuery', artefact })
        return []
      })
  }

  const loadDataflowsFromDataSource = async ({
    dataSpaceId,
    datasourceId,
    baseUrl,
    loadingId,
    queries = [],
    headers = {},
  }) => {
    const t0 = new Date()
    em.emit('datasource:loading', { datasourceId, loadingId, dataSpaceId })
    const allDataflows = await reduce(
      (promise, query) =>
        promise.then(async acc =>
          concat(acc, await loadDataflowsFromQuery(query, { dataSpaceId, datasourceId, baseUrl, loadingId, headers })),
        ),
      Promise.resolve([]),
      queries,
    )
      .then(data => {
        em.emit('datasource:loaded', { datasourceId, loadingId })
        return data
      })
      .catch(error => {
        debug.error(error) // eslint-disable-line
        em.emit('error', new Error(`Loader: cannot load Datasource: ${datasourceId}`), { datasourceId, loadingId })
        return []
      })

    const hdataflows = reduce(
      (acc, dataflow) => {
        if (!acc[dataflow.fields.id]) acc[dataflow.fields.id] = dataflow
        else {
          const existingDataflow = acc[dataflow.fields.id]
          if (greaterThan(dataflow.fields.version, existingDataflow.fields.version)) acc[dataflow.fields.id] = dataflow
        }
        return acc
      },
      {},
      allDataflows,
    )
    const dataflows = values(hdataflows)
    em.emit('done', {
      dataflows,
      loaded: allDataflows.length,
      loading: { datasourceId, loadingId },
      delay: new Date() - t0,
    })
    return { dataflows, loading: { datasourceId, loadingId }, delay: new Date() - t0 }
  }

  const activeDatasources = filter(prop('indexed'))

  async function* loader(tenant, { loadingId, userEmail }) {
    const loadings = compose(
      // map(([datasourceId, { dataqueries, headers }]) => {
      map(([datasourceId, ds]) => {
        const baseUrl = tenant.spaces[ds.dataSpaceId]?.url
        return {
          datasourceId,
          baseUrl,
          loadingId,
          queries: ds.dataqueries,
          headers: ds.headers,
          userEmail,
          dataSpaceId: ds.dataSpaceId,
          tenantId: tenant.id,
        }
      }),
      toPairs,
      activeDatasources,
    )(tenant.datasources || [])

    for await (const loading of loadings) {
      yield await loadDataflowsFromDataSource(loading)
    }
  }

  async function* loadOne(tenant, datasource, { loadingId, agencyId, dataflowId, version, userEmail, ...others }) {
    const t0 = new Date()
    const { headers } = datasource
    const space = tenant.spaces[datasource.dataSpaceId]
    if (!space) throw new Error(`Wrong space for datasource ${datasource.id}`)
    if (!space.url) throw new Error(`Wrong URL for space ${datasource.dataSpaceId}`)
    const datasourceId = datasource.id
    const dataflow = { id: dataflowId, version, agencyID: agencyId, ...others }
    const loading = {
      dataSpaceId: datasource.dataSpaceId,
      datasourceId,
      baseUrl: space.url,
      loadingId,
      headers,
      userEmail,
    }
    em.emit('datasource:loading', loading)
    const dataflows = await loadDataflow(dataflow, loading)
    em.emit('done', {
      dataflows,
      loaded: dataflows.length,
      loading: { datasourceId, loadingId },
      delay: new Date() - t0,
    })
    yield { dataflows, loading, delay: new Date() - t0 }
  }

  loader.loadAll = loader
  loader.loadOne = loadOne
  loader.on = (...params) => em.on(...params)
  return loader
}

export default Loader
