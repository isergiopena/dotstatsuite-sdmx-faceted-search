import { keys } from 'ramda'
import Report from '../index'
import { START } from '../process'
import {
  DATAFLOW_INDEXED,
  DATAFLOWS_LOADED,
  DATAFLOW_LOADED,
  DATAFLOW_REJECTED,
  START_LOADING_DATASOURCE,
} from '../loadings'

describe('Reporter', () => {
  it('should start report', () => {
    const report = Report()
    report({ type: START })
    const r = report()
    expect(r.process.startTime).toBeDefined()
  })

  it('should report', () => {
    const report = Report()
    const datasourceId = datasourceId
    const loadingId = 1
    const loading = { loadingId, datasourceId }
    report({ type: START })
    report({ type: START_LOADING_DATASOURCE, loading })
    let r = report()
    expect(keys(r.loadings).length).toEqual(1)

    report({ type: DATAFLOWS_LOADED, dataflows: [1, 2, 3], loading })
    r = report()
    expect(r.loadings[loadingId][datasourceId].dataflows.selected).toEqual(3)

    report({ type: DATAFLOW_LOADED, loading, delay: 55 })
    r = report()
    expect(r.loadings[loadingId][datasourceId].dataflows.loaded).toEqual(1)
    expect(r.loadings[loadingId][datasourceId].stats.min).toEqual(55)
    expect(r.loadings[loadingId][datasourceId].stats.max).toEqual(55)

    report({ type: DATAFLOW_REJECTED, loading })
    r = report()
    expect(r.loadings[loadingId][datasourceId].dataflows.rejected).toEqual(1)

    report({ type: DATAFLOW_INDEXED, loading })
    r = report()
    expect(r.loadings[loadingId][datasourceId].dataflows.indexed).toEqual(1)
  })
})
