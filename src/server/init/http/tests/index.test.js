import axios from 'axios';
import initHttp from '..';
import { initConfig } from '../../../tests/utils';

let CTX;

describe('Http', function() {
  beforeAll(() =>
    initConfig()
      .then(ctx => ctx({ ...ctx, services: {} }))
      .then(initHttp)
      .then(ctx => (CTX = ctx)),
  );
  afterAll(() => CTX().http.close());

  it('should ping', async () => {
    const url = `${CTX().http.url}/ping`;
    const { data } = await axios({ method: 'GET', url });
    expect(data.ping).toEqual('pong');
  });
});
