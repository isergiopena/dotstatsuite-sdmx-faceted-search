import debug from '../debug'
import queue from 'queue'

const init = async ctx => {
  const { models, report } = ctx()
  var q = queue({ concurrency: 1, autostart: true })
  report.listen((state, action) => q.push(() => models.jobs.manageEvent(action, state)))
  debug.info('logger is ready')
  return ctx
}

export default init
