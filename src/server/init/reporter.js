import debug from '../debug';
import Report from '../reporter';
import { START } from '../reporter/process';

const init = async ctx => {
  const report = Report();
  report({ type: START });
  debug.info('reporter on site');
  return ctx({ report });
};

export default init;
