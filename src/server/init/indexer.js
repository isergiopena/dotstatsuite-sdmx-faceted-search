import debug from '../debug'
import { compose } from 'ramda'
import Loader from '../loader'
import Builder from '../builder'
import { ERROR, WARNING } from '../reporter/errors'
import { doSearch } from '../search'
import { rlabel } from '../sdmx/utils'
import {
  CATEGORY_SCHEME_LOADED,
  DATAFLOW_TRANSPILED,
  DATAFLOW_INDEXED,
  DATAFLOWS_LOADED,
  DATAFLOWS_DELETED,
  DATAFLOW_LOADED,
  DATAFLOW_REJECTED,
  DATAFLOW_DELETED,
  START_LOADING_DATASOURCE,
  START_INDEXING,
  END_INDEXING,
} from '../reporter/loadings'

export default async ctx => {
  const {
    config: { excludedCategorySchemeFacets, fields, dimensionValuesLimit },
    configManager,
    solr,
    report,
  } = ctx()
  const loader = Loader({ dimensionValuesLimit })
  const builder = Builder(fields, { excludedCategorySchemeFacets })

  debug.info('ready to index SDMX')

  loader.on('datasource:loading', loading => report({ type: START_LOADING_DATASOURCE, loading }))
  loader.on('datasource:loading', loading =>
    debug.info(`indexer:: start loading datasource: '${loading.datasourceId}'`),
  )
  loader.on('datasource:loaded', loading => debug.info(`datasource: '${loading.datasourceId}' loaded`))

  loader.on('categoryscheme:loading', loading =>
    debug.info(`indexer:: start loading categoryscheme '${loading.categorySchemeId}'`),
  )
  loader.on('categoryscheme:loaded', loading =>
    debug.info(`indexer:: categoryscheme '${loading.categorySchemeId}' loaded`),
  )
  loader.on('categoryscheme:loaded', loading => report({ type: CATEGORY_SCHEME_LOADED, loading }))

  loader.on('error', error => {
    debug.error(error)
  })
  loader.on('error', (error, loading) => report({ type: ERROR, error, loading }))

  loader.on('dataflow:loaded', ({ loading, delay }) => report({ type: DATAFLOW_LOADED, loading, delay }))
  loader.on('dataflow:rejected', ({ loading, delay }) => report({ type: DATAFLOW_REJECTED, loading, delay }))

  loader.on('done', output =>
    debug.info(`indexer:: ${output.loaded} dataflows found, ${output.dataflows.length} selected`),
  )
  loader.on('done', ({ dataflows, loaded, loading }) => report({ type: DATAFLOWS_LOADED, dataflows, loaded, loading }))

  builder.on('error', error => debug.error(error.toString()))
  builder.on('error', (error, loading) => report({ type: ERROR, error, loading }))

  builder.on('dataflow', output => report({ type: DATAFLOW_TRANSPILED, ...output }))

  async function* loadManyStep(tenant, options) {
    for await (const datasourceDataflows of loader.loadAll(tenant, options)) {
      yield datasourceDataflows
    }
  }

  async function* loadOneStep(tenant, datasource, options) {
    for await (const datasourceDataflows of loader.loadOne(tenant, datasource, options)) {
      yield datasourceDataflows
    }
  }

  async function* buildStep(datasourceDataflows) {
    for await (const dataflows of datasourceDataflows) {
      for await (const builtDataflow of builder(dataflows)) {
        yield builtDataflow
      }
    }
  }

  const finalizeStep = tenant =>
    async function* finalize(builtDataflows) {
      const solrClient = solr.getClient(tenant)
      for await (const { meta, dataflow, locales, schema, loading, orders } of builtDataflows) {
        try {
          await solrClient.add([dataflow])
          await Promise.all([
            configManager.updateLocales(tenant, locales),
            configManager.updateSchema(tenant, schema),
            configManager.updateOrders(tenant, orders),
          ])
          report({ type: DATAFLOW_INDEXED, loading: meta, dataflow })
          yield dataflow
        } catch (error) {
          debug.error(error)
          const message = `Cannot index dataflow: ${error.message}`
          report({ type: ERROR, error, loading: { ...meta, message } })
        }
      }
    }

  async function loadOne(tenant, datasource, options = {}) {
    const loadingId = new Date().getTime()
    const loading = { ...options, loadingId }
    try {
      report({ type: START_INDEXING, loading: { ...loading, tenant, action: 'indexOne' } })
      const process = compose(finalizeStep(tenant), buildStep, loadOneStep)
      for await (const dataflow of process(tenant, datasource, loading)) {
        debug.info(`indexer:: dataflow ${dataflow.id} indexed`)
      }
    } catch (error) {
      const message = `Cannot index dataflow: ${error.message}`
      const logger = 'indexer.loadOne'
      const artefact = rlabel({ ...loading, ...options, resourceType: 'dataflow' })
      report({ type: ERROR, error, loading: { ...loading, artefact, logger, message } })
      throw error
    } finally {
      report({ type: END_INDEXING, loading: { loadingId } })
    }
  }

  async function* indexer(tenant, options = {}) {
    const loadingId = new Date().getTime()
    yield loadingId
    const loading = { ...options, loadingId }
    try {
      report({ type: START_INDEXING, loading: { ...loading, tenant, action: 'indexAll' } })
      const process = compose(finalizeStep(tenant), buildStep, loadManyStep)
      for await (const dataflow of process(tenant, loading)) {
        debug.info(`indexer:: dataflow ${dataflow.id} indexed`)
      }
    } catch (error) {
      const logger = 'indexer.loadAll'
      const message = `Cannot index dataflows: ${error.message}`
      report({ type: ERROR, error, loading: { ...loading, logger, message } })
      throw error
    } finally {
      report({ type: END_INDEXING, loading: { loadingId } })
    }
  }

  async function deleteAll(tenant, options) {
    debug.info(`deleting all data for member ${tenant.id}`)
    const loadingId = new Date().getTime()
    const loading = { ...options, loadingId }
    try {
      report({ type: START_INDEXING, loading: { ...loading, tenant, action: 'deleteAll' } })
      const solrClient = solr.getClient(tenant)
      await solrClient.deleteAll()
      const message = `All dataflows deleted for organisation ${tenant.id}`
      report({ type: DATAFLOWS_DELETED, loading: { ...loading, message } })
    } catch (error) {
      const message = `Cannot delete all dataflows: ${error.message}`
      const logger = 'indexer.deleteAll'
      report({ type: ERROR, error, loading: { ...loading, logger, message } })
      throw error
    } finally {
      report({ type: END_INDEXING, loading: { loadingId } })
    }
  }

  async function deleteOne(tenant, dataSpaceId, datasources, agencyId, id, version, options) {
    const loadingId = new Date().getTime()
    const loading = { ...options, dataSpaceId, loadingId }
    try {
      report({ type: START_INDEXING, loading: { ...loading, tenant, action: 'deleteOne' } })
      const solrClient = solr.getClient(tenant)
      const solrConfig = await configManager.getUpdatedConfig(tenant)
      const matches = []
      for (const datasourceId of datasources) {
        const query = `datasourceId:"${datasourceId}" dataflowId:"${id}" agencyId:"${agencyId}" version:"${version}"`
        const res = await doSearch(null, query, null, null, solrClient, solrConfig)
        if (!res.numFound) {
          const message = `Cannot find dataflow ${datasourceId}:${id}:${agencyId}:${version} for organisation ${tenant.id}`
          const logger = 'indexer.deleteOne'
          const artefact = rlabel({ ...loading, agencyId, id, version, resourceType: 'dataflow' })
          report({ type: WARNING, loading: { ...loading, logger, artefact, message } })
        } else {
          await solrClient.deleteOne(res.dataflows[0].id)
          matches.push(datasourceId)
          const message = `dataflow ${datasourceId}:${id}:${agencyId}:${version} deleted for organisation ${tenant.id}`
          const artefact = rlabel({ ...loading, agencyId, id, version, resourceType: 'dataflow' })
          report({ type: DATAFLOW_DELETED, loading: { ...loading, artefact, message } })
        }
      }
      return matches
    } catch (error) {
      const message = `Cannot delete dataflow: ${error.message}`
      const logger = 'indexer.deleteOne'
      const artefact = rlabel({ agencyId, id, version, resourceType: 'dataflow' })
      report({ type: ERROR, error, loading: { ...loading, artefact, logger, message } })
      throw error
    } finally {
      report({ type: END_INDEXING, loading: { loadingId } })
    }
  }

  indexer.loadAll = indexer
  indexer.loadOne = loadOne
  indexer.deleteOne = deleteOne
  indexer.deleteAll = deleteAll

  return ctx({ indexer })
}
