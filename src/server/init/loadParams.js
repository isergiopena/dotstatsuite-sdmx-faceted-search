import { dissoc } from 'ramda'
import debug from '../debug'
import params from './params'

export default async ctx => {
  const { solr, configProvider, configManager } = ctx()
  const activeMembers = await solr.getActiveMembers()
  const gparams = await configProvider.getGlobalConfig({ default: null })
  if (!gparams) debug.warn('Cannot get SFS global config!')

  const fields = { ...(params?.fields || {}) }
  for (const fieldId in gparams.fields || {}) {
    if (fields[fieldId]) fields[fieldId] = { ...fields[fieldId], ...gparams.fields[fieldId] }
    else fields[fieldId] = { ...gparams.fields[fieldId] }
  }

  const newParams = { ...params, ...dissoc('fields', gparams), fields }

  for (const member of activeMembers) {
    await configManager.updateSchema({ id: member }, fields)
  }
  debug.info('Global params loaded and schema updated')
  return ctx({ config: { ...ctx().config, ...newParams } })
}
