import debug from '../debug'
import ConfigProvider from '../configProvider'

require('../env')

export default async ctx => {
  const server = { host: process.env.SERVER_HOST || '0.0.0.0', port: Number(process.env.SERVER_PORT) || 80 }

  const config = {
    sfsId: process.env.SFS_ID || 'oecd',
    configUrl: process.env.CONFIG_URL,
    defaultTenant: process.env.DEFAULT_TENANT,
    gitHash: process.env.GIT_HASH || 'None',
    env: process.env.NODE_ENV,
    apiKey: process.env.API_KEY || 'secret',
    server,
    mongo: {
      url: process.env.MONGODB_URL || 'mongodb://localhost:27017',
      dbName: process.env.MONGODB_DATABASE || 'sfs',
    },
    solr: {
      host: process.env.SOLR_HOST || '0.0.0.0',
      port: process.env.SOLR_PORT || 8983,
      logLevel: 0,
    },
  }

  const configProvider = ConfigProvider(config)

  debug.info(`running "${config.env}" env`)
  return ctx({ startTime: new Date(), config, configProvider })
}
