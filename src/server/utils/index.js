import { propEq, compose, trim, reduce, includes } from 'ramda';
import bs52 from 'bs52';

export const encode = compose(x => bs52.encode(x), trim);
export const decode = x => bs52.decode(x);
export const isTestEnv = propEq('env', 'test');

export const filterMemberCollections = (members, collections) => {
  const memberList = Object.keys(members);
  return reduce(
    (acc, col) => {
      if (includes(col, memberList)) return { ...acc, [col]: collections[col] };
      return acc;
    },
    {},
    Object.keys(collections),
  );
};
