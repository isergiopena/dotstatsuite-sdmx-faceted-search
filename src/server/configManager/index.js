import { toPairs, keys, dissoc } from 'ramda'
import debug from '../debug'
import Schema from '../schema'

const NAME = 'configs'
const schemaKey = id => `schema:${id}`
const ordersKey = (id, locale) => `orders:${id}:${locale}`
const localesKey = id => `locales:${id}`
const encodeKeys = data => {
  const res = {}
  for (const [key, value] of toPairs(data)) {
    const newKey = key.replace(/\./g, '~~p').replace(/\$/g, '~~d')
    res[newKey] = value
  }
  return res
}

const decodeKeys = data => {
  const res = {}
  for (const [key, value] of toPairs(data)) {
    if (key === '_id') continue
    const newKey = key.replace(/~~p/g, '.').replace(/~~g/g, '$')
    res[newKey] = value
  }
  return res
}

export default ctx => {
  const { mongo } = ctx()
  const db = mongo.database
  const collection = db.collection(NAME)

  const Model = {
    getLocales: ({ id: memberId }) => {
      return collection.findOne({ _id: localesKey(memberId) }).then(x => (x ? dissoc('_id', x) : {}))
    },

    updateLocales: ({ id: memberId }, locales) => {
      debug.info(`configManager:: update locales '${memberId}'`)
      return collection.updateOne({ _id: localesKey(memberId) }, { $set: locales }, { upsert: true })
    },

    getSchema: ({ id: memberId }) => {
      return collection.findOne({ _id: schemaKey(memberId) }).then(x => (x ? dissoc('_id', x) : undefined))
    },

    updateSchema: ({ id: memberId }, schema) => {
      debug.info(`configManager:: update schema '${memberId}'`)
      return collection.updateOne({ _id: schemaKey(memberId) }, { $set: schema }, { upsert: true })
    },

    reset: ({ id: memberId }) => {
      return collection.deleteMany({ _id: { $in: [schemaKey(memberId), localesKey(memberId)] } })
    },

    getUpdatedConfig: async member => {
      if (!member) throw new Error('Member is required!')
      const { config } = ctx()
      const [locales, schema] = await Promise.all([Model.getLocales(member), Model.getSchema(member)])
      return {
        ...config,
        dataflows: {
          ...config.dataflows,
          locales: keys(locales),
          ...Schema(schema),
        },
      }
    },

    getOrders: ({ id: memberId }) => locale => {
      return collection.findOne({ _id: ordersKey(memberId, locale) }).then(decodeKeys)
    },

    updateOrders: async ({ id: memberId }, orders) => {
      debug.info(`configManager:: update orders '${memberId}'`)
      for (const [locale, items] of toPairs(orders)) {
        if (!Object.keys(items).length) continue
        await collection.updateOne({ _id: ordersKey(memberId, locale) }, { $set: encodeKeys(items) }, { upsert: true })
      }
    },
  }

  return Model
}
