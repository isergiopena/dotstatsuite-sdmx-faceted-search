import { doSearch } from '../../search'

const NAME = 'search'

const getSortParam = (sort, sortMode) => {
  const modes = {
    name: 'sname asc, indexationDate desc',
    score: 'score desc',
    date: 'indexationDate desc',
  }
  return modes[sortMode] || sort
}

export const service = {
  post({ facets, search = '', highlighting, lang, start, rows, sort, sortMode, fl, minCount }) {
    const { solrClient, solrConfig, facetOrders } = this.locals
    return doSearch(
      facets,
      search,
      highlighting,
      lang,
      solrClient,
      solrConfig,
      facetOrders,
      start,
      rows,
      getSortParam(sort, sortMode),
      fl,
      minCount,
    )
  },

  get({ q = '', lang, start, rows, sort, sortMode }) {
    const { solrClient, solrConfig, facetOrders } = this.locals
    return doSearch(null, q, null, lang, solrClient, solrConfig, facetOrders, start, rows, getSortParam(sort, sortMode))
  },
}

export default evtx => evtx.use(NAME, service)
