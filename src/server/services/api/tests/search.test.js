import evtX from 'evtx'
import { dissoc, map, prop } from 'ramda'
import initSolr from '../../../solr'
import initMongo from '../../../init/mongo'
import { initConfig, TENANT } from '../../../tests/utils'
import initSearchService from '../search'
import initConfigService from '../config'
import { checkTenant, getSolrClient, getSolrConfig } from '../../utils'

const data = [
  {
    id: 'IRS',
    datasourceId_ss: ['SOURCE1'],
    type_s: 'dataflow',
    agency_s: 'ECB',
    version_s: '1.0',
    name_en_t: 'Interest rate statistics',
    name_cz_t: 'CZ Interest rate statistics',
    cat_ss: ['0/C1/', '1/C1/C11/'],
    frequency_ss: ['Daily (D)', 'Monthly (M)', 'Yearly (Y)'],
    reference_area_cz_ss: ['Austria (AT)'],
    toto_t: 'toto:name',
    interest_rate_type_ss: [
      'Bank interest rates (B)',
      'Long-term interest rate for convergence purposes (L)',
      'Money market interest rates (M)',
    ],
  },
  {
    id: 'ABC',
    datasourceId_ss: ['SOURCE1'],
    type_s: 'dataflow',
    agency_s: 'Agency1',
    version_s: '1.0',
    name_en_t: 'EN Name',
    name_cz_t: 'CZ Name1',
    cat_ss: ['0/C1/', '1/C1/C11/', '2/C1/C11/C111/'],
    frequency_ss: ['Daily (D)', 'Monthly (M)'],
    reference_area_cz_ss: ['Austria (AT)', 'Belgium (BE)'],
  },
  {
    id: 'CDE',
    datasourceId_ss: ['SOURCE2'],
    type_s: 'dataflow',
    agency_s: 'Agency2',
    version_s: '1.0',
    name_en_t: 'EN Name2',
    name_cz_t: 'CZ Name2',
    cat_ss: ['0/C1/', '1/C1/C12/', '0/C2/'],
    reference_area_cz_ss: ['Austria (AT)', 'Belgium (BE)', 'Bulgaria (BG)', 'Cyprus (CY)', 'Germany (DE)'],
    frequency_ss: ['Yearly (Y)'],
    boost_t: 'blah blah interest blah',
    interest_rate_type_ss: ['Bank interest rates (B)', 'Money market interest rates (M)'],
  },
]

const config = {
  dataflows: {
    outFields: ['id', 'agency', 'version', 'name'],
    searchFields: ['id', 'agency', 'name'],
    schema: [
      {
        name: 'name',
        field: lang => `name_${lang}_t`,
      },
      {
        name: 'lorder',
        field: lang => `lorder_${lang}_i`,
      },
      {
        name: 'boost',
        field: `boost_t`,
        weight: 2,
      },
      {
        name: 'toto',
        field: `toto_t`,
      },
      {
        name: 'agency',
        field: 'agency_s',
      },
      {
        name: 'version',
        field: 'version_s',
      },
      {
        name: 'reference_area',
        field: lang => `reference_area_${lang}_ss`,
        ID: 'REFERENCE_AREA',
      },
      {
        name: 'interest_rate_type',
        field: 'interest_rate_type_ss',
      },
      {
        name: 'frequency',
        field: 'frequency_ss',
        ID: 'FREQUENCY',
      },
      {
        name: 'cat',
        field: 'cat_ss',
      },
      {
        name: 'datasourceId',
        field: 'datasourceId_ss',
        ID: 'DATASOURCEID',
      },
    ],
    facets: [
      {
        name: 'datasourceId',
        type: 'list',
        op: 'OR',
      },

      {
        name: 'frequency',
        type: 'list',
        op: 'OR',
      },
      {
        name: 'reference_area',
        type: 'list',
        op: 'OR',
      },
    ],
  },
}

const lang = 'cz'
let CTX

const localConfig = jest.fn()
const initConfigManager = ctx => {
  const configManager = { getUpdatedConfig: async () => localConfig() }
  return ctx({ configManager })
}

beforeAll(async () => {
  CTX = await initConfig()
    .then(initMongo)
    .then(initConfigManager)
    .then(initSolr)
  const solrClient = CTX().solr.getClient(TENANT)
  await solrClient.deleteAll()
  await solrClient.add(data)
})

const tearContext = async () => {
  try {
    await CTX().mongo.dropDatabase()
    const solrClient = CTX().solr.getClient(TENANT)
    await solrClient.deleteAll()
    await CTX().mongo.close()
  } catch (err) {
    console.error(err) // eslint-disable-line
    throw err
  }
}

afterAll(tearContext)

const runService = config => {
  localConfig.mockReturnValue(config)
  const api = evtX(CTX({ config }))
    .configure(initSearchService)
    .configure(initConfigService)
    .before(checkTenant, getSolrConfig, getSolrClient)
  return CTX({ api })
}

describe('Solr Search', () => {
  describe('Text search', () => {
    it('should get all data', async () => {
      const input = { lang, search: '' }
      const ctx = runService(config)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(dataflows.length).toBe(data.length)
    })

    it('should get all data without search param', async () => {
      const input = { lang }
      const ctx = runService(config)
      await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
    })

    it('should filter on name_t with on term', async () => {
      const input = { lang, search: 'name' }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['name'],
          outFields: ['name'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(map(dissoc('score'), dataflows)).toEqual([{ name: 'CZ Name1' }, { name: 'CZ Name2' }])
    })

    it('should filter on name_t with 2 terms', async () => {
      const input = { lang, search: 'cz name' }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['name'],
          outFields: ['name'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(map(dissoc('score'), dataflows)).toEqual([{ name: 'CZ Name1' }, { name: 'CZ Name2' }])
    })

    it('should filter on name_t with one phrase double quoted', async () => {
      const input = { lang: 'en', search: '"en name"' }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['name'],
          outFields: ['name'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(map(dissoc('score'), dataflows)).toEqual([{ name: 'EN Name' }])
    })

    it('should filter on name_t with one phrase single quoted', async () => {
      const input = { lang: 'en', search: "'en name'" }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['name'],
          outFields: ['name'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(map(dissoc('score'), dataflows)).toEqual([{ name: 'EN Name' }])
    })

    it('should filter on name_t excluding one term', async () => {
      const input = { lang: 'en', search: '-name' }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['name'],
          outFields: ['name'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(map(dissoc('score'), dataflows)).toEqual([{ name: 'Interest rate statistics' }])
    })

    it('should filter on name_t excluding one phrase', async () => {
      const input = { lang: 'en', search: '-"rate statistics"' }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['name'],
          outFields: ['name'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(map(dissoc('score'), dataflows)).toEqual([{ name: 'EN Name' }, { name: 'EN Name2' }])
    })

    it('should filter on name and interest_rate_type', async () => {
      const input = { lang: 'en', search: 'interest' }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['name', 'interest_rate_type'],
          outFields: ['id'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(map(dissoc('score'), dataflows)).toEqual([{ id: 'IRS' }, { id: 'CDE' }])
    })

    it('should filter on name and interest_rate_type excluding one term', async () => {
      const input = { lang: 'en', search: '-interest' }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['name', 'interest_rate_type'],
          outFields: ['id'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(map(dissoc('score'), dataflows)).toEqual([{ id: 'ABC' }])
    })

    it('should filter on name and interest_rate_type with 2 terms excluding one', async () => {
      const input = { lang: 'en', search: '-interest XXX' }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['name', 'interest_rate_type'],
          outFields: ['id'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(map(dissoc('score'), dataflows)).toEqual([])
    })

    it('should filter on name and interest_rate_type with 2 phrases excluding one', async () => {
      const input = { lang: 'en', search: '-"interest rate" XXX' }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['name', 'interest_rate_type'],
          outFields: ['id'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(dataflows).toEqual([])
    })

    it('should filter on agency_s', async () => {
      const input = { lang, search: 'Agency' }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['agency'],
          outFields: ['agency'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(map(dissoc('score'), dataflows)).toEqual([{ agency: 'Agency1' }, { agency: 'Agency2' }])
    })

    it('should filter on agency_s in lower case', async () => {
      const input = { lang, search: 'agency' }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['agency'],
          outFields: ['agency'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(dataflows.length).toBe(0)
    })
  })

  describe('Boosting terms', () => {
    it('should filter with weight', async () => {
      const input = { lang, search: 'interest' }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['name', 'boost'],
          outFields: ['id'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(map(dissoc('score'), dataflows)).toEqual([{ id: 'CDE' }, { id: 'IRS' }])
    })
  })

  describe('Tagged terms', () => {
    it('should filter with one tag', async () => {
      const input = { lang, search: 'name:interest' }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['name'],
          outFields: ['id'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(map(dissoc('score'), dataflows)).toEqual([{ id: 'IRS' }])
    })

    it('should filter with 2 tags', async () => {
      const input = { lang, search: 'name:name -agency:"Agency2"' }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['name', 'agency'],
          outFields: ['id'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(map(dissoc('score'), dataflows)).toEqual([{ id: 'ABC' }])
    })

    it('should filter with wrong tags', async () => {
      const input = { lang, search: 'toto:name' }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['name', 'toto'],
          outFields: ['id'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(map(dissoc('score'), dataflows)).toEqual([{ id: 'IRS' }])
    })
  })

  describe('Locale', () => {
    it('should filter with existing locale', async () => {
      const input = {
        lang,
        search: 'CZ Name1',
      }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['name'],
          outFields: ['id'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(map(dissoc('score'), dataflows)).toEqual([{ id: 'ABC' }])
    })

    it('should filter with non existing locale', async () => {
      const input = {
        lang: 'xx',
        search: 'CZ Name1',
      }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          searchFields: ['name'],
          outFields: ['name'],
        },
      }
      const ctx = runService(localConfig)
      const { dataflows } = await ctx().api.run({ service: 'search', method: 'post', input }, { tenant: TENANT })
      expect(dataflows.length).toBe(0)
    })
  })

  describe('Facets', () => {
    it('should aggregate facets', async () => {
      const input = {
        lang,
        facets: {
          frequency: ['Daily (D)', 'Monthly (M)'],
          reference_area: ['Belgium (BE)'],
        },
      }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          outFields: ['id'],
        },
      }

      const ctx = runService(localConfig)
      const { dataflows, facets } = await ctx().api.run(
        { service: 'search', method: 'post', input },
        { tenant: TENANT },
      )
      expect(map(dissoc('score'), dataflows)).toEqual([{ id: 'ABC' }])
      expect(map(prop('count'), facets.frequency.buckets)).toEqual([1, 1])
    })
  })

  describe('Config', () => {
    it('should post config with on facet', async () => {
      const input = {
        lang,
        facets: {
          datasourceId: ['SOURCE2'],
        },
      }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          outFields: ['id'],
        },
      }

      const ctx = runService(localConfig)
      const { locale, facets } = await ctx().api.run({ service: 'config', method: 'post', input }, { tenant: TENANT })
      expect(locale).toEqual(lang)
      expect(facets.datasourceId.buckets[0].val).toEqual('SOURCE2')
    })

    it('should post config without facets', async () => {
      const input = {
        lang,
      }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          outFields: ['id'],
        },
      }

      const ctx = runService(localConfig)
      const { facets } = await ctx().api.run({ service: 'config', method: 'post', input }, { tenant: TENANT })
      expect(map(prop('val'), facets.datasourceId.buckets)).toEqual(['SOURCE1', 'SOURCE2'])
    })

    it('should post config without facets values', async () => {
      const input = {
        lang,
        facets: {},
      }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          outFields: ['id'],
        },
      }

      const ctx = runService(localConfig)
      const { facets } = await ctx().api.run({ service: 'config', method: 'post', input }, { tenant: TENANT })
      expect(map(prop('val'), facets.datasourceId.buckets)).toEqual(['SOURCE1', 'SOURCE2'])
    })

    it('should post config with wrong facets', async () => {
      const input = {
        lang,
        facets: {
          datasourceId: ['SOURCE2', 'SOURCE3'],
        },
      }
      const localConfig = {
        ...config,
        dataflows: {
          ...config.dataflows,
          outFields: ['id'],
        },
      }

      const ctx = runService(localConfig)
      const { locale, facets } = await ctx().api.run({ service: 'config', method: 'post', input }, { tenant: TENANT })
      expect(locale).toEqual(lang)
      expect(facets.datasourceId.buckets.length).toEqual(1)
    })
  })
})
