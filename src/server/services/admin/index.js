import { reduce } from 'ramda'
import debug from '../../debug'
import evtX from 'evtx'
import initDataflows from './dataflows'
import initDataflow from './dataflow'
import initConfig from './config'
import initReport from './report'
import initLogs from './logs'
import { checkApiKey } from './utils'

const allServices = [initConfig, initDataflows, initDataflow, initReport, initLogs]
const initServices = evtx => reduce((acc, service) => acc.configure(service), evtx, allServices)

export default async ctx => {
  const admin = evtX(ctx)
    .configure(initServices)
    .before(checkApiKey)
  debug.info('admin API setup.')
  return ctx({ services: { ...ctx().services, admin } })
}
