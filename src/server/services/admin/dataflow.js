import { join, find, filter, pluck, toPairs, propOr, compose } from 'ramda'
import debug from '../../debug'
import { HTTPError } from '../../utils/errors'
import { ID, getStructure } from '../../sdmx/utils'
import { checkTenant, getSolrClient, getSolrConfig } from '../utils'

const NAME = 'dataflow'

export const service = {
  async delete({ spaceId, agencyId, id, version, email }) {
    const { indexer } = this.globals()
    const { res, tenant } = this.locals
    const space = tenant.spaces?.[spaceId]
    const dID = ID('dataflow')
    if (!space) throw new HTTPError(400, `Unknown space ${spaceId} for organisation ${tenant.id}`)

    debug.info(`deleting [dataflow] ${agencyId}:${id}(${version}) for organisation ${tenant.id} from space ${spaceId}`)
    const datasources = compose(
      pluck(0),
      filter(([datasourceId, { dataSpaceId, indexed }]) => dataSpaceId === spaceId && indexed), // eslint-disable-line
      toPairs,
      propOr([], 'datasources'),
    )(tenant)

    if (!datasources.length)
      throw new HTTPError(404, `No datasource found linked to space ${spaceId} for organisation ${tenant.id}`)

    const matches = await indexer.deleteOne(tenant, spaceId, datasources, agencyId, id, version, { userEmail: email })

    if (!matches.length) {
      const message = `${dID({ agencyId, id, version })} was not found`
      debug.warn(message)
      res.status(400).json({ message })
    } else {
      const message = `${dID({ agencyId, id, version })} deleted with datasources: [${join(', ', matches)}]`
      debug.info(message)
      res.status(200).json({ message })
    }
  },

  async post({ spaceId, agencyId, id, version, email }) {
    const { indexer } = this.globals()
    const { res, tenant } = this.locals
    const space = tenant.spaces?.[spaceId]
    if (!space) throw new HTTPError(400, `Unknown space ${spaceId} for organisation ${tenant.id}`)
    debug.info(`upserting [dataflow] ${agencyId}:${id}(${version}) for organisation ${tenant.id} from space ${spaceId}`)
    const datasources = compose(
      filter(([datasourceId, { dataSpaceId, indexed }]) => dataSpaceId === spaceId && indexed), // eslint-disable-line
      toPairs,
      propOr([], 'datasources'),
    )(tenant)
    if (!datasources.length)
      throw new HTTPError(404, `No datasource found linked to space ${spaceId} for organisation ${tenant.id}`)
    const spaceUrl = space.url
    const dID = ID('dataflow')
    debug.info(`Found ${datasources.length} datasource(s): [${join(', ', pluck(0, datasources))}]`)
    const matches = []
    for (const [datasourceId, ds] of datasources) {
      ds.id = datasourceId
      for (const query of ds.dataqueries) {
        try {
          const structure = await getStructure({
            spaceUrl,
            resourceType: 'categoryscheme',
            resourceId: query.categorySchemeId,
            version: query.version,
            agencyId: query.agencyId,
            references: 'dataflow',
            detail: 'full',
          })
          const dataflow = find(
            dataflow => dID(dataflow) === dID({ agencyId, id, version }),
            structure.data?.dataflows || [],
          )
          if (dataflow) {
            debug.info(dataflow, `found ${agencyId}:${id}(${version}) in ${datasourceId}`)
            await indexer.loadOne(tenant, ds, {
              dataflowId: dataflow.id,
              datasourceId,
              dataSpaceId: spaceId,
              agencyId,
              version,
              isExternalReference: dataflow.isExternalReference,
              links: dataflow.links,
              userEmail: email,
            })
            matches.push(datasourceId)
          } else {
            debug.info(`cannot find ${agencyId}:${id}(${version}) in ${datasourceId}`)
          }
        } catch (err) {
          debug.error(err)
          throw err
        }
      }
    }

    if (!matches.length) {
      const message = `${dID({ agencyId, id, version })} do not match any datasources`
      debug.warn(message)
      res.status(400).json({ message })
    } else {
      const message = `${dID({ agencyId, id, version })} indexed with datasources: [${join(', ', matches)}]`
      debug.info(message)
      res.status(200).json({ message })
    }
  },
}

const init = evtx => {
  evtx.use(NAME, service).service(service.name)
  evtx.service(NAME).before({
    delete: [checkTenant, getSolrConfig, getSolrClient],
    post: [checkTenant, getSolrConfig, getSolrClient],
  })
}
export default init
