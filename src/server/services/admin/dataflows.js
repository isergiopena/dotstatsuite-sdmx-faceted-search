import { path } from 'ramda'
import { checkTenant, getSolrClient } from '../utils'

const NAME = 'dataflows'

export const service = {
  async delete({ email }) {
    const { indexer } = this.globals()
    const { tenant } = this.locals
    await indexer.deleteAll(tenant, { userEmail: email })
    return {}
  },

  async post({ mode, email }) {
    const { report, indexer } = this.globals()
    const { tenant, res } = this.locals
    const process = indexer(tenant, { userEmail: email })
    const { value: loadingId } = await process.next()

    if (mode === 'stream') {
      res.setHeader('Content-Type', 'application/json')
      res.setHeader('Transfer-Encoding', 'chunked')
      const unlisten = report.listen(state => {
        const loading = path(['loadings', loadingId], state)
        if (loading) res.write(JSON.stringify({ type: 'INDEXING_DATAFLOWS', loadingId, datasources: loading }))
      })
      res.write(JSON.stringify({ type: 'START_INDEXING_DATAFLOWS', service: '/admin/dataflows', loadingId }))
      await process.next()
      unlisten()
      res.write(JSON.stringify({ type: 'END_INDEXING_DATAFLOWS', service: '/admin/dataflows', loadingId }))
      res.end()
    } else if (mode === 'sync') {
      await process.next()
      return path(['loadings', loadingId], report())
    } else {
      process.next()
      return { loadingId }
    }
  },
}

const init = evtx => {
  evtx.use(NAME, service)
  evtx.service(NAME).before({
    post: [checkTenant, getSolrClient],
    delete: [checkTenant, getSolrClient],
  })
}

export default init
