import HTTPStatus from 'http-status';
import { HTTPError } from '../../utils/errors';

export const checkApiKey = ctx => {
  const {
    req: {
      headers: { 'x-api-key': xApiKey },
      query: { 'api-key': qApiKey },
    },
  } = ctx.locals;
  const {
    config: { apiKey },
  } = ctx.globals();
  if (apiKey !== (xApiKey || qApiKey)) return Promise.reject(new HTTPError(HTTPStatus.UNAUTHORIZED));
  return Promise.resolve(ctx);
};
