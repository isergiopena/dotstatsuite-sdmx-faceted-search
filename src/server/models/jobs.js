import { ObjectID } from 'mongodb'
import { has, prop } from 'ramda'
import Model from './model'
import debug from '../debug'
import { WARNING, ERROR } from '../reporter/errors'
import {
  DATAFLOWS_DELETED,
  DATAFLOW_DELETED,
  DATAFLOW_INDEXED,
  START_LOADING_DATASOURCE,
  START_INDEXING,
  END_INDEXING,
} from '../reporter/loadings'

const NAME = 'jobs'

const STATUS = {
  warning: 'warning',
  error: 'error',
  success: 'success',
  completed: 'completed',
  queued: 'queued',
  inProgress: 'inProgress',
}

// const parseArtefact = urn => urn.match(/^(.+):(.+)\((.+)\)/)

const makeLoadAllFilter = params => {
  const filter = {}
  if (!params) return filter
  if (params.userEmail) filter.userEmail = params.userEmail
  if (params.action) filter.action = params.action
  if (params.dataspace) filter.dataSpaceIds = params.dataspace
  if (params.artefact) {
    if (params.artefact.resourceType) filter['logs.artefact.resourceType'] = params.artefact.resourceType
    if (params.artefact.agencyID) filter['logs.artefact.agencyID'] = params.artefact.agencyID
    if (params.artefact.id) filter['logs.artefact.id'] = params.artefact.id
    if (params.artefact.version) filter['logs.artefact.version'] = params.artefact.version
  }
  if (params.submissionStart) filter.executionStart = { $gte: new Date(params.submissionStart) }
  if (params.submissionEnd) filter.executionEnd = { $lte: new Date(params.submissionEnd) }
  if (params.executionStatus) filter.executionStatus = params.executionStatus
  if (params.id) filter._id = Number(params.id)

  // if(has('executionStatus', params)){
  //   if(params.executionStatus === 0) filter.executionStatus = STATUS.queued
  //   else if(params.executionStatus === 1) filter.executionStatus = STATUS.inProgress
  //   else if(params.executionStatus === 2) filter.executionStatus = STATUS.completed
  // }
  //
  // if(has('executionOutcome', params)){
  //   if(params.executionOutcome === 0) filter.outcome = STATUS.success
  //   else if(params.executionOutcome === 1) filter.outcome = STATUS.warning
  //   else if(params.executionOutcome === 2) filter.outcome = STATUS.error
  // }

  return filter
}

export class Job extends Model {
  constructor(name, collection) {
    super(name, collection)
  }

  getOutcome(job) {
    if (!job) return
    for (const log of job.logs || []) {
      if (job.outcome === STATUS.error) return job
      if (!job.outcome || log.status === ERROR) job.outcome = log.status
      else if (job.outcome !== STATUS.warning) job.outcome = log.status
    }
    return job
  }

  loadOne(_id, params) {
    return this.collection.findOne({ _id }, params)
  }

  async loadAll(params = {}) {
    const filter = makeLoadAllFilter(params)
    const jobs = await this.collection.find(filter).toArray()
    return jobs.reduce((acc, job) => {
      const version = this.getOutcome(job)
      if (params.executionOutcome && params.executionOutcome !== version.outcome) return acc
      version.id = version._id
      delete version._id
      return [...acc, version]
    }, [])
  }

  insertOne(document) {
    return this.collection.insertOne(document).then(prop('insertedId'))
  }

  insertMany(documents) {
    return this.collection.insertMany(documents).then(prop('insertedIds'))
  }

  async delete(id) {
    if (!(await this.loadOne(id))) throw new Error('Unknown document')
    return this.collection.deleteOne({ _id: new ObjectID(id) })
  }

  async checkJob(id) {
    const job = await this.loadOne(id)
    if (job) return job
    debug.error(`Cannot get job: ${id}`)
  }

  async startIndexing({ loadingId, userEmail, tenant, action }) {
    const job = await this.loadOne(loadingId)
    if (job) return debug.error(`job: ${loadingId} already exist!`)
    const data = {
      _id: loadingId,
      userEmail,
      submissionTime: new Date(),
      executionStart: new Date(),
      executionStatus: STATUS.queued,
      organisationId: tenant.id,
      action,
    }
    await this.insertOne(data)
  }

  async startLoadingDatasource({ loadingId, datasourceId, dataSpaceId }) {
    const job = await this.checkJob(loadingId)
    if (!job) return

    const updates = { executionStatus: STATUS.inProgress }
    await this.collection.updateOne(
      { _id: job._id },
      { $set: updates, $addToSet: { dataSourceIds: datasourceId, dataSpaceIds: dataSpaceId } },
    )
  }

  async processed({ loadingId, message, dataSpaceId, artefact }) {
    const job = await this.checkJob(loadingId)
    if (!job) return

    const logEntry = {
      executionStart: new Date(),
      message: message || 'Successfully indexed',
      status: STATUS.success,
      server: 'sfs',
    }
    if (artefact) logEntry.artefact = artefact

    const query = { $push: { logs: logEntry } }
    if (dataSpaceId) {
      logEntry.dataSpaceId = dataSpaceId
      query.$addToSet = { dataSpaceIds: dataSpaceId }
    }
    await this.collection.updateOne({ _id: job._id }, query)
  }

  async manageError(error, { loadingId, dataSpaceId, message, logger, artefact }) {
    const job = await this.checkJob(loadingId)
    if (!job) return

    const logEntry = {
      executionStart: new Date(),
      message,
      logger,
      status: STATUS.error,
      server: 'sfs',
      exception: error.stack,
    }
    if (artefact) logEntry.artefact = artefact
    const query = { $push: { logs: logEntry } }
    if (dataSpaceId) {
      logEntry.dataSpaceId = dataSpaceId
      query.$addToSet = { dataSpaceIds: dataSpaceId }
    }
    await this.collection.updateOne({ _id: job._id }, query)
  }

  async manageWarning({ loadingId, dataSpaceId, message, logger, artefact }) {
    const job = await this.checkJob(loadingId)
    if (!job) return

    const logEntry = {
      executionStart: new Date(),
      message,
      logger,
      status: STATUS.warning,
      server: 'sfs',
    }
    if (artefact) logEntry.artefact = artefact
    const query = { $push: { logs: logEntry } }
    if (dataSpaceId) {
      logEntry.dataSpaceId = dataSpaceId
      query.$addToSet = { dataSpaceIds: dataSpaceId }
    }
    await this.collection.updateOne({ _id: job._id }, query)
  }

  async endIndexing({ loadingId }) {
    const job = await this.checkJob(loadingId)
    if (!job) return

    const updates = { executionStatus: STATUS.completed, executionEnd: new Date() }
    return this.collection.updateOne({ _id: job._id }, { $set: updates })
  }

  async manageEvent({ type, ...others }) {
    switch (type) {
      case START_INDEXING:
        return this.startIndexing(others.loading)
      case START_LOADING_DATASOURCE:
        return this.startLoadingDatasource(others.loading)
      case DATAFLOW_INDEXED:
      case DATAFLOW_DELETED:
      case DATAFLOWS_DELETED:
        return this.processed(others.loading)
      case ERROR:
        return this.manageError(others.error, others.loading)
      case WARNING:
        return this.manageWarning(others.loading)
      case END_INDEXING:
        return this.endIndexing(others.loading)
    }
  }
}

export default async ({ database }) => {
  const col = await database.listCollections({ name: NAME }, { nameOnly: true }).toArray()
  if (!col) await database.createCollection(NAME, { capped: true, size: 1024000 })
  const collection = database.collection(NAME)
  return [
    NAME,
    new Job(NAME, collection),
    {
      indexes: [[{ submissionTime: 1 }]],
    },
  ]
}
