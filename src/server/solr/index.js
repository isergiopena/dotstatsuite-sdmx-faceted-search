import { dissoc, pathEq } from 'ramda'
import debug from '../debug'
import { filterMemberCollections } from '../utils'
import got from 'got'

const isStatusOk = pathEq(['responseHeader', 'status'], 0)

class Solr {
  constructor({ host, port, logLevel }, allMembers) {
    this.host = host
    this.port = port
    this.logLevel = logLevel
    this.baseURL = `http://${host}:${port}`
    this.allMembers = allMembers
  }

  getClient(tenant) {
    return new Client(this, tenant)
  }

  post(url, options) {
    debug.trace(options, `solr:: POST: ${this.baseURL}/${url}`)
    // console.log(options, `solr:: POST: ${this.baseURL}/${url}`)
    return this.request(url, { ...options, method: 'post', prefixUrl: this.baseURL })
  }

  get(url) {
    return this.request(url, { method: 'get', prefixUrl: this.baseURL })
  }

  async request(url, options) {
    try {
      const res = await got(url, options).json()
      debug.trace(res)
      if (isStatusOk(res)) return res
      throw new Error(res.error.msg)
    } catch (err) {
      if (err?.response?.body) debug.error(JSON.parse(err.response.body), 'Solr Error')
      throw err
    }
  }

  async adminStatus() {
    const url = 'solr/admin/info/system?&wt=json'
    const status = await this.get(url)
    return dissoc('responseHeader', status)
  }

  async getActiveMembers() {
    const res = await this.status()
    const collections = filterMemberCollections(res, this.allMembers)
    return Object.keys(collections)
  }

  status() {
    const url = 'solr/admin/collections?action=CLUSTERSTATUS&wt=json'
    return this.get(url).then(({ cluster }) => cluster.collections)
  }

  createTenant(tenant) {
    const url = `solr/admin/collections?action=CREATE&name=${tenant}&numShards=1&collection.configName=_default`
    return this.get(url)
  }
}

class Client {
  constructor(solr, tenant) {
    this.solr = solr
    this.collection = tenant.id
  }

  deleteAll() {
    const { collection } = this
    const url = `solr/${collection}/update/json?commit=true`
    const json = { delete: { query: '*:*' } }
    return this.solr.post(url, { json })
  }

  deleteOne(id) {
    const { collection } = this
    const url = `solr/${collection}/update/json?commit=true`
    const json = { delete: { query: `id:"${id}"` } }
    return this.solr.post(url, { json })
  }

  add(data) {
    const { collection } = this
    const url = `solr/${collection}/update/json?commit=true`
    const json = { add: data }
    return this.solr.post(url, { json })
  }

  select(request) {
    const { collection } = this
    const url = `solr/${collection}/select?wt=json`
    return this.solr.post(url, { json: request })
  }
}

const init = async ctx => {
  const {
    config: { solr: solrConfig },
    configProvider,
  } = ctx()
  const members = await configProvider.getTenants()
  const solr = new Solr(solrConfig, members)
  const activeMembers = await solr.getActiveMembers()
  if (!activeMembers.length) debug.warn(`Solr has no member collection`)
  else debug.info(`Solr is ready to serve U with member collections: [${activeMembers.join(', ')}]`)
  return ctx({ solr })
}

export default init
