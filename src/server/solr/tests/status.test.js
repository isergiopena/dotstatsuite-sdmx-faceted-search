import { initResources } from 'jeto';
import initSolr from '..';
import { initConfig } from '../../tests/utils';

let CTX;

describe('Solr Status', () => {
  beforeAll(() => initResources([initConfig, initSolr]).then(ctx => (CTX = ctx)));
  afterAll(() => CTX().http.close());

  it('should get right status', async () => {
    await CTX().solr.status();
  });
});
