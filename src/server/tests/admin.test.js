import axios from 'axios'
import nock from 'nock'
import { prop } from 'ramda'
import urljoin from 'url-join'
import { createContext } from 'jeto'
import initHttp from '../init/http'
import initServices from '../services'
import initSolr from '../solr'
import initIndexer from '../init/indexer'
import initReporter from '../init/reporter'
import initConfigManager from '../init/configManager'
import initParams from '../init/loadParams'
import fixture from './fixtures/data1'
import { TENANT, initConfig } from './utils'
import initMongo from '../init/mongo'

const IRS = require('./mocks/sdmx/IRS')

const makeRequestFactory = (http, { apiKey }) => ({ method = 'post', url, data }) =>
  axios({ method, url: urljoin(http.url, url), data, headers: { 'x-tenant': TENANT.id, 'x-api-key': apiKey } }).then(
    prop('data'),
  )
let makeRequest
let CTX

const tearContext = async () => {
  if (!CTX) return
  await CTX().mongo.dropDatabase()
  await CTX().http.close()
  await CTX().mongo.close()
  const solrClient = CTX().solr.getClient(TENANT)
  await solrClient.deleteAll()
}

const params = () => ({
  defaultLocale: 'en',
})

const initContext = async () => {
  try {
    CTX = await initConfig(createContext, params())
      .then(initMongo)
      .then(initReporter)
      .then(initSolr)
      .then(initConfigManager)
      .then(initParams)
      .then(initIndexer)
      .then(initServices)
      .then(initHttp)
    const solrClient = CTX().solr.getClient(TENANT)
    await solrClient.deleteAll()
    // await CTX().configManager.updateSchema(TENANT, CTX().config.fields)
    makeRequest = makeRequestFactory(CTX().http, CTX().config)
  } catch (e) {
    console.error(e) // eslint-disable-line
    throw e
  }
}

beforeAll(initContext)
// afterAll(tearContext)

describe('Server | services | admin', () => {
  it('should get config', async () => {
    const res = await makeRequest({ method: 'GET', url: '/admin/config' })
    expect(res.configUrl).toEqual('http://configProvider')
  })

  it('should create a dataflow', async () => {
    const spaceId = 'demo-stable'
    const space = TENANT.spaces[spaceId]

    const { dataflowId_s: id, version_s: version, agencyId_s: agencyId } = fixture.data[0]

    const dataflows = [
      {
        id,
        agencyId,
        version,
      },
    ]

    nock(space.url)
      .get('/categoryscheme/OECD/OECDCS1/1.0?references=dataflow&detail=full')
      .reply(200, { data: { dataflows } })

    nock(space.url)
      .get('/categoryscheme/OECD/TEST/1.0?references=dataflow&detail=full')
      .reply(200, { data: {} })

    nock(space.url)
      .get('/dataflow/ECB/IRS/1.0/?references=all&detail=referencepartial')
      .reply(200, IRS)

    await makeRequest({
      method: 'POST',
      url: '/admin/dataflow',
      data: { spaceId, id, version, agencyId },
    })

    const search = ` dataflowId:"${id}" version:"${version}" agencyId:"${agencyId}"`
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    expect(res.numFound).toEqual(1)
  })

  it('should update a dataflow', async () => {
    const spaceId = 'demo-stable'
    const space = TENANT.spaces[spaceId]

    const { dataflowId_s: id, agencyId_s: agencyId } = fixture.data[0]

    const version = '1.4'
    const dataflows = [
      {
        id,
        agencyId,
        version,
      },
    ]

    nock(space.url)
      .get('/categoryscheme/OECD/OECDCS1/1.0?references=dataflow&detail=full')
      .reply(200, { data: { dataflows } })

    nock(space.url)
      .get('/categoryscheme/OECD/TEST/1.0?references=dataflow&detail=full')
      .reply(200, { data: {} })

    IRS.data.dataflows[0].version = version
    nock(space.url)
      .get('/dataflow/ECB/IRS/1.4/?references=all&detail=referencepartial')
      .reply(200, IRS)

    await makeRequest({
      method: 'POST',
      url: '/admin/dataflow',
      data: { spaceId, id, version, agencyId },
    })

    const search = ` dataflowId:"${id}" version:"${version}" agencyId:"${agencyId}"`
    const facets = { Country: ['0|Australia#AUS#'] }
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search, facets } })
    expect(res.numFound).toEqual(1)
    expect(res.dataflows[0].version).toEqual(version)
  })

  it('should delete a dataflow', async () => {
    const spaceId = 'demo-stable'
    const { dataflowId_s: id, agencyId_s: agencyId } = fixture.data[0]
    const version = '1.4'

    await makeRequest({
      method: 'DELETE',
      url: '/admin/dataflow',
      data: { spaceId, id, version, agencyId },
    })

    const search = ` dataflowId:"${id}" agencyId:"${agencyId}"`
    const res = await makeRequest({ method: 'POST', url: '/api/search', data: { search } })
    expect(res.numFound).toEqual(0)
  })
})
