import R from 'ramda';

const fixture = {
  name: 'localized search',
  config: {
    defaultLocale: 'cz',
  },
  data: [
    {
      id: 'SRC:IRS',
      dataflowId_s: 'IRS',
      datasourceId_s: 'SRC',
      datasourceId_t: 'SRC',
      type_s: 'dataflow',
      agencyId_s: 'ECB',
      version_s: '1.0',
      name_en_t: 'Interest rate statistics',
      name_cz_t: 'CZ Interest rate statistics',
      name_fr_t: 'Bonjour',
      description_en_t: 'hello good dev',
    },
    {
      id: 'SRC:ABC',
      datasourceId_s: 'SRC',
      dataflowId_s: 'ABC',
      type_s: 'dataflow',
      agencyId_s: 'Agency1',
      version_t: '1.0',
      name_en_t: 'Name1',
      name_cz_t: 'CZ Name1',
      decription_en_t: 'Facere aut expedita quibusdam earum veniam',
    },
    {
      id: 'SRC:CDE',
      dataflowId_s: 'CDE',
      datasourceId_s: 'SRC',
      type_s: 'dataflow',
      agencyId_s: 'Agency2',
      version_t: '1.0',
      name_en_t: 'Name2',
      name_cz_t: 'CZ Name2',
    },
  ],
  tests: [
    {
      name: 'should get IDS',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          search: 'hello rate',
          lang: 'en',
        },
      },
      results: [
        {
          fct: R.compose(R.pluck('id'), R.prop('dataflows')),
          data: ['SRC:IRS'],
        },
      ],
    },
    {
      name: 'should get localized names (cz)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          lang: 'cz',
        },
      },
      results: [
        {
          fct: R.compose(R.pluck('name'), R.prop('dataflows')),
          data: ['CZ Interest rate statistics', 'CZ Name1', 'CZ Name2'],
        },
      ],
    },
    {
      name: 'should get default localized names (cz)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {},
      },
      results: [
        {
          fct: R.compose(R.pluck('name'), R.prop('dataflows')),
          data: ['CZ Interest rate statistics', 'CZ Name1', 'CZ Name2'],
        },
      ],
    },

    {
      name: 'should get localized names (en)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          lang: 'en',
        },
      },
      results: [
        {
          fct: R.compose(R.pluck('name'), R.prop('dataflows')),
          data: ['Interest rate statistics', 'Name1', 'Name2'],
        },
      ],
    },
    {
      name: 'should get localized names (fr)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          lang: 'fr',
        },
      },
      results: [
        {
          fct: R.compose(R.filter(R.identity), R.pluck('name'), R.prop('dataflows')),
          data: ['Bonjour'],
        },
      ],
    },
    {
      name: 'should get agencyId',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          lang: 'cz',
        },
      },
      results: [
        {
          fct: R.compose(R.pluck('agencyId'), R.prop('dataflows')),
          data: ['ECB', 'Agency1', 'Agency2'],
        },
      ],
    },
    {
      name: 'should search (fr)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          search: 'Bonjou',
          lang: 'fr',
        },
      },
      results: [
        {
          fct: R.compose(R.filter(R.identity), R.pluck('name'), R.prop('dataflows')),
          data: ['Bonjour'],
        },
      ],
    },
    {
      name: 'should search (fr)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          search: 'BonjouX',
          lang: 'fr',
        },
      },
      results: [
        {
          fct: R.prop('dataflows'),
          data: [],
        },
      ],
    },
    {
      name: 'should multi search (en)',
      request: {
        method: 'post',
        url: '/api/search',
        data: {
          search: 'hello rate',
          lang: 'en',
        },
      },
      results: [
        {
          fct: R.compose(R.filter(R.identity), R.pluck('id'), R.prop('dataflows')),
          data: ['SRC:IRS'],
        },
      ],
    },
  ],
};

export default fixture;
