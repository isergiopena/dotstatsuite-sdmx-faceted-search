import axios from 'axios'
import nock from 'nock'
import { prop } from 'ramda'
import urljoin from 'url-join'
import { createContext } from 'jeto'
import initHttp from '../init/http'
import initServices from '../services'
import initSolr from '../solr'
import initIndexer from '../init/indexer'
import initReporter from '../init/reporter'
import initConfigManager from '../init/configManager'
import initParams from '../init/loadParams'
import fixture from './fixtures/data1'
import { TENANT, initConfig, waitFor } from './utils'
import initMongo from '../init/mongo'
import initModels from '../init/models'
import initLogger from '../init/logger'

const IRS = require('./mocks/sdmx/IRS')

const makeRequestFactory = (http, { apiKey }) => ({ method = 'post', url, data }) =>
  axios({ method, url: urljoin(http.url, url), data, headers: { 'x-tenant': TENANT.id, 'x-api-key': apiKey } }).then(
    prop('data'),
  )
let makeRequest
let CTX

const tearContext = async () => {
  if (!CTX) return
  await CTX().mongo.dropDatabase()
  await CTX().http.close()
  await CTX().mongo.close()
  const solrClient = CTX().solr.getClient(TENANT)
  await solrClient.deleteAll()
}

const params = () => ({
  defaultLocale: 'en',
})

const initContext = async () => {
  try {
    CTX = await initConfig(createContext, params())
      .then(initMongo)
      .then(initModels)
      .then(initReporter)
      .then(initLogger)
      .then(initSolr)
      .then(initConfigManager)
      .then(initParams)
      .then(initIndexer)
      .then(initServices)
      .then(initHttp)
    const solrClient = CTX().solr.getClient(TENANT)
    await solrClient.deleteAll()
    await CTX().configManager.updateSchema(TENANT, CTX().config.fields)
    makeRequest = makeRequestFactory(CTX().http, CTX().config)
  } catch (e) {
    throw e
  }
}

beforeAll(initContext)
afterAll(tearContext)

describe('Server | services | admin', () => {
  const email = 'EMAIL'
  let requestId

  it('should not index catategoryscheme', async () => {
    const { models } = CTX()
    await makeRequest({ method: 'POST', url: `/admin/dataflows?mode=sync&email=${email}` })
    let logs
    await waitFor(async () => {
      logs = await models.jobs.loadAll()
      expect(logs[0].executionStatus).toEqual('completed')
    })

    const data = logs[0]
    expect(data.userEmail).toEqual('EMAIL')
    expect(data.organisationId).toEqual('test')
    expect(data.action).toEqual('indexAll')
    expect(data.dataSourceIds).toEqual(['qa-stable', 'demo-stable'])
    expect(data.logs.length).toEqual(3)
  })

  it('should get logs with errors', async () => {
    const params = {
      userEmail: email,
      dataspace: 'demo-stable',
      submissionStart: new Date([2017]),
      submissionEnd: new Date([new Date().getUTCFullYear() + 1]),
      executionStatus: 'completed',
      executionOutcome: 'error',
      artefact: {
        resourceType: 'categoryscheme',
        agencyID: 'OECD',
        id: 'TEST',
        version: '1.0',
      },
    }
    let logs = await makeRequest({ method: 'POST', data: params, url: `/admin/logs` })
    const data = logs[0]
    expect(data.logs.length).toEqual(3)
  })

  it('should get catategoryscheme but not index dataflow', async () => {
    const { dataflowId_s: id, version_s: version, agencyId_s: agencyID } = fixture.data[0]
    const dataflows = [{ id, agencyID, version }]

    nock('http://qa-stable')
      .persist()
      .get('/categoryscheme/OECD/OECDCS1/1.0/?references=dataflow')
      .reply(200, { data: {} })

    nock('http://demo-stable')
      .persist()
      .get('/categoryscheme/OECD/OECDCS1/1.0/?references=dataflow')
      .reply(200, { data: { dataflows } })

    nock('http://demo-stable')
      .persist()
      .get('/categoryscheme/OECD/TEST/1.0/?references=dataflow')
      .reply(200, { data: {} })

    const { models } = CTX()
    await makeRequest({ method: 'POST', url: `/admin/dataflows?mode=sync&email=${email}` })
    let logs
    await waitFor(async () => {
      logs = await models.jobs.loadAll()
      expect(logs[1].executionStatus).toEqual('completed')
    })
    expect(logs[1].logs[0].artefact).toEqual({ resourceType: 'dataflow', agencyID: 'ECB', id: 'IRS', version: '1.0' })
  })

  it('should index many dataflows', async () => {
    nock('http://demo-stable')
      .persist()
      .get('/dataflow/ECB/IRS/1.0/?references=all&detail=referencepartial')
      .reply(200, IRS)

    const { models } = CTX()
    await makeRequest({ method: 'POST', url: `/admin/dataflows?mode=sync&email=${email}` })
    let logs
    await waitFor(async () => {
      logs = await models.jobs.loadAll()
      expect(logs[2].executionStatus).toEqual('completed')
    })
    const res = await makeRequest({ method: 'GET', url: `/admin/logs` })
    expect(logs[2].logs[0].artefact).toEqual({ resourceType: 'dataflow', agencyID: 'ECB', id: 'IRS', version: '1.0' })
    expect(res[2].outcome).toEqual('success')
  })

  it('should get logs in success', async () => {
    const params = {
      userEmail: email,
      dataspace: 'demo-stable',
      submissionStart: new Date([2017]),
      submissionEnd: new Date([new Date().getUTCFullYear() + 1]),
      executionStatus: 'completed',
      executionOutcome: 'success',
    }
    let logs = await makeRequest({ method: 'POST', data: params, url: `/admin/logs` })
    requestId = logs[0].id
    expect(logs.length).toEqual(1)
  })

  it('should load one log', async () => {
    const log = await makeRequest({ method: 'GET', url: `/admin/logs?id=${requestId}` })
    expect(log.id).toEqual(requestId)
  })

  it('should index one dataflow', async () => {
    const { models } = CTX()
    const spaceId = 'demo-stable'
    const space = TENANT.spaces[spaceId]

    const { dataflowId_s: id, agencyId_s: agencyId } = fixture.data[0]

    const version = '1.4'
    const dataflows = [
      {
        id,
        agencyId,
        version,
      },
    ]

    nock(space.url)
      .get('/categoryscheme/OECD/OECDCS1/1.0?references=dataflow&detail=full')
      .reply(200, { data: { dataflows } })

    nock(space.url)
      .get('/categoryscheme/OECD/TEST/1.0?references=dataflow&detail=full')
      .reply(200, { data: {} })

    IRS.data.dataflows[0].version = version
    nock(space.url)
      .get('/dataflow/ECB/IRS/1.4/?references=all&detail=referencepartial')
      .reply(200, IRS)

    await makeRequest({
      method: 'POST',
      url: `/admin/dataflow?email=${email}`,
      data: { spaceId, id, version, agencyId },
    })

    let logs
    await waitFor(async () => {
      logs = await models.jobs.loadAll()
      expect(logs[3].executionStatus).toEqual('completed')
    })
    const res = await makeRequest({ method: 'GET', url: `/admin/logs` })
    expect(res[3].logs[0].artefact).toEqual({ resourceType: 'dataflow', agencyID: 'ECB', id: 'IRS', version: '1.4' })
    expect(res[3].action).toEqual('indexOne')
    expect(res[3].outcome).toEqual('success')
  })

  it('should delete a dataflow', async () => {
    const { models } = CTX()
    const spaceId = 'demo-stable'
    const { dataflowId_s: id, agencyId_s: agencyId } = fixture.data[0]
    const version = '1.4'

    await makeRequest({
      method: 'DELETE',
      url: `/admin/dataflow?email=${email}`,
      data: { spaceId, id, version, agencyId },
    })
    let logs
    await waitFor(async () => {
      logs = await models.jobs.loadAll()
      expect(logs[4].executionStatus).toEqual('completed')
    })

    const res = await makeRequest({ method: 'GET', url: `/admin/logs` })
    expect(res[4].action).toEqual('deleteOne')
    expect(res[4].outcome).toEqual('success')
  })

  it('should not delete a dataflow', async () => {
    const { models } = CTX()
    const spaceId = 'demo-stable'
    const { dataflowId_s: id, agencyId_s: agencyId } = fixture.data[0]
    const version = '1.4'

    await makeRequest({
      method: 'DELETE',
      url: `/admin/dataflow?email=${email}`,
      data: { spaceId, id, version, agencyId },
    }).catch(() => null)

    let logs
    await waitFor(async () => {
      logs = await models.jobs.loadAll()
      expect(logs[5].executionStatus).toEqual('completed')
    })

    const res = await makeRequest({ method: 'GET', url: `/admin/logs` })
    expect(res[5].action).toEqual('deleteOne')
    expect(res[5].outcome).toEqual('warning')
  })

  it('should delete all dataflows', async () => {
    const { models } = CTX()
    const spaceId = 'demo-stable'
    const { dataflowId_s: id, agencyId_s: agencyId } = fixture.data[0]
    const version = '1.4'

    await makeRequest({
      method: 'DELETE',
      url: `/admin/dataflows`,
      data: { spaceId, id, version, agencyId, email },
    })

    let logs
    await waitFor(async () => {
      logs = await models.jobs.loadAll()
      expect(logs[6].executionStatus).toEqual('completed')
    })

    const res = await makeRequest({ method: 'GET', url: `/admin/logs` })
    expect(res[6].action).toEqual('deleteAll')
    expect(res[6].outcome).toEqual('success')
    expect(res[6].logs.length).toEqual(1)
  })

  it('should get deleteAll logs', async () => {
    const res = await makeRequest({ method: 'GET', url: `/admin/logs?action=deleteAll&userEmail=EMAIL` })
    expect(res.length).toEqual(1)
  })
})
