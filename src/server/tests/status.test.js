import { prop } from 'ramda'
import { createContext } from 'jeto'
import axios from 'axios'
import initHttp from '../init/http'
import initServices from '../services'
import initSolr from '../solr'
import initMongo from '../init/mongo'
import { TENANT, initConfig } from './utils'
import initConfigManager from '../init/configManager'

let CTX

const tearContext = async () => {
  if (!CTX) return
  await CTX().http.close()
  await CTX().mongo.dropDatabase()
  await CTX().mongo.close()
}

const initContext = async () => {
  try {
    CTX = await initConfig(createContext)
      .then(initMongo)
      .then(initSolr)
      .then(initConfigManager)
      .then(initServices)
      .then(initHttp)
  } catch (e) {
    console.error(e) // eslint-disable-line
    throw e
  }
}

describe('API status service', () => {
  beforeAll(initContext)
  afterAll(tearContext)

  it('should get right status', async () => {
    const { http } = CTX()
    const res = await axios({ method: 'get', url: `${http.url}/healthcheck` }).then(prop('data'))
    expect(res.mongo).toEqual('OK')
    expect(res.totalMembers).toEqual(4)
  })
})
