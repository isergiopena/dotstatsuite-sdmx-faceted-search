import axios from 'axios'
import urljoin from 'url-join'
import { createContext } from 'jeto'
import initHttp from '../init/http'
import initServices from '../services'
import initConfigManager from '../init/configManager'
import initParams from '../init/loadParams'
import initSolr from '../solr'
import { initConfig, TENANT, tearContext } from './utils'
import initMongo from '../init/mongo'
import fixtures from './fixtures'

const makeRequest = (http, { method = 'post', url, data }) =>
  axios({ method, url: urljoin(http.url, url), data, headers: { 'x-tenant': TENANT.id } })
const checkResult = result => ({ fct, data }) => expect(fct(result)).toEqual(data)
const runFixtureTest = (http, request, results) =>
  makeRequest(http, request).then(({ data }) => results.forEach(checkResult(data)))

const params = {
  ...fixtures[0].config, // TODO CARE!!!
}

describe('API', () => {
  let CTX

  const initContext = fixture => async () => {
    CTX = await initConfig(createContext(), params)
      .then(ctx => ctx({ config: { ...ctx().config, defaultLocale: fixture.config.defaultLocale } }))
      .then(initMongo)
      .then(initSolr)
      .then(async ctx => {
        const solrClient = ctx().solr.getClient(TENANT)
        await solrClient.deleteAll()
        return ctx
      })
      .then(initConfigManager)
      .then(initParams)
      .then(initServices)
      .then(initHttp)

    const solrClient = CTX().solr.getClient(TENANT)
    await solrClient.add(fixture.data)
  }

  const runTest = ({ name, request, results }) => {
    test(name, () => {
      const { http } = CTX()
      return runFixtureTest(http, request, results)
    })
  }

  fixtures.forEach(fixture => {
    describe(fixture.name, () => {
      beforeAll(initContext(fixture))
      afterAll(() => tearContext(CTX))
      fixture.tests.forEach(runTest)
    })
  })
})
