import axios from 'axios';
import initHttp from '../init/http';
import initServices from '../services';
import { initConfig } from './utils';

let CTX;

describe('Main', function() {
  beforeAll(() => {
    return initConfig()
      .then(initServices)
      .then(initHttp)
      .then(ctx => (CTX = ctx));
  });

  afterAll(() => CTX().http.close());

  it('should ping', function(done) {
    const url = `${CTX().http.url}/ping`;
    axios({ url })
      .then(({ data }) => {
        expect(data.ping).toEqual('pong');
        done();
      })
      .catch(done);
  });
});
