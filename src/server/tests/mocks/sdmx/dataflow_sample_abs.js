module.exports = {
  data: {
    dataflows: [
      {
        id: 'DF_CPI',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ABS:DF_CPI(1.0.0)',
            type: 'dataflow',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ABS:DS_CPI(1.0.0)',
            type: 'datastructure',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'Consumer Price Index (CPI) 17th Series',
        names: {
          en: 'Consumer Price Index (CPI) 17th Series é-._#& تكرر',
        },
        annotations: [
          {
            type: 'SEARCH_WEIGHT',
            title: 12,
            texts: {
              en: '11',
            },
          },
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ABS:DS_CPI(1.0.0)',
      },
    ],
    categorySchemes: [
      {
        id: 'ABS_TOPICS',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=ABS:ABS_TOPICS(1.0.0)',
            type: 'categoryscheme',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'ABS Topics',
        names: {
          en: 'ABS 123(Top)ics وحدة',
        },
        description: 'Australian Bureau of Statistics - Topics Framework',
        descriptions: {
          en: 'Australian Bureau of Statistics - Topics Framework',
        },
        isPartial: false,
        categories: [
          {
            id: 'ECONOMY',
            name: 'ECONOMY',
            names: {
              en: 'ECONOMY',
            },
            annotations: [
              {
                id: 'CategoryScheme_node_order',
                title: 'CategoryScheme_node_order',
                type: 'CategoryScheme_node_order',
                text: '0',
                texts: {
                  en: '0',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).ECONOMY',
                type: 'category',
              },
            ],
            categories: [
              {
                id: 'PRICE_INDEX_INFLATION',
                name: 'Price Indexes and Inflation',
                names: {
                  en: 'Price Indexes and Inflation',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '0',
                    texts: {
                      en: '0',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).ECONOMY.PRICE_INDEX_INFLATION',
                    type: 'category',
                  },
                ],
              },
              {
                id: 'INTERNATIONAL_TRADE',
                name: 'International Trade',
                names: {
                  en: 'International Trade',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '1',
                    texts: {
                      en: '1',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).ECONOMY.INTERNATIONAL_TRADE',
                    type: 'category',
                  },
                ],
                categories: [
                  {
                    id: 'BOP_IIP',
                    name: 'Balance of Payments and International Investment Position',
                    names: {
                      en: '12345-Balance of Payments and International Investment Position',
                    },
                    annotations: [
                      {
                        id: 'CategoryScheme_node_order',
                        title: 'CategoryScheme_node_order',
                        type: 'CategoryScheme_node_order',
                        text: '0',
                        texts: {
                          en: '0',
                        },
                      },
                    ],
                    links: [
                      {
                        rel: 'self',
                        urn:
                          'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).ECONOMY.INTERNATIONAL_TRADE.BOP_IIP',
                        type: 'category',
                      },
                    ],
                  },
                ],
              },
              {
                id: 'BUSINESS_INDICATORS',
                name: 'Business Indicators',
                names: {
                  en: 'Business Indicators',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '2',
                    texts: {
                      en: '2',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).ECONOMY.BUSINESS_INDICATORS',
                    type: 'category',
                  },
                ],
                categories: [
                  {
                    id: 'COUNTS_OF_BUSINESSES',
                    name: 'Counts of Businesses',
                    names: {
                      en: 'Counts of Businesses',
                    },
                    annotations: [
                      {
                        id: 'CategoryScheme_node_order',
                        title: 'CategoryScheme_node_order',
                        type: 'CategoryScheme_node_order',
                        text: '0',
                        texts: {
                          en: '0',
                        },
                      },
                    ],
                    links: [
                      {
                        rel: 'self',
                        urn:
                          'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).ECONOMY.BUSINESS_INDICATORS.COUNTS_OF_BUSINESSES',
                        type: 'category',
                      },
                    ],
                  },
                ],
              },
              {
                id: 'NATIONAL_ACCOUNTS',
                name: 'National Accounts',
                names: {
                  en: 'National Accounts',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '3',
                    texts: {
                      en: '3',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).ECONOMY.NATIONAL_ACCOUNTS',
                    type: 'category',
                  },
                ],
              },
              {
                id: 'FINANCE',
                name: 'Finance',
                names: {
                  en: 'Finance',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '4',
                    texts: {
                      en: '4',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).ECONOMY.FINANCE',
                    type: 'category',
                  },
                ],
                categories: [
                  {
                    id: 'LENDING_FINANCE',
                    name: 'Lending Income',
                    names: {
                      en: 'Lending Income',
                    },
                    annotations: [
                      {
                        id: 'CategoryScheme_node_order',
                        title: 'CategoryScheme_node_order',
                        type: 'CategoryScheme_node_order',
                        text: '0',
                        texts: {
                          en: '0',
                        },
                      },
                    ],
                    links: [
                      {
                        rel: 'self',
                        urn:
                          'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).ECONOMY.FINANCE.LENDING_FINANCE',
                        type: 'category',
                      },
                    ],
                  },
                  {
                    id: 'FAMILY_INCOME',
                    name: 'Family Income',
                    names: {
                      en: 'Family Income',
                    },
                    annotations: [
                      {
                        id: 'CategoryScheme_node_order',
                        title: 'CategoryScheme_node_order',
                        type: 'CategoryScheme_node_order',
                        text: '1',
                        texts: {
                          en: '1',
                        },
                      },
                    ],
                    links: [
                      {
                        rel: 'self',
                        urn:
                          'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).ECONOMY.FINANCE.FAMILY_INCOME',
                        type: 'category',
                      },
                    ],
                  },
                  {
                    id: 'PERSONAL_INCOME',
                    name: 'Personal Income',
                    names: {
                      en: 'Personal Income',
                    },
                    annotations: [
                      {
                        id: 'CategoryScheme_node_order',
                        title: 'CategoryScheme_node_order',
                        type: 'CategoryScheme_node_order',
                        text: '2',
                        texts: {
                          en: '2',
                        },
                      },
                    ],
                    links: [
                      {
                        rel: 'self',
                        urn:
                          'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).ECONOMY.FINANCE.PERSONAL_INCOME',
                        type: 'category',
                      },
                    ],
                  },
                  {
                    id: 'HOUSEHOLD_INCOME',
                    name: 'Household Income',
                    names: {
                      en: 'Household Income',
                    },
                    annotations: [
                      {
                        id: 'CategoryScheme_node_order',
                        title: 'CategoryScheme_node_order',
                        type: 'CategoryScheme_node_order',
                        text: '3',
                        texts: {
                          en: '3',
                        },
                      },
                    ],
                    links: [
                      {
                        rel: 'self',
                        urn:
                          'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).ECONOMY.FINANCE.HOUSEHOLD_INCOME',
                        type: 'category',
                      },
                    ],
                  },
                ],
              },
              {
                id: 'INDICATORS',
                name: 'Key Economic Indicators',
                names: {
                  en: 'Key Economic Indicators',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '5',
                    texts: {
                      en: '5',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).ECONOMY.INDICATORS',
                    type: 'category',
                  },
                ],
              },
            ],
          },
          {
            id: 'INDUSTRY',
            name: 'INDUSTRY',
            names: {
              en: 'INDUSTRY',
            },
            annotations: [
              {
                id: 'CategoryScheme_node_order',
                title: 'CategoryScheme_node_order',
                type: 'CategoryScheme_node_order',
                text: '1',
                texts: {
                  en: '1',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY',
                type: 'category',
              },
            ],
            categories: [
              {
                id: 'TECHNOLOGY',
                name: 'Technology and Innovation',
                names: {
                  en: 'Technology and Innovation',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '0',
                    texts: {
                      en: '0',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY.TECHNOLOGY',
                    type: 'category',
                  },
                ],
                categories: [
                  {
                    id: 'INTERNET',
                    name: 'Internet',
                    names: {
                      en: 'Internet',
                    },
                    annotations: [
                      {
                        id: 'CategoryScheme_node_order',
                        title: 'CategoryScheme_node_order',
                        type: 'CategoryScheme_node_order',
                        text: '0',
                        texts: {
                          en: '0',
                        },
                      },
                    ],
                    links: [
                      {
                        rel: 'self',
                        urn:
                          'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY.TECHNOLOGY.INTERNET',
                        type: 'category',
                      },
                    ],
                  },
                  {
                    id: 'RESEARCH',
                    name: 'Research',
                    names: {
                      en: 'Research',
                    },
                    annotations: [
                      {
                        id: 'CategoryScheme_node_order',
                        title: 'CategoryScheme_node_order',
                        type: 'CategoryScheme_node_order',
                        text: '1',
                        texts: {
                          en: '1',
                        },
                      },
                    ],
                    links: [
                      {
                        rel: 'self',
                        urn:
                          'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY.TECHNOLOGY.RESEARCH',
                        type: 'category',
                      },
                    ],
                  },
                ],
              },
              {
                id: 'MINNING',
                name: 'Minning',
                names: {
                  en: 'Minning',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '1',
                    texts: {
                      en: '1',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY.MINNING',
                    type: 'category',
                  },
                ],
              },
              {
                id: 'BUILDING_CONSTRUCTION',
                name: 'Building and Construction',
                names: {
                  en: 'Building and Construction',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '2',
                    texts: {
                      en: '2',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY.BUILDING_CONSTRUCTION',
                    type: 'category',
                  },
                ],
                categories: [
                  {
                    id: 'ENGINEERING_CONSTRUCTION',
                    name: 'Engineering Construction',
                    names: {
                      en: 'Engineering Construction',
                    },
                    annotations: [
                      {
                        id: 'CategoryScheme_node_order',
                        title: 'CategoryScheme_node_order',
                        type: 'CategoryScheme_node_order',
                        text: '0',
                        texts: {
                          en: '0',
                        },
                      },
                    ],
                    links: [
                      {
                        rel: 'self',
                        urn:
                          'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY.BUILDING_CONSTRUCTION.ENGINEERING_CONSTRUCTION',
                        type: 'category',
                      },
                    ],
                  },
                  {
                    id: 'BUILDING_ACTIVITY',
                    name: 'Building Activity',
                    names: {
                      en: 'Building Activity',
                    },
                    annotations: [
                      {
                        id: 'CategoryScheme_node_order',
                        title: 'CategoryScheme_node_order',
                        type: 'CategoryScheme_node_order',
                        text: '1',
                        texts: {
                          en: '1',
                        },
                      },
                    ],
                    links: [
                      {
                        rel: 'self',
                        urn:
                          'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY.BUILDING_CONSTRUCTION.BUILDING_ACTIVITY',
                        type: 'category',
                      },
                    ],
                  },
                  {
                    id: 'BUILDING_APPROVALS',
                    name: 'Building Approvals',
                    names: {
                      en: 'Building Approvals',
                    },
                    annotations: [
                      {
                        id: 'CategoryScheme_node_order',
                        title: 'CategoryScheme_node_order',
                        type: 'CategoryScheme_node_order',
                        text: '2',
                        texts: {
                          en: '2',
                        },
                      },
                    ],
                    links: [
                      {
                        rel: 'self',
                        urn:
                          'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY.BUILDING_CONSTRUCTION.BUILDING_APPROVALS',
                        type: 'category',
                      },
                    ],
                  },
                ],
              },
              {
                id: 'TOURISM_TRANSPORT',
                name: 'Tourism and Transport',
                names: {
                  en: 'Tourism and Transport',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '3',
                    texts: {
                      en: '3',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY.TOURISM_TRANSPORT',
                    type: 'category',
                  },
                ],
                categories: [
                  {
                    id: 'ACCOMODATION',
                    name: 'Accomodation',
                    names: {
                      en: 'Accomodation',
                    },
                    annotations: [
                      {
                        id: 'CategoryScheme_node_order',
                        title: 'CategoryScheme_node_order',
                        type: 'CategoryScheme_node_order',
                        text: '0',
                        texts: {
                          en: '0',
                        },
                      },
                    ],
                    links: [
                      {
                        rel: 'self',
                        urn:
                          'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY.TOURISM_TRANSPORT.ACCOMODATION',
                        type: 'category',
                      },
                    ],
                  },
                  {
                    id: 'VEHICE_USAGE',
                    name: 'Vehicle Usage',
                    names: {
                      en: 'Vehicle Usage',
                    },
                    annotations: [
                      {
                        id: 'CategoryScheme_node_order',
                        title: 'CategoryScheme_node_order',
                        type: 'CategoryScheme_node_order',
                        text: '1',
                        texts: {
                          en: '1',
                        },
                      },
                    ],
                    links: [
                      {
                        rel: 'self',
                        urn:
                          'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY.TOURISM_TRANSPORT.VEHICE_USAGE',
                        type: 'category',
                      },
                    ],
                  },
                ],
              },
              {
                id: 'RETAIL_WHOLESALE',
                name: 'Retail and Wholesale Trade',
                names: {
                  en: 'Retail and Wholesale Trade',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '4',
                    texts: {
                      en: '4',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY.RETAIL_WHOLESALE',
                    type: 'category',
                  },
                ],
              },
              {
                id: 'ENERGY',
                name: 'Energy',
                names: {
                  en: 'Energy',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '5',
                    texts: {
                      en: '5',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY.ENERGY',
                    type: 'category',
                  },
                ],
              },
              {
                id: 'AGRICULTURE',
                name: 'Agriculture',
                names: {
                  en: 'Agriculture',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '6',
                    texts: {
                      en: '6',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY.AGRICULTURE',
                    type: 'category',
                  },
                ],
                categories: [
                  {
                    id: 'LAND_USE',
                    name: 'Land Use',
                    names: {
                      en: 'Land Use',
                    },
                    annotations: [
                      {
                        id: 'CategoryScheme_node_order',
                        title: 'CategoryScheme_node_order',
                        type: 'CategoryScheme_node_order',
                        text: '0',
                        texts: {
                          en: '0',
                        },
                      },
                    ],
                    links: [
                      {
                        rel: 'self',
                        urn:
                          'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY.AGRICULTURE.LAND_USE',
                        type: 'category',
                      },
                    ],
                  },
                  {
                    id: 'LAND_COVER',
                    name: 'Land Cover',
                    names: {
                      en: 'Land Cover',
                    },
                    annotations: [
                      {
                        id: 'CategoryScheme_node_order',
                        title: 'CategoryScheme_node_order',
                        type: 'CategoryScheme_node_order',
                        text: '1',
                        texts: {
                          en: '1',
                        },
                      },
                    ],
                    links: [
                      {
                        rel: 'self',
                        urn:
                          'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).INDUSTRY.AGRICULTURE.LAND_COVER',
                        type: 'category',
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            id: 'LABOUR',
            name: 'LABOUR',
            names: {
              en: 'LABOUR',
            },
            annotations: [
              {
                id: 'CategoryScheme_node_order',
                title: 'CategoryScheme_node_order',
                type: 'CategoryScheme_node_order',
                text: '2',
                texts: {
                  en: '2',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).LABOUR',
                type: 'category',
              },
            ],
            categories: [
              {
                id: 'EMPLOYMENT_UNEMPLOYMENT',
                name: 'Employment and Unemployment',
                names: {
                  en: 'Employment and Unemployment',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '0',
                    texts: {
                      en: '0',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).LABOUR.EMPLOYMENT_UNEMPLOYMENT',
                    type: 'category',
                  },
                ],
              },
              {
                id: 'INDUSTRY_EMPLOYMENT',
                name: 'Industry of Employment',
                names: {
                  en: 'Industry of Employment',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '1',
                    texts: {
                      en: '1',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).LABOUR.INDUSTRY_EMPLOYMENT',
                    type: 'category',
                  },
                ],
              },
              {
                id: 'TRAVEL',
                name: 'Travel to Work',
                names: {
                  en: 'Travel to Work',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '2',
                    texts: {
                      en: '2',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).LABOUR.TRAVEL',
                    type: 'category',
                  },
                ],
              },
              {
                id: 'EARNINGS_HOURS',
                name: 'Earnings and Work Hours',
                names: {
                  en: 'Earnings and Work Hours',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '3',
                    texts: {
                      en: '3',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).LABOUR.EARNINGS_HOURS',
                    type: 'category',
                  },
                ],
              },
              {
                id: 'UNPAID_WORK',
                name: 'Unpaid Work',
                names: {
                  en: 'Unpaid Work',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '4',
                    texts: {
                      en: '4',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).LABOUR.UNPAID_WORK',
                    type: 'category',
                  },
                ],
              },
              {
                id: 'OCCUPATION',
                name: 'Occupation',
                names: {
                  en: 'Occupation',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '5',
                    texts: {
                      en: '5',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).LABOUR.OCCUPATION',
                    type: 'category',
                  },
                ],
              },
              {
                id: 'INDUSTRIAL_DISPUTES',
                name: 'Industrial Disputes',
                names: {
                  en: 'Industrial Disputes',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '6',
                    texts: {
                      en: '6',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).LABOUR.INDUSTRIAL_DISPUTES',
                    type: 'category',
                  },
                ],
              },
              {
                id: 'LABOUR_FORCE_STATUS',
                name: 'Labour Force Status',
                names: {
                  en: 'Labour Force Status',
                },
                annotations: [
                  {
                    id: 'CategoryScheme_node_order',
                    title: 'CategoryScheme_node_order',
                    type: 'CategoryScheme_node_order',
                    text: '7',
                    texts: {
                      en: '7',
                    },
                  },
                ],
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).LABOUR.LABOUR_FORCE_STATUS',
                    type: 'category',
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
    categorisations: [
      {
        id: 'CAT_CPI_INDEX',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Categorisation=ABS:CAT_CPI_INDEX(1.0.0)',
            type: 'categorisation',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'CPI Index Categorisation',
        names: {
          en: 'CPI Index Categorisation',
        },
        source: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ABS:DF_CPI(1.0.0)',
        target:
          'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=ABS:ABS_TOPICS(1.0.0).ECONOMY.PRICE_INDEX_INFLATION',
      },
    ],
    conceptSchemes: [
      {
        id: 'CS_ATR',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=ABS:CS_ATR(1.0.0)',
            type: 'conceptscheme',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'Common Attributes',
        names: {
          en: 'Common Attributes',
        },
        isPartial: false,
        concepts: [
          {
            id: 'UNIT_MEASURE',
            name: 'Unit of measure',
            names: {
              en: 'Unit of measure',
            },
            description: 'The unit in which the data values are measured.',
            descriptions: {
              en: 'The unit in which the data values are measured.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ATR(1.0.0).UNIT_MEASURE',
                type: 'concept',
              },
            ],
          },
          {
            id: 'UNIT_MULT',
            name: 'Unit multiplier',
            names: {
              en: 'Unit multiplier',
            },
            description: 'Quantity the data is recorded in.',
            descriptions: {
              en: 'Quantity the data is recorded in.',
            },
            annotations: [
              {
                type: 'CONTEXT',
                text: 'e.g. a unit multiplier of 3 would mean that the observation repsesents thousands.',
                texts: {
                  en: 'e.g. a unit multiplier of 3 would mean that the observation repsesents thousands.',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ATR(1.0.0).UNIT_MULT',
                type: 'concept',
              },
            ],
          },
          {
            id: 'OBS_STATUS',
            name: 'Observation status',
            names: {
              en: 'Observation status',
            },
            description: 'Information on the quality of a value or an unusual or missing value.',
            descriptions: {
              en: 'Information on the quality of a value or an unusual or missing value.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ATR(1.0.0).OBS_STATUS',
                type: 'concept',
              },
            ],
          },
          {
            id: 'OBS_COMMENT',
            name: 'Observation Comment',
            names: {
              en: 'Observation Comment',
            },
            description:
              'This attribute is typically a catch all for free text annoations from the information warehouse.',
            descriptions: {
              en: 'This attribute is typically a catch all for free text annoations from the information warehouse.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ATR(1.0.0).OBS_COMMENT',
                type: 'concept',
              },
            ],
          },
          {
            id: 'BASE_PERIOD',
            name: 'Reference Base Period',
            names: {
              en: 'Reference Base Period',
            },
            description: 'The period at which the index is set as 100.0.',
            descriptions: {
              en: 'The period at which the index is set as 100.0.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ATR(1.0.0).BASE_PERIOD',
                type: 'concept',
              },
            ],
          },
          {
            id: 'REPORTING_YEAR_START_DAY',
            name: 'Reporting year start day',
            names: {
              en: 'Reporting year start day',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ATR(1.0.0).REPORTING_YEAR_START_DAY',
                type: 'concept',
              },
            ],
          },
        ],
      },
      {
        id: 'CS_COM',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=ABS:CS_COM(1.0.0)',
            type: 'conceptscheme',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'Common Concepts',
        names: {
          en: 'Common Concepts',
        },
        isPartial: false,
        concepts: [
          {
            id: 'TIME_PERIOD',
            name: 'Time period',
            names: {
              en: 'Time period',
              fr: 'Période de temps',
            },
            description: 'The period of time or point in time to which the measured observation refers.',
            descriptions: {
              en: 'The period of time or point in time to which the measured observation refers.',
            },
            annotations: [
              {
                type: 'CONTEXT',
                text:
                  'The measurement represented by each observation corresponds to a specific point in time (e.g. a single day) or a period (e.g. a month, a fiscal year, or a calendar year). This is used as a time stamp and is of particular importance for time series data. In cases where the actual time period of the data differs from the target reference period, "time period" refers to the actual period.',
                texts: {
                  en:
                    'The measurement represented by each observation corresponds to a specific point in time (e.g. a single day) or a period (e.g. a month, a fiscal year, or a calendar year). This is used as a time stamp and is of particular importance for time series data. In cases where the actual time period of the data differs from the target reference period, "time period" refers to the actual period.',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_COM(1.0.0).TIME_PERIOD',
                type: 'concept',
              },
            ],
          },
          {
            id: 'OBS_VALUE',
            name: 'Observation value',
            names: {
              en: 'Observation value',
            },
            description: 'The value of a particular variable at a particular period.',
            descriptions: {
              en: 'The value of a particular variable at a particular period.',
            },
            annotations: [
              {
                type: 'CONTEXT',
                text: 'The "observation value" is the field which holds the data.',
                texts: {
                  en: 'The "observation value" is the field which holds the data.',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_COM(1.0.0).OBS_VALUE',
                type: 'concept',
              },
            ],
          },
          {
            id: 'FREQ',
            name: 'Frequency',
            names: {
              en: 'Frequency',
            },
            description: 'The rate at which data is collated.',
            descriptions: {
              en: 'The rate at which data is collated.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_COM(1.0.0).FREQ',
                type: 'concept',
              },
            ],
          },
          {
            id: 'MEAS',
            name: 'Measure',
            names: {
              en: 'Measure',
            },
            description: 'Statistical concepts for which data is provided.',
            descriptions: {
              en: 'Statistical concepts for which data is provided.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_COM(1.0.0).MEAS',
                type: 'concept',
              },
            ],
          },
          {
            id: 'TSEST',
            name: 'Ajustment Type',
            names: {
              en: 'Ajustment Type',
            },
            description: 'The statistical method applied to time series data.',
            descriptions: {
              en: 'The statistical method applied to time series data.',
            },
            annotations: [
              {
                type: 'OTHER_LINKS',
                text: 'https://www.abs.gov.au/websitedbs/D3310114.nsf/home/Time+Series+Analysis:+The+Basics',
                texts: {
                  en: 'https://www.abs.gov.au/websitedbs/D3310114.nsf/home/Time+Series+Analysis:+The+Basics',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_COM(1.0.0).TSEST',
                type: 'concept',
              },
            ],
          },
        ],
      },
      {
        id: 'CS_ECO',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=ABS:CS_ECO(1.0.0)',
            type: 'conceptscheme',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'Economic Concepts',
        names: {
          en: 'Economic Concepts',
        },
        isPartial: false,
        concepts: [
          {
            id: 'IND',
            name: 'Industry',
            names: {
              en: 'Industry',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ECO(1.0.0).IND',
                type: 'concept',
              },
            ],
          },
          {
            id: 'SEC',
            name: 'Sector',
            names: {
              en: 'Sector',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ECO(1.0.0).SEC',
                type: 'concept',
              },
            ],
          },
          {
            id: 'OCC',
            name: 'Occupation',
            names: {
              en: 'Occupation',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ECO(1.0.0).OCC',
                type: 'concept',
              },
            ],
          },
          {
            id: 'MOCC',
            name: 'Occupation of main job',
            names: {
              en: 'Occupation of main job',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ECO(1.0.0).MOCC',
                type: 'concept',
              },
            ],
          },
          {
            id: 'LOCC',
            name: 'Occupation of last job',
            names: {
              en: 'Occupation of last job',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ECO(1.0.0).LOCC',
                type: 'concept',
              },
            ],
          },
          {
            id: 'IDX',
            name: 'Index',
            names: {
              en: 'Index',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ECO(1.0.0).IDX',
                type: 'concept',
              },
            ],
          },
          {
            id: 'DITM',
            name: 'Data Item',
            names: {
              en: 'Data Item',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ECO(1.0.0).DITM',
                type: 'concept',
              },
            ],
          },
          {
            id: 'PRD',
            name: 'Product',
            names: {
              en: 'Product',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ECO(1.0.0).PRD',
                type: 'concept',
              },
            ],
          },
          {
            id: 'COMM',
            name: 'Commodity',
            names: {
              en: 'Commodity',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ECO(1.0.0).COMM',
                type: 'concept',
              },
            ],
          },
        ],
      },
      {
        id: 'CS_GEO',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=ABS:CS_GEO(1.0.0)',
            type: 'conceptscheme',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'Geography Conepts',
        names: {
          en: 'Geography Conepts',
        },
        isPartial: false,
        concepts: [
          {
            id: 'COUNTRY',
            name: 'Country',
            names: {
              en: 'Country',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_GEO(1.0.0).COUNTRY',
                type: 'concept',
              },
            ],
          },
          {
            id: 'REGION',
            name: 'Region',
            names: {
              en: 'Region',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_GEO(1.0.0).REGION',
                type: 'concept',
              },
            ],
          },
          {
            id: 'STATE',
            name: 'State',
            names: {
              en: 'State',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_GEO(1.0.0).STATE',
                type: 'concept',
              },
            ],
          },
        ],
      },
    ],
    codelists: [
      {
        id: 'CL_BASE_PERIOD',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ABS:CL_BASE_PERIOD(1.0.0)',
            type: 'codelist',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'Reference Base Period',
        names: {
          en: 'Reference Base Period',
        },
        isPartial: false,
        codes: [
          {
            id: '1',
            name: '2000-01 = 100.0',
            names: {
              en: '2000-01 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).1',
                type: 'code',
              },
            ],
          },
          {
            id: '2',
            name: '2001-02 = 100.0',
            names: {
              en: '2001-02 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).2',
                type: 'code',
              },
            ],
          },
          {
            id: '3',
            name: '2002-03 = 100.0',
            names: {
              en: '2002-03 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).3',
                type: 'code',
              },
            ],
          },
          {
            id: '4',
            name: '2003-04 = 100.0',
            names: {
              en: '2003-04 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).4',
                type: 'code',
              },
            ],
          },
          {
            id: '5',
            name: '2004-05 = 100.0',
            names: {
              en: '2004-05 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).5',
                type: 'code',
              },
            ],
          },
          {
            id: '6',
            name: '2005-06 = 100.0',
            names: {
              en: '2005-06 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).6',
                type: 'code',
              },
            ],
          },
          {
            id: '7',
            name: '2006-07 = 100.0',
            names: {
              en: '2006-07 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).7',
                type: 'code',
              },
            ],
          },
          {
            id: '8',
            name: '2007-08 = 100.0',
            names: {
              en: '2007-08 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).8',
                type: 'code',
              },
            ],
          },
          {
            id: '9',
            name: '2008-09 = 100.0',
            names: {
              en: '2008-09 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).9',
                type: 'code',
              },
            ],
          },
          {
            id: '10',
            name: '2009-10 = 100.0',
            names: {
              en: '2009-10 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).10',
                type: 'code',
              },
            ],
          },
          {
            id: '11',
            name: '2010-11 = 100.0',
            names: {
              en: '2010-11 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).11',
                type: 'code',
              },
            ],
          },
          {
            id: '12',
            name: '2011-12 = 100.0',
            names: {
              en: '2011-12 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).12',
                type: 'code',
              },
            ],
          },
          {
            id: '13',
            name: '2012-13 = 100.0',
            names: {
              en: '2012-13 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).13',
                type: 'code',
              },
            ],
          },
          {
            id: '14',
            name: '2013-14 = 100.0',
            names: {
              en: '2013-14 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).14',
                type: 'code',
              },
            ],
          },
          {
            id: '15',
            name: '2014-15 = 100.0',
            names: {
              en: '2014-15 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).15',
                type: 'code',
              },
            ],
          },
          {
            id: '16',
            name: '2015-16 = 100.0',
            names: {
              en: '2015-16 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).16',
                type: 'code',
              },
            ],
          },
          {
            id: '17',
            name: '2016-17 = 100.0',
            names: {
              en: '2016-17 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).17',
                type: 'code',
              },
            ],
          },
          {
            id: '18',
            name: '2017-18 = 100.0',
            names: {
              en: '2017-18 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).18',
                type: 'code',
              },
            ],
          },
          {
            id: '19',
            name: '2018-19 = 100.0',
            names: {
              en: '2018-19 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).19',
                type: 'code',
              },
            ],
          },
          {
            id: '20',
            name: '2019-20 = 100.0',
            names: {
              en: '2019-20 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).20',
                type: 'code',
              },
            ],
          },
          {
            id: '21',
            name: '2020-21 = 100.0',
            names: {
              en: '2020-21 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).21',
                type: 'code',
              },
            ],
          },
          {
            id: '22',
            name: '2021-22 = 100.0',
            names: {
              en: '2021-22 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).22',
                type: 'code',
              },
            ],
          },
          {
            id: '23',
            name: '2022-23 = 100.0',
            names: {
              en: '2022-23 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).23',
                type: 'code',
              },
            ],
          },
          {
            id: '24',
            name: '2023-24 = 100.0',
            names: {
              en: '2023-24 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).24',
                type: 'code',
              },
            ],
          },
          {
            id: '25',
            name: '2024-25 = 100.0',
            names: {
              en: '2024-25 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).25',
                type: 'code',
              },
            ],
          },
          {
            id: '26',
            name: '2025-26 = 100.0',
            names: {
              en: '2025-26 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).26',
                type: 'code',
              },
            ],
          },
          {
            id: '27',
            name: '2026-27 = 100.0',
            names: {
              en: '2026-27 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).27',
                type: 'code',
              },
            ],
          },
          {
            id: '28',
            name: '2027-28 = 100.0',
            names: {
              en: '2027-28 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).28',
                type: 'code',
              },
            ],
          },
          {
            id: '29',
            name: '2028-29 = 100.0',
            names: {
              en: '2028-29 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).29',
                type: 'code',
              },
            ],
          },
          {
            id: '30',
            name: '2029-30 = 100.0',
            names: {
              en: '2029-30 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).30',
                type: 'code',
              },
            ],
          },
          {
            id: '31',
            name: '2030-31 = 100.0',
            names: {
              en: '2030-31 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).31',
                type: 'code',
              },
            ],
          },
          {
            id: '32',
            name: '2031-32 = 100.0',
            names: {
              en: '2031-32 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).32',
                type: 'code',
              },
            ],
          },
          {
            id: '33',
            name: '2032-33 = 100.0',
            names: {
              en: '2032-33 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).33',
                type: 'code',
              },
            ],
          },
          {
            id: '34',
            name: '2033-34 = 100.0',
            names: {
              en: '2033-34 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).34',
                type: 'code',
              },
            ],
          },
          {
            id: '35',
            name: '2034-35 = 100.0',
            names: {
              en: '2034-35 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).35',
                type: 'code',
              },
            ],
          },
          {
            id: '36',
            name: '2035-36 = 100.0',
            names: {
              en: '2035-36 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).36',
                type: 'code',
              },
            ],
          },
          {
            id: '37',
            name: '2036-37 = 100.0',
            names: {
              en: '2036-37 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).37',
                type: 'code',
              },
            ],
          },
          {
            id: '38',
            name: '2037-38 = 100.0',
            names: {
              en: '2037-38 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).38',
                type: 'code',
              },
            ],
          },
          {
            id: '39',
            name: '2038-39 = 100.0',
            names: {
              en: '2038-39 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).39',
                type: 'code',
              },
            ],
          },
          {
            id: '40',
            name: '2039-40 = 100.0',
            names: {
              en: '2039-40 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).40',
                type: 'code',
              },
            ],
          },
          {
            id: '41',
            name: '2040-41 = 100.0',
            names: {
              en: '2040-41 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).41',
                type: 'code',
              },
            ],
          },
          {
            id: '42',
            name: '2041-42 = 100.0',
            names: {
              en: '2041-42 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).42',
                type: 'code',
              },
            ],
          },
          {
            id: '43',
            name: '2042-43 = 100.0',
            names: {
              en: '2042-43 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).43',
                type: 'code',
              },
            ],
          },
          {
            id: '44',
            name: '2043-44 = 100.0',
            names: {
              en: '2043-44 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).44',
                type: 'code',
              },
            ],
          },
          {
            id: '45',
            name: '2044-45 = 100.0',
            names: {
              en: '2044-45 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).45',
                type: 'code',
              },
            ],
          },
          {
            id: '46',
            name: '2045-46 = 100.0',
            names: {
              en: '2045-46 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).46',
                type: 'code',
              },
            ],
          },
          {
            id: '47',
            name: '2046-47 = 100.0',
            names: {
              en: '2046-47 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).47',
                type: 'code',
              },
            ],
          },
          {
            id: '48',
            name: '2047-48 = 100.0',
            names: {
              en: '2047-48 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).48',
                type: 'code',
              },
            ],
          },
          {
            id: '49',
            name: '2048-49 = 100.0',
            names: {
              en: '2048-49 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).49',
                type: 'code',
              },
            ],
          },
          {
            id: '50',
            name: '2049-50 = 100.0',
            names: {
              en: '2049-50 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).50',
                type: 'code',
              },
            ],
          },
          {
            id: '51',
            name: '2050-51 = 100.0',
            names: {
              en: '2050-51 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).51',
                type: 'code',
              },
            ],
          },
          {
            id: '52',
            name: '2051-52 = 100.0',
            names: {
              en: '2051-52 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).52',
                type: 'code',
              },
            ],
          },
          {
            id: '53',
            name: '2052-53 = 100.0',
            names: {
              en: '2052-53 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).53',
                type: 'code',
              },
            ],
          },
          {
            id: '54',
            name: '2053-54 = 100.0',
            names: {
              en: '2053-54 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).54',
                type: 'code',
              },
            ],
          },
          {
            id: '55',
            name: '2054-55 = 100.0',
            names: {
              en: '2054-55 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).55',
                type: 'code',
              },
            ],
          },
          {
            id: '56',
            name: '2055-56 = 100.0',
            names: {
              en: '2055-56 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).56',
                type: 'code',
              },
            ],
          },
          {
            id: '57',
            name: '2056-57 = 100.0',
            names: {
              en: '2056-57 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).57',
                type: 'code',
              },
            ],
          },
          {
            id: '58',
            name: '2057-58 = 100.0',
            names: {
              en: '2057-58 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).58',
                type: 'code',
              },
            ],
          },
          {
            id: '59',
            name: '2058-59 = 100.0',
            names: {
              en: '2058-59 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).59',
                type: 'code',
              },
            ],
          },
          {
            id: '60',
            name: '2059-60 = 100.0',
            names: {
              en: '2059-60 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).60',
                type: 'code',
              },
            ],
          },
          {
            id: '61',
            name: '2060-61 = 100.0',
            names: {
              en: '2060-61 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).61',
                type: 'code',
              },
            ],
          },
          {
            id: '62',
            name: '2061-62 = 100.0',
            names: {
              en: '2061-62 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).62',
                type: 'code',
              },
            ],
          },
          {
            id: '63',
            name: '2062-63 = 100.0',
            names: {
              en: '2062-63 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).63',
                type: 'code',
              },
            ],
          },
          {
            id: '64',
            name: '2063-64 = 100.0',
            names: {
              en: '2063-64 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).64',
                type: 'code',
              },
            ],
          },
          {
            id: '65',
            name: '2064-65 = 100.0',
            names: {
              en: '2064-65 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).65',
                type: 'code',
              },
            ],
          },
          {
            id: '66',
            name: '2065-66 = 100.0',
            names: {
              en: '2065-66 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).66',
                type: 'code',
              },
            ],
          },
          {
            id: '67',
            name: '2066-67 = 100.0',
            names: {
              en: '2066-67 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).67',
                type: 'code',
              },
            ],
          },
          {
            id: '68',
            name: '2067-68 = 100.0',
            names: {
              en: '2067-68 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).68',
                type: 'code',
              },
            ],
          },
          {
            id: '69',
            name: '2068-69 = 100.0',
            names: {
              en: '2068-69 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).69',
                type: 'code',
              },
            ],
          },
          {
            id: '70',
            name: '2069-70 = 100.0',
            names: {
              en: '2069-70 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).70',
                type: 'code',
              },
            ],
          },
          {
            id: '71',
            name: '2070-71 = 100.0',
            names: {
              en: '2070-71 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).71',
                type: 'code',
              },
            ],
          },
          {
            id: '72',
            name: '2071-72 = 100.0',
            names: {
              en: '2071-72 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).72',
                type: 'code',
              },
            ],
          },
          {
            id: '73',
            name: '2072-73 = 100.0',
            names: {
              en: '2072-73 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).73',
                type: 'code',
              },
            ],
          },
          {
            id: '74',
            name: '2073-74 = 100.0',
            names: {
              en: '2073-74 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).74',
                type: 'code',
              },
            ],
          },
          {
            id: '75',
            name: '2074-75 = 100.0',
            names: {
              en: '2074-75 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).75',
                type: 'code',
              },
            ],
          },
          {
            id: '76',
            name: '2075-76 = 100.0',
            names: {
              en: '2075-76 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).76',
                type: 'code',
              },
            ],
          },
          {
            id: '77',
            name: '2076-77 = 100.0',
            names: {
              en: '2076-77 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).77',
                type: 'code',
              },
            ],
          },
          {
            id: '78',
            name: '2077-78 = 100.0',
            names: {
              en: '2077-78 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).78',
                type: 'code',
              },
            ],
          },
          {
            id: '79',
            name: '2078-79 = 100.0',
            names: {
              en: '2078-79 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).79',
                type: 'code',
              },
            ],
          },
          {
            id: '80',
            name: '2079-80 = 100.0',
            names: {
              en: '2079-80 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).80',
                type: 'code',
              },
            ],
          },
          {
            id: '81',
            name: '2080-81 = 100.0',
            names: {
              en: '2080-81 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).81',
                type: 'code',
              },
            ],
          },
          {
            id: '82',
            name: '2081-82 = 100.0',
            names: {
              en: '2081-82 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).82',
                type: 'code',
              },
            ],
          },
          {
            id: '83',
            name: '2082-83 = 100.0',
            names: {
              en: '2082-83 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).83',
                type: 'code',
              },
            ],
          },
          {
            id: '84',
            name: '2083-84 = 100.0',
            names: {
              en: '2083-84 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).84',
                type: 'code',
              },
            ],
          },
          {
            id: '85',
            name: '2084-85 = 100.0',
            names: {
              en: '2084-85 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).85',
                type: 'code',
              },
            ],
          },
          {
            id: '86',
            name: '2085-86 = 100.0',
            names: {
              en: '2085-86 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).86',
                type: 'code',
              },
            ],
          },
          {
            id: '87',
            name: '2086-87 = 100.0',
            names: {
              en: '2086-87 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).87',
                type: 'code',
              },
            ],
          },
          {
            id: '88',
            name: '2087-88 = 100.0',
            names: {
              en: '2087-88 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).88',
                type: 'code',
              },
            ],
          },
          {
            id: '89',
            name: '2088-89 = 100.0',
            names: {
              en: '2088-89 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).89',
                type: 'code',
              },
            ],
          },
          {
            id: '90',
            name: '2089-90 = 100.0',
            names: {
              en: '2089-90 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).90',
                type: 'code',
              },
            ],
          },
          {
            id: '91',
            name: '2090-91 = 100.0',
            names: {
              en: '2090-91 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).91',
                type: 'code',
              },
            ],
          },
          {
            id: '92',
            name: '2091-92 = 100.0',
            names: {
              en: '2091-92 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).92',
                type: 'code',
              },
            ],
          },
          {
            id: '93',
            name: '2092-93 = 100.0',
            names: {
              en: '2092-93 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).93',
                type: 'code',
              },
            ],
          },
          {
            id: '94',
            name: '2093-94 = 100.0',
            names: {
              en: '2093-94 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).94',
                type: 'code',
              },
            ],
          },
          {
            id: '95',
            name: '2094-95 = 100.0',
            names: {
              en: '2094-95 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).95',
                type: 'code',
              },
            ],
          },
          {
            id: '96',
            name: '2095-96 = 100.0',
            names: {
              en: '2095-96 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).96',
                type: 'code',
              },
            ],
          },
          {
            id: '97',
            name: '2096-97 = 100.0',
            names: {
              en: '2096-97 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).97',
                type: 'code',
              },
            ],
          },
          {
            id: '98',
            name: '2097-98 = 100.0',
            names: {
              en: '2097-98 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).98',
                type: 'code',
              },
            ],
          },
          {
            id: '99',
            name: '2098-99 = 100.0',
            names: {
              en: '2098-99 = 100.0',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_BASE_PERIOD(1.0.0).99',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_CPI_INDEX_17',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ABS:CL_CPI_INDEX_17(1.0.0)',
            type: 'codelist',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'CPI 17th Series Index',
        names: {
          en: 'CPI 17th Series Index',
        },
        description: 'Consumer Price Index 17th Series Index',
        descriptions: {
          en: 'Consumer Price Index 17th Series Index',
        },
        isPartial: false,
        codes: [
          {
            id: '10001',
            name: 'All groups CPI',
            names: {
              en: 'All groups CPI',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '10',
                texts: {
                  en: '10',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).10001',
                type: 'code',
              },
            ],
          },
          {
            id: '20001',
            name: 'Food and non-alcoholic beverages',
            names: {
              en: 'Food and non-alcoholic beverages',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '710',
                texts: {
                  en: '710',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).20001',
                type: 'code',
              },
            ],
            parent: '10001',
          },
          {
            id: '30002',
            name: 'Bread and cereal products',
            names: {
              en: 'Bread and cereal products',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '780',
                texts: {
                  en: '780',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).30002',
                type: 'code',
              },
            ],
            parent: '20001',
          },
          {
            id: '40005',
            name: 'Bread',
            names: {
              en: 'Bread',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '930',
                texts: {
                  en: '930',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40005',
                type: 'code',
              },
            ],
            parent: '30002',
          },
          {
            id: '40006',
            name: 'Cakes and biscuits',
            names: {
              en: 'Cakes and biscuits',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '940',
                texts: {
                  en: '940',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40006',
                type: 'code',
              },
            ],
            parent: '30002',
          },
          {
            id: '40007',
            name: 'Breakfast cereals',
            names: {
              en: 'Breakfast cereals',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '950',
                texts: {
                  en: '950',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40007',
                type: 'code',
              },
            ],
            parent: '30002',
          },
          {
            id: '40008',
            name: 'Other cereal products',
            names: {
              en: 'Other cereal products',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '960',
                texts: {
                  en: '960',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40008',
                type: 'code',
              },
            ],
            parent: '30002',
          },
          {
            id: '30003',
            name: 'Meat and seafoods',
            names: {
              en: 'Meat and seafoods',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '790',
                texts: {
                  en: '790',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).30003',
                type: 'code',
              },
            ],
            parent: '20001',
          },
          {
            id: '40009',
            name: 'Beef and veal',
            names: {
              en: 'Beef and veal',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '970',
                texts: {
                  en: '970',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40009',
                type: 'code',
              },
            ],
            parent: '30003',
          },
          {
            id: '131178',
            name: 'Pork',
            names: {
              en: 'Pork',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '500',
                texts: {
                  en: '500',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131178',
                type: 'code',
              },
            ],
            parent: '30003',
          },
          {
            id: '40010',
            name: 'Lamb and goat',
            names: {
              en: 'Lamb and goat',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '980',
                texts: {
                  en: '980',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40010',
                type: 'code',
              },
            ],
            parent: '30003',
          },
          {
            id: '40012',
            name: 'Poultry',
            names: {
              en: 'Poultry',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '990',
                texts: {
                  en: '990',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40012',
                type: 'code',
              },
            ],
            parent: '30003',
          },
          {
            id: '40014',
            name: 'Other meats',
            names: {
              en: 'Other meats',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1000',
                texts: {
                  en: '1000',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40014',
                type: 'code',
              },
            ],
            parent: '30003',
          },
          {
            id: '40015',
            name: 'Fish and other seafood',
            names: {
              en: 'Fish and other seafood',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1010',
                texts: {
                  en: '1010',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40015',
                type: 'code',
              },
            ],
            parent: '30003',
          },
          {
            id: '30001',
            name: 'Dairy and related products',
            names: {
              en: 'Dairy and related products',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '770',
                texts: {
                  en: '770',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).30001',
                type: 'code',
              },
            ],
            parent: '20001',
          },
          {
            id: '40001',
            name: 'Milk',
            names: {
              en: 'Milk',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '900',
                texts: {
                  en: '900',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40001',
                type: 'code',
              },
            ],
            parent: '30001',
          },
          {
            id: '40002',
            name: 'Cheese',
            names: {
              en: 'Cheese',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '910',
                texts: {
                  en: '910',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40002',
                type: 'code',
              },
            ],
            parent: '30001',
          },
          {
            id: '40004',
            name: 'Ice cream and other dairy products',
            names: {
              en: 'Ice cream and other dairy products',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '920',
                texts: {
                  en: '920',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40004',
                type: 'code',
              },
            ],
            parent: '30001',
          },
          {
            id: '114120',
            name: 'Fruit and vegetables',
            names: {
              en: 'Fruit and vegetables',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '130',
                texts: {
                  en: '130',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).114120',
                type: 'code',
              },
            ],
            parent: '20001',
          },
          {
            id: '114121',
            name: 'Fruit',
            names: {
              en: 'Fruit',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '140',
                texts: {
                  en: '140',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).114121',
                type: 'code',
              },
            ],
            parent: '114120',
          },
          {
            id: '114122',
            name: 'Vegetables',
            names: {
              en: 'Vegetables',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '150',
                texts: {
                  en: '150',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).114122',
                type: 'code',
              },
            ],
            parent: '114120',
          },
          {
            id: '131179',
            name: 'Food products n.e.c.',
            names: {
              en: 'Food products n.e.c.',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '510',
                texts: {
                  en: '510',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131179',
                type: 'code',
              },
            ],
            parent: '20001',
          },
          {
            id: '40027',
            name: 'Eggs',
            names: {
              en: 'Eggs',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1040',
                texts: {
                  en: '1040',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40027',
                type: 'code',
              },
            ],
            parent: '131179',
          },
          {
            id: '40029',
            name: 'Jams, honey and spreads',
            names: {
              en: 'Jams, honey and spreads',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1050',
                texts: {
                  en: '1050',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40029',
                type: 'code',
              },
            ],
            parent: '131179',
          },
          {
            id: '97549',
            name: 'Food additives and condiments',
            names: {
              en: 'Food additives and condiments',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1420',
                texts: {
                  en: '1420',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97549',
                type: 'code',
              },
            ],
            parent: '131179',
          },
          {
            id: '97550',
            name: 'Oils and fats',
            names: {
              en: 'Oils and fats',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1430',
                texts: {
                  en: '1430',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97550',
                type: 'code',
              },
            ],
            parent: '131179',
          },
          {
            id: '115501',
            name: 'Snacks and confectionery',
            names: {
              en: 'Snacks and confectionery',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '290',
                texts: {
                  en: '290',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115501',
                type: 'code',
              },
            ],
            parent: '131179',
          },
          {
            id: '40034',
            name: 'Other food products n.e.c.',
            names: {
              en: 'Other food products n.e.c.',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1070',
                texts: {
                  en: '1070',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40034',
                type: 'code',
              },
            ],
            parent: '131179',
          },
          {
            id: '131180',
            name: 'Non-alcoholic beverages',
            names: {
              en: 'Non-alcoholic beverages',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '520',
                texts: {
                  en: '520',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131180',
                type: 'code',
              },
            ],
            parent: '20001',
          },
          {
            id: '40030',
            name: 'Coffee, tea and cocoa',
            names: {
              en: 'Coffee, tea and cocoa',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1060',
                texts: {
                  en: '1060',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40030',
                type: 'code',
              },
            ],
            parent: '131180',
          },
          {
            id: '115520',
            name: 'Waters, soft drinks and juices',
            names: {
              en: 'Waters, soft drinks and juices',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '300',
                texts: {
                  en: '300',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115520',
                type: 'code',
              },
            ],
            parent: '131180',
          },
          {
            id: '30007',
            name: 'Meals out and take away foods',
            names: {
              en: 'Meals out and take away foods',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '800',
                texts: {
                  en: '800',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).30007',
                type: 'code',
              },
            ],
            parent: '20001',
          },
          {
            id: '40025',
            name: 'Restaurant meals',
            names: {
              en: 'Restaurant meals',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1020',
                texts: {
                  en: '1020',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40025',
                type: 'code',
              },
            ],
            parent: '30007',
          },
          {
            id: '40026',
            name: 'Take away and fast foods',
            names: {
              en: 'Take away and fast foods',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1030',
                texts: {
                  en: '1030',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40026',
                type: 'code',
              },
            ],
            parent: '30007',
          },
          {
            id: '20006',
            name: 'Alcohol and tobacco',
            names: {
              en: 'Alcohol and tobacco',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '760',
                texts: {
                  en: '760',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).20006',
                type: 'code',
              },
            ],
            parent: '10001',
          },
          {
            id: '30026',
            name: 'Alcoholic beverages',
            names: {
              en: 'Alcoholic beverages',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '870',
                texts: {
                  en: '870',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).30026',
                type: 'code',
              },
            ],
            parent: '20006',
          },
          {
            id: '40089',
            name: 'Spirits',
            names: {
              en: 'Spirits',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1300',
                texts: {
                  en: '1300',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40089',
                type: 'code',
              },
            ],
            parent: '30026',
          },
          {
            id: '40088',
            name: 'Wine',
            names: {
              en: 'Wine',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1290',
                texts: {
                  en: '1290',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40088',
                type: 'code',
              },
            ],
            parent: '30026',
          },
          {
            id: '40087',
            name: 'Beer',
            names: {
              en: 'Beer',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1280',
                texts: {
                  en: '1280',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40087',
                type: 'code',
              },
            ],
            parent: '30026',
          },
          {
            id: '30027',
            name: 'Tobacco',
            names: {
              en: 'Tobacco',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '880',
                texts: {
                  en: '880',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).30027',
                type: 'code',
              },
            ],
            parent: '20006',
          },
          {
            id: '40090',
            name: 'Tobacco',
            names: {
              en: 'Tobacco',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1310',
                texts: {
                  en: '1310',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40090',
                type: 'code',
              },
            ],
            parent: '30027',
          },
          {
            id: '20002',
            name: 'Clothing and footwear',
            names: {
              en: 'Clothing and footwear',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '720',
                texts: {
                  en: '720',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).20002',
                type: 'code',
              },
            ],
            parent: '10001',
          },
          {
            id: '131181',
            name: 'Garments',
            names: {
              en: 'Garments',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '530',
                texts: {
                  en: '530',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131181',
                type: 'code',
              },
            ],
            parent: '20002',
          },
          {
            id: '97551',
            name: 'Garments for men',
            names: {
              en: 'Garments for men',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1440',
                texts: {
                  en: '1440',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97551',
                type: 'code',
              },
            ],
            parent: '131181',
          },
          {
            id: '97554',
            name: 'Garments for women',
            names: {
              en: 'Garments for women',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1450',
                texts: {
                  en: '1450',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97554',
                type: 'code',
              },
            ],
            parent: '131181',
          },
          {
            id: '97555',
            name: 'Garments for infants and children',
            names: {
              en: 'Garments for infants and children',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1460',
                texts: {
                  en: '1460',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97555',
                type: 'code',
              },
            ],
            parent: '131181',
          },
          {
            id: '30012',
            name: 'Footwear',
            names: {
              en: 'Footwear',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '810',
                texts: {
                  en: '810',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).30012',
                type: 'code',
              },
            ],
            parent: '20002',
          },
          {
            id: '40045',
            name: 'Footwear for men',
            names: {
              en: 'Footwear for men',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1080',
                texts: {
                  en: '1080',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40045',
                type: 'code',
              },
            ],
            parent: '30012',
          },
          {
            id: '40046',
            name: 'Footwear for women',
            names: {
              en: 'Footwear for women',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1090',
                texts: {
                  en: '1090',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40046',
                type: 'code',
              },
            ],
            parent: '30012',
          },
          {
            id: '40047',
            name: 'Footwear for infants and children',
            names: {
              en: 'Footwear for infants and children',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1100',
                texts: {
                  en: '1100',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40047',
                type: 'code',
              },
            ],
            parent: '30012',
          },
          {
            id: '97556',
            name: 'Accessories and clothing services',
            names: {
              en: 'Accessories and clothing services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1470',
                texts: {
                  en: '1470',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97556',
                type: 'code',
              },
            ],
            parent: '20002',
          },
          {
            id: '97557',
            name: 'Accessories',
            names: {
              en: 'Accessories',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1480',
                texts: {
                  en: '1480',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97557',
                type: 'code',
              },
            ],
            parent: '97556',
          },
          {
            id: '40048',
            name: 'Cleaning, repair and hire of clothing and footwear',
            names: {
              en: 'Cleaning, repair and hire of clothing and footwear',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1110',
                texts: {
                  en: '1110',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40048',
                type: 'code',
              },
            ],
            parent: '97556',
          },
          {
            id: '20003',
            name: 'Housing',
            names: {
              en: 'Housing',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '730',
                texts: {
                  en: '730',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).20003',
                type: 'code',
              },
            ],
            parent: '10001',
          },
          {
            id: '115522',
            name: 'Rents',
            names: {
              en: 'Rents',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '310',
                texts: {
                  en: '310',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115522',
                type: 'code',
              },
            ],
            parent: '20003',
          },
          {
            id: '30014',
            name: 'Rents',
            names: {
              en: 'Rents',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '820',
                texts: {
                  en: '820',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).30014',
                type: 'code',
              },
            ],
            parent: '115522',
          },
          {
            id: '131186',
            name: 'New dwelling purchase by owner-occupiers',
            names: {
              en: 'New dwelling purchase by owner-occupiers',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '580',
                texts: {
                  en: '580',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131186',
                type: 'code',
              },
            ],
            parent: '20003',
          },
          {
            id: '97559',
            name: 'New dwelling purchase by owner-occupiers',
            names: {
              en: 'New dwelling purchase by owner-occupiers',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1500',
                texts: {
                  en: '1500',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97559',
                type: 'code',
              },
            ],
            parent: '131186',
          },
          {
            id: '131187',
            name: 'Other housing',
            names: {
              en: 'Other housing',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '590',
                texts: {
                  en: '590',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131187',
                type: 'code',
              },
            ],
            parent: '20003',
          },
          {
            id: '40053',
            name: 'Maintenance and repair of the dwelling',
            names: {
              en: 'Maintenance and repair of the dwelling',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1120',
                texts: {
                  en: '1120',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40053',
                type: 'code',
              },
            ],
            parent: '131187',
          },
          {
            id: '97560',
            name: 'Property rates and charges',
            names: {
              en: 'Property rates and charges',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1510',
                texts: {
                  en: '1510',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97560',
                type: 'code',
              },
            ],
            parent: '131187',
          },
          {
            id: '30016',
            name: 'Utilities',
            names: {
              en: 'Utilities',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '830',
                texts: {
                  en: '830',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).30016',
                type: 'code',
              },
            ],
            parent: '20003',
          },
          {
            id: '97558',
            name: 'Water and sewerage',
            names: {
              en: 'Water and sewerage',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1490',
                texts: {
                  en: '1490',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97558',
                type: 'code',
              },
            ],
            parent: '30016',
          },
          {
            id: '40055',
            name: 'Electricity',
            names: {
              en: 'Electricity',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1130',
                texts: {
                  en: '1130',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40055',
                type: 'code',
              },
            ],
            parent: '30016',
          },
          {
            id: '115524',
            name: 'Gas and other household fuels',
            names: {
              en: 'Gas and other household fuels',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '320',
                texts: {
                  en: '320',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115524',
                type: 'code',
              },
            ],
            parent: '30016',
          },
          {
            id: '20004',
            name: 'Furnishings, household equipment and services',
            names: {
              en: 'Furnishings, household equipment and services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '740',
                texts: {
                  en: '740',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).20004',
                type: 'code',
              },
            ],
            parent: '10001',
          },
          {
            id: '131184',
            name: 'Furniture and furnishings',
            names: {
              en: 'Furniture and furnishings',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '560',
                texts: {
                  en: '560',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131184',
                type: 'code',
              },
            ],
            parent: '20004',
          },
          {
            id: '40058',
            name: 'Furniture',
            names: {
              en: 'Furniture',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1140',
                texts: {
                  en: '1140',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40058',
                type: 'code',
              },
            ],
            parent: '131184',
          },
          {
            id: '131185',
            name: 'Carpets and other floor coverings',
            names: {
              en: 'Carpets and other floor coverings',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '570',
                texts: {
                  en: '570',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131185',
                type: 'code',
              },
            ],
            parent: '131184',
          },
          {
            id: '131182',
            name: 'Household textiles',
            names: {
              en: 'Household textiles',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '540',
                texts: {
                  en: '540',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131182',
                type: 'code',
              },
            ],
            parent: '20004',
          },
          {
            id: '131183',
            name: 'Household textiles',
            names: {
              en: 'Household textiles',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '550',
                texts: {
                  en: '550',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131183',
                type: 'code',
              },
            ],
            parent: '131182',
          },
          {
            id: '97561',
            name: 'Household appliances, utensils and tools',
            names: {
              en: 'Household appliances, utensils and tools',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1520',
                texts: {
                  en: '1520',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97561',
                type: 'code',
              },
            ],
            parent: '20004',
          },
          {
            id: '40060',
            name: 'Major household appliances',
            names: {
              en: 'Major household appliances',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1150',
                texts: {
                  en: '1150',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40060',
                type: 'code',
              },
            ],
            parent: '97561',
          },
          {
            id: '115484',
            name: 'Small electric household appliances',
            names: {
              en: 'Small electric household appliances',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '170',
                texts: {
                  en: '170',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115484',
                type: 'code',
              },
            ],
            parent: '97561',
          },
          {
            id: '115485',
            name: 'Glassware, tableware and household utensils',
            names: {
              en: 'Glassware, tableware and household utensils',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '180',
                texts: {
                  en: '180',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115485',
                type: 'code',
              },
            ],
            parent: '97561',
          },
          {
            id: '40066',
            name: 'Tools and equipment for house and garden',
            names: {
              en: 'Tools and equipment for house and garden',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1160',
                texts: {
                  en: '1160',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40066',
                type: 'code',
              },
            ],
            parent: '97561',
          },
          {
            id: '97563',
            name: 'Non-durable household products',
            names: {
              en: 'Non-durable household products',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1530',
                texts: {
                  en: '1530',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97563',
                type: 'code',
              },
            ],
            parent: '20004',
          },
          {
            id: '40067',
            name: 'Cleaning and maintenance products',
            names: {
              en: 'Cleaning and maintenance products',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1170',
                texts: {
                  en: '1170',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40067',
                type: 'code',
              },
            ],
            parent: '97563',
          },
          {
            id: '40095',
            name: 'Personal care products',
            names: {
              en: 'Personal care products',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1360',
                texts: {
                  en: '1360',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40095',
                type: 'code',
              },
            ],
            parent: '97563',
          },
          {
            id: '97564',
            name: 'Other non-durable household products',
            names: {
              en: 'Other non-durable household products',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1540',
                texts: {
                  en: '1540',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97564',
                type: 'code',
              },
            ],
            parent: '97563',
          },
          {
            id: '97565',
            name: 'Domestic and household services',
            names: {
              en: 'Domestic and household services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1550',
                texts: {
                  en: '1550',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97565',
                type: 'code',
              },
            ],
            parent: '20004',
          },
          {
            id: '115498',
            name: 'Child care',
            names: {
              en: 'Child care',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '270',
                texts: {
                  en: '270',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115498',
                type: 'code',
              },
            ],
            parent: '97565',
          },
          {
            id: '40096',
            name: 'Hairdressing and personal grooming services',
            names: {
              en: 'Hairdressing and personal grooming services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1370',
                texts: {
                  en: '1370',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40096',
                type: 'code',
              },
            ],
            parent: '97565',
          },
          {
            id: '115500',
            name: 'Other household services',
            names: {
              en: 'Other household services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '280',
                texts: {
                  en: '280',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115500',
                type: 'code',
              },
            ],
            parent: '97565',
          },
          {
            id: '115486',
            name: 'Health',
            names: {
              en: 'Health',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '190',
                texts: {
                  en: '190',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115486',
                type: 'code',
              },
            ],
            parent: '10001',
          },
          {
            id: '131188',
            name: 'Medical products, appliances and equipment',
            names: {
              en: 'Medical products, appliances and equipment',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '600',
                texts: {
                  en: '600',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131188',
                type: 'code',
              },
            ],
            parent: '115486',
          },
          {
            id: '40094',
            name: 'Pharmaceutical products',
            names: {
              en: 'Pharmaceutical products',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1350',
                texts: {
                  en: '1350',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40094',
                type: 'code',
              },
            ],
            parent: '131188',
          },
          {
            id: '40092',
            name: 'Therapeutic appliances and equipment',
            names: {
              en: 'Therapeutic appliances and equipment',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1330',
                texts: {
                  en: '1330',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40092',
                type: 'code',
              },
            ],
            parent: '131188',
          },
          {
            id: '131189',
            name: 'Medical, dental and hospital services',
            names: {
              en: 'Medical, dental and hospital services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '610',
                texts: {
                  en: '610',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131189',
                type: 'code',
              },
            ],
            parent: '115486',
          },
          {
            id: '40091',
            name: 'Medical and hospital services',
            names: {
              en: 'Medical and hospital services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1320',
                texts: {
                  en: '1320',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40091',
                type: 'code',
              },
            ],
            parent: '131189',
          },
          {
            id: '40093',
            name: 'Dental services',
            names: {
              en: 'Dental services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1340',
                texts: {
                  en: '1340',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40093',
                type: 'code',
              },
            ],
            parent: '131189',
          },
          {
            id: '20005',
            name: 'Transport',
            names: {
              en: 'Transport',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '750',
                texts: {
                  en: '750',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).20005',
                type: 'code',
              },
            ],
            parent: '10001',
          },
          {
            id: '30024',
            name: 'Private motoring',
            names: {
              en: 'Private motoring',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '850',
                texts: {
                  en: '850',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).30024',
                type: 'code',
              },
            ],
            parent: '20005',
          },
          {
            id: '40080',
            name: 'Motor vehicles',
            names: {
              en: 'Motor vehicles',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1220',
                texts: {
                  en: '1220',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40080',
                type: 'code',
              },
            ],
            parent: '30024',
          },
          {
            id: '40084',
            name: 'Spare parts and accessories for motor vehicles',
            names: {
              en: 'Spare parts and accessories for motor vehicles',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1250',
                texts: {
                  en: '1250',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40084',
                type: 'code',
              },
            ],
            parent: '30024',
          },
          {
            id: '40081',
            name: 'Automotive fuel',
            names: {
              en: 'Automotive fuel',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1230',
                texts: {
                  en: '1230',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40081',
                type: 'code',
              },
            ],
            parent: '30024',
          },
          {
            id: '40085',
            name: 'Maintenance and repair of motor vehicles',
            names: {
              en: 'Maintenance and repair of motor vehicles',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1260',
                texts: {
                  en: '1260',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40085',
                type: 'code',
              },
            ],
            parent: '30024',
          },
          {
            id: '40083',
            name: 'Other services in respect of motor vehicles',
            names: {
              en: 'Other services in respect of motor vehicles',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1240',
                texts: {
                  en: '1240',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40083',
                type: 'code',
              },
            ],
            parent: '30024',
          },
          {
            id: '30025',
            name: 'Urban transport fares',
            names: {
              en: 'Urban transport fares',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '860',
                texts: {
                  en: '860',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).30025',
                type: 'code',
              },
            ],
            parent: '20005',
          },
          {
            id: '40086',
            name: 'Urban transport fares',
            names: {
              en: 'Urban transport fares',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1270',
                texts: {
                  en: '1270',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40086',
                type: 'code',
              },
            ],
            parent: '30025',
          },
          {
            id: '115488',
            name: 'Communication',
            names: {
              en: 'Communication',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '200',
                texts: {
                  en: '200',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115488',
                type: 'code',
              },
            ],
            parent: '10001',
          },
          {
            id: '30022',
            name: 'Communication',
            names: {
              en: 'Communication',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '840',
                texts: {
                  en: '840',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).30022',
                type: 'code',
              },
            ],
            parent: '115488',
          },
          {
            id: '40077',
            name: 'Postal services',
            names: {
              en: 'Postal services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1200',
                texts: {
                  en: '1200',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40077',
                type: 'code',
              },
            ],
            parent: '30022',
          },
          {
            id: '40078',
            name: 'Telecommunication equipment and services',
            names: {
              en: 'Telecommunication equipment and services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1210',
                texts: {
                  en: '1210',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40078',
                type: 'code',
              },
            ],
            parent: '30022',
          },
          {
            id: '115489',
            name: 'Recreation and culture',
            names: {
              en: 'Recreation and culture',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '210',
                texts: {
                  en: '210',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115489',
                type: 'code',
              },
            ],
            parent: '10001',
          },
          {
            id: '131193',
            name: 'Audio, visual and computing equipment and services',
            names: {
              en: 'Audio, visual and computing equipment and services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '650',
                texts: {
                  en: '650',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131193',
                type: 'code',
              },
            ],
            parent: '115489',
          },
          {
            id: '40098',
            name: 'Audio, visual and computing equipment',
            names: {
              en: 'Audio, visual and computing equipment',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1380',
                texts: {
                  en: '1380',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40098',
                type: 'code',
              },
            ],
            parent: '131193',
          },
          {
            id: '131190',
            name: 'Audio, visual and computing media and services',
            names: {
              en: 'Audio, visual and computing media and services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '620',
                texts: {
                  en: '620',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131190',
                type: 'code',
              },
            ],
            parent: '131193',
          },
          {
            id: '131191',
            name: 'Newspapers, books and stationery',
            names: {
              en: 'Newspapers, books and stationery',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '630',
                texts: {
                  en: '630',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131191',
                type: 'code',
              },
            ],
            parent: '115489',
          },
          {
            id: '97567',
            name: 'Books',
            names: {
              en: 'Books',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1560',
                texts: {
                  en: '1560',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97567',
                type: 'code',
              },
            ],
            parent: '131191',
          },
          {
            id: '131192',
            name: 'Newspapers, magazines and stationery',
            names: {
              en: 'Newspapers, magazines and stationery',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '640',
                texts: {
                  en: '640',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131192',
                type: 'code',
              },
            ],
            parent: '131191',
          },
          {
            id: '30033',
            name: 'Holiday travel and accommodation',
            names: {
              en: 'Holiday travel and accommodation',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '890',
                texts: {
                  en: '890',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).30033',
                type: 'code',
              },
            ],
            parent: '115489',
          },
          {
            id: '40101',
            name: 'Domestic holiday travel and accommodation',
            names: {
              en: 'Domestic holiday travel and accommodation',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1390',
                texts: {
                  en: '1390',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40101',
                type: 'code',
              },
            ],
            parent: '30033',
          },
          {
            id: '40102',
            name: 'International holiday travel and accommodation',
            names: {
              en: 'International holiday travel and accommodation',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1400',
                texts: {
                  en: '1400',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40102',
                type: 'code',
              },
            ],
            parent: '30033',
          },
          {
            id: '115492',
            name: 'Other recreation, sport and culture',
            names: {
              en: 'Other recreation, sport and culture',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '220',
                texts: {
                  en: '220',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115492',
                type: 'code',
              },
            ],
            parent: '115489',
          },
          {
            id: '97571',
            name: 'Equipment for sports, camping and open-air recreation',
            names: {
              en: 'Equipment for sports, camping and open-air recreation',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1570',
                texts: {
                  en: '1570',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97571',
                type: 'code',
              },
            ],
            parent: '115492',
          },
          {
            id: '97572',
            name: 'Games, toys and hobbies',
            names: {
              en: 'Games, toys and hobbies',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1580',
                texts: {
                  en: '1580',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97572',
                type: 'code',
              },
            ],
            parent: '115492',
          },
          {
            id: '40073',
            name: 'Pets and related products',
            names: {
              en: 'Pets and related products',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1190',
                texts: {
                  en: '1190',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40073',
                type: 'code',
              },
            ],
            parent: '115492',
          },
          {
            id: '40072',
            name: 'Veterinary and other services for pets',
            names: {
              en: 'Veterinary and other services for pets',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1180',
                texts: {
                  en: '1180',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40072',
                type: 'code',
              },
            ],
            parent: '115492',
          },
          {
            id: '97573',
            name: 'Sports participation',
            names: {
              en: 'Sports participation',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1590',
                texts: {
                  en: '1590',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97573',
                type: 'code',
              },
            ],
            parent: '115492',
          },
          {
            id: '97574',
            name: 'Other recreational, sporting and cultural services',
            names: {
              en: 'Other recreational, sporting and cultural services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1600',
                texts: {
                  en: '1600',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).97574',
                type: 'code',
              },
            ],
            parent: '115492',
          },
          {
            id: '115493',
            name: 'Education',
            names: {
              en: 'Education',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '230',
                texts: {
                  en: '230',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115493',
                type: 'code',
              },
            ],
            parent: '10001',
          },
          {
            id: '40106',
            name: 'Education',
            names: {
              en: 'Education',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1410',
                texts: {
                  en: '1410',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).40106',
                type: 'code',
              },
            ],
            parent: '115493',
          },
          {
            id: '115495',
            name: 'Preschool and primary education',
            names: {
              en: 'Preschool and primary education',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '240',
                texts: {
                  en: '240',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115495',
                type: 'code',
              },
            ],
            parent: '40106',
          },
          {
            id: '115496',
            name: 'Secondary education',
            names: {
              en: 'Secondary education',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '250',
                texts: {
                  en: '250',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115496',
                type: 'code',
              },
            ],
            parent: '40106',
          },
          {
            id: '115497',
            name: 'Tertiary education',
            names: {
              en: 'Tertiary education',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '260',
                texts: {
                  en: '260',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115497',
                type: 'code',
              },
            ],
            parent: '40106',
          },
          {
            id: '126670',
            name: 'Insurance and financial services',
            names: {
              en: 'Insurance and financial services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '470',
                texts: {
                  en: '470',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).126670',
                type: 'code',
              },
            ],
            parent: '10001',
          },
          {
            id: '115528',
            name: 'Insurance',
            names: {
              en: 'Insurance',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '330',
                texts: {
                  en: '330',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115528',
                type: 'code',
              },
            ],
            parent: '126670',
          },
          {
            id: '115529',
            name: 'Insurance',
            names: {
              en: 'Insurance',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '340',
                texts: {
                  en: '340',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115529',
                type: 'code',
              },
            ],
            parent: '115528',
          },
          {
            id: '131195',
            name: 'Financial services',
            names: {
              en: 'Financial services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '670',
                texts: {
                  en: '670',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131195',
                type: 'code',
              },
            ],
            parent: '126670',
          },
          {
            id: '131194',
            name: 'Deposit and loan facilities (direct charges)',
            names: {
              en: 'Deposit and loan facilities (direct charges)',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '660',
                texts: {
                  en: '660',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131194',
                type: 'code',
              },
            ],
            parent: '131195',
          },
          {
            id: '1144',
            name: 'Other financial services',
            names: {
              en: 'Other financial services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '160',
                texts: {
                  en: '160',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).1144',
                type: 'code',
              },
            ],
            parent: '131195',
          },
          {
            id: '115901',
            name: 'All groups CPI excluding',
            names: {
              en: 'All groups CPI excluding',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '350',
                texts: {
                  en: '350',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115901',
                type: 'code',
              },
            ],
          },
          {
            id: '115902',
            name: 'All groups CPI excluding Food and non-alcoholic beverages',
            names: {
              en: 'All groups CPI excluding Food and non-alcoholic beverages',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '360',
                texts: {
                  en: '360',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115902',
                type: 'code',
              },
            ],
            parent: '115901',
          },
          {
            id: '115922',
            name: 'All groups CPI excluding Alcohol and tobacco',
            names: {
              en: 'All groups CPI excluding Alcohol and tobacco',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '370',
                texts: {
                  en: '370',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115922',
                type: 'code',
              },
            ],
            parent: '115901',
          },
          {
            id: '115941',
            name: 'All groups CPI excluding Clothing and footwear',
            names: {
              en: 'All groups CPI excluding Clothing and footwear',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '380',
                texts: {
                  en: '380',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115941',
                type: 'code',
              },
            ],
            parent: '115901',
          },
          {
            id: '115961',
            name: 'All groups CPI excluding Housing',
            names: {
              en: 'All groups CPI excluding Housing',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '390',
                texts: {
                  en: '390',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).115961',
                type: 'code',
              },
            ],
            parent: '115901',
          },
          {
            id: '117101',
            name: 'All groups CPI excluding Furnishings, household equipment and services',
            names: {
              en: 'All groups CPI excluding Furnishings, household equipment and services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '400',
                texts: {
                  en: '400',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).117101',
                type: 'code',
              },
            ],
            parent: '115901',
          },
          {
            id: '117104',
            name: 'All groups CPI excluding Health',
            names: {
              en: 'All groups CPI excluding Health',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '410',
                texts: {
                  en: '410',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).117104',
                type: 'code',
              },
            ],
            parent: '115901',
          },
          {
            id: '117107',
            name: 'All groups CPI excluding Transport',
            names: {
              en: 'All groups CPI excluding Transport',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '420',
                texts: {
                  en: '420',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).117107',
                type: 'code',
              },
            ],
            parent: '115901',
          },
          {
            id: '117121',
            name: 'All groups CPI excluding Communication',
            names: {
              en: 'All groups CPI excluding Communication',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '430',
                texts: {
                  en: '430',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).117121',
                type: 'code',
              },
            ],
            parent: '115901',
          },
          {
            id: '117124',
            name: 'All groups CPI excluding Recreation and culture',
            names: {
              en: 'All groups CPI excluding Recreation and culture',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '440',
                texts: {
                  en: '440',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).117124',
                type: 'code',
              },
            ],
            parent: '115901',
          },
          {
            id: '117127',
            name: 'All groups CPI excluding Education',
            names: {
              en: 'All groups CPI excluding Education',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '450',
                texts: {
                  en: '450',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).117127',
                type: 'code',
              },
            ],
            parent: '115901',
          },
          {
            id: '128058',
            name: 'All groups CPI excluding Insurance and financial services',
            names: {
              en: 'All groups CPI excluding Insurance and financial services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '480',
                texts: {
                  en: '480',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).128058',
                type: 'code',
              },
            ],
            parent: '115901',
          },
          {
            id: '128061',
            name: 'All groups CPI excluding Housing and Insurance and financial services',
            names: {
              en: 'All groups CPI excluding Housing and Insurance and financial services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '490',
                texts: {
                  en: '490',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).128061',
                type: 'code',
              },
            ],
            parent: '115901',
          },
          {
            id: '117144',
            name: 'All groups CPI excluding Medical and hospital services',
            names: {
              en: 'All groups CPI excluding Medical and hospital services',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '460',
                texts: {
                  en: '460',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).117144',
                type: 'code',
              },
            ],
            parent: '115901',
          },
          {
            id: '104122',
            name: "All groups CPI excluding 'volatile items'",
            names: {
              en: "All groups CPI excluding 'volatile items'",
            },
            annotations: [
              {
                type: 'ORDER',
                text: '120',
                texts: {
                  en: '120',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).104122',
                type: 'code',
              },
            ],
            parent: '115901',
          },
          {
            id: '131197',
            name: 'All groups CPI excluding food and energy',
            names: {
              en: 'All groups CPI excluding food and energy',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '680',
                texts: {
                  en: '680',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131197',
                type: 'code',
              },
            ],
            parent: '115901',
          },
          {
            id: '131198',
            name: 'All groups CPI including',
            names: {
              en: 'All groups CPI including',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '690',
                texts: {
                  en: '690',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131198',
                type: 'code',
              },
            ],
          },
          {
            id: '131199',
            name: 'All groups CPI including deposit and loan facilities (indirect charges)',
            names: {
              en: 'All groups CPI including deposit and loan facilities (indirect charges)',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '700',
                texts: {
                  en: '700',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).131199',
                type: 'code',
              },
            ],
            parent: '131198',
          },
          {
            id: '104099',
            name: "Market goods and services excluding 'volatile items'",
            names: {
              en: "Market goods and services excluding 'volatile items'",
            },
            annotations: [
              {
                type: 'ORDER',
                text: '50',
                texts: {
                  en: '50',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).104099',
                type: 'code',
              },
            ],
          },
          {
            id: '104102',
            name: "Market goods and services excluding 'volatile items' - Goods",
            names: {
              en: "Market goods and services excluding 'volatile items' - Goods",
            },
            annotations: [
              {
                type: 'ORDER',
                text: '80',
                texts: {
                  en: '80',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).104102',
                type: 'code',
              },
            ],
            parent: '104099',
          },
          {
            id: '104105',
            name: "Market goods and services excluding 'volatile items' - Services",
            names: {
              en: "Market goods and services excluding 'volatile items' - Services",
            },
            annotations: [
              {
                type: 'ORDER',
                text: '100',
                texts: {
                  en: '100',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).104105',
                type: 'code',
              },
            ],
            parent: '104099',
          },
          {
            id: '104120',
            name: "Market goods and services excluding 'volatile items' - Total",
            names: {
              en: "Market goods and services excluding 'volatile items' - Total",
            },
            annotations: [
              {
                type: 'ORDER',
                text: '110',
                texts: {
                  en: '110',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).104120',
                type: 'code',
              },
            ],
            parent: '104099',
          },
          {
            id: '104100',
            name: 'Goods and services series',
            names: {
              en: 'Goods and services series',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '60',
                texts: {
                  en: '60',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).104100',
                type: 'code',
              },
            ],
          },
          {
            id: '104101',
            name: 'All groups, goods component',
            names: {
              en: 'All groups, goods component',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '70',
                texts: {
                  en: '70',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).104101',
                type: 'code',
              },
            ],
            parent: '104100',
          },
          {
            id: '104104',
            name: 'All groups, services component',
            names: {
              en: 'All groups, services component',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '90',
                texts: {
                  en: '90',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).104104',
                type: 'code',
              },
            ],
            parent: '104100',
          },
          {
            id: '102674',
            name: 'International trade exposure series',
            names: {
              en: 'International trade exposure series',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '20',
                texts: {
                  en: '20',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).102674',
                type: 'code',
              },
            ],
          },
          {
            id: '102675',
            name: 'Tradables',
            names: {
              en: 'Tradables',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '30',
                texts: {
                  en: '30',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).102675',
                type: 'code',
              },
            ],
            parent: '102674',
          },
          {
            id: '102676',
            name: 'Non-tradables',
            names: {
              en: 'Non-tradables',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '40',
                texts: {
                  en: '40',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).102676',
                type: 'code',
              },
            ],
            parent: '102674',
          },
          {
            id: '999900',
            name: 'Underlying trend series',
            names: {
              en: 'Underlying trend series',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1610',
                texts: {
                  en: '1610',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).999900',
                type: 'code',
              },
            ],
          },
          {
            id: '999901',
            name: 'All groups CPI, seasonally adjusted',
            names: {
              en: 'All groups CPI, seasonally adjusted',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1620',
                texts: {
                  en: '1620',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).999901',
                type: 'code',
              },
            ],
            parent: '999900',
          },
          {
            id: '999902',
            name: 'Trimmed Mean',
            names: {
              en: 'Trimmed Mean',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1630',
                texts: {
                  en: '1630',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).999902',
                type: 'code',
              },
            ],
            parent: '999900',
          },
          {
            id: '999903',
            name: 'Weighted Median',
            names: {
              en: 'Weighted Median',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1640',
                texts: {
                  en: '1640',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_INDEX_17(1.0.0).999903',
                type: 'code',
              },
            ],
            parent: '999900',
          },
        ],
      },
      {
        id: 'CL_CPI_MEASURES',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ABS:CL_CPI_MEASURES(1.0.0)',
            type: 'codelist',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'CPI Measures',
        names: {
          en: 'CPI Measures',
        },
        isPartial: false,
        codes: [
          {
            id: '1',
            name: 'Index Numbers',
            names: {
              en: 'Index Numbers',
            },
            description: 'Index Numbers',
            descriptions: {
              en: 'Index Numbers',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1',
                texts: {
                  en: '1',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Index Numbers',
                texts: {
                  en: 'Index Numbers',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_MEASURES(1.0.0).1',
                type: 'code',
              },
            ],
          },
          {
            id: '2',
            name: 'Percentage Change from Previous Period',
            names: {
              en: 'Percentage Change from Previous Period',
            },
            description: 'Percentage Change from Previous Period',
            descriptions: {
              en: 'Percentage Change from Previous Period',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '2',
                texts: {
                  en: '2',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Percentage Change from Previous Period',
                texts: {
                  en: 'Percentage Change from Previous Period',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_MEASURES(1.0.0).2',
                type: 'code',
              },
            ],
          },
          {
            id: '3',
            name: 'Percentage Change from Corresponding Quarter of the Previous Year',
            names: {
              en: 'Percentage Change from Corresponding Quarter of the Previous Year',
            },
            description: 'Percentage Change from Corresponding Quarter of the Previous Year',
            descriptions: {
              en: 'Percentage Change from Corresponding Quarter of the Previous Year',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '3',
                texts: {
                  en: '3',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Percentage Change from Corresponding Quarter of the Previous Year',
                texts: {
                  en: 'Percentage Change from Corresponding Quarter of the Previous Year',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_MEASURES(1.0.0).3',
                type: 'code',
              },
            ],
          },
          {
            id: '4',
            name: 'Contribution to Total CPI',
            names: {
              en: 'Contribution to Total CPI',
            },
            description: 'Contribution to Total CPI',
            descriptions: {
              en: 'Contribution to Total CPI',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '4',
                texts: {
                  en: '4',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Contribution to Total CPI',
                texts: {
                  en: 'Contribution to Total CPI',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_MEASURES(1.0.0).4',
                type: 'code',
              },
            ],
          },
          {
            id: '5',
            name: 'Change in Contribution to Total CPI',
            names: {
              en: 'Change in Contribution to Total CPI',
            },
            description: 'Change in Contribution to Total CPI',
            descriptions: {
              en: 'Change in Contribution to Total CPI',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '5',
                texts: {
                  en: '5',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Change in Contribution to Total CPI',
                texts: {
                  en: 'Change in Contribution to Total CPI',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_MEASURES(1.0.0).5',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_CPI_REGION',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ABS:CL_CPI_REGION(1.0.0)',
            type: 'codelist',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'CPI Region; Capital cities and International countries',
        names: {
          en: 'CPI Region; Capital cities and International countries',
        },
        isPartial: false,
        codes: [
          {
            id: '50',
            name: 'Weighted average of eight capital cities',
            names: {
              en: 'Weighted average of eight capital cities',
            },
            description: 'Weighted average of eight capital cities',
            descriptions: {
              en: 'Weighted average of eight capital cities',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '10',
                texts: {
                  en: '10',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Australia',
                texts: {
                  en: 'Australia',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_REGION(1.0.0).50',
                type: 'code',
              },
            ],
          },
          {
            id: '1',
            name: 'Sydney',
            names: {
              en: 'Sydney',
            },
            description: 'Sydney',
            descriptions: {
              en: 'Sydney',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1',
                texts: {
                  en: '1',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Sydney',
                texts: {
                  en: 'Sydney',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_REGION(1.0.0).1',
                type: 'code',
              },
            ],
            parent: '50',
          },
          {
            id: '5011',
            name: 'Sydney downtown',
            names: {
              en: 'Sydney downtown',
            },
            description: 'Sydney downtown',
            descriptions: {
              en: 'Sydney downtown',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1',
                texts: {
                  en: '1',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Sydney downtown',
                texts: {
                  en: 'Sydney downtown',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_REGION(1.0.0).5011',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: '2',
            name: 'Melbourne',
            names: {
              en: 'Melbourne',
            },
            description: 'Melbourne',
            descriptions: {
              en: 'Melbourne',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '2',
                texts: {
                  en: '2',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Melbourne',
                texts: {
                  en: 'Melbourne',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_REGION(1.0.0).2',
                type: 'code',
              },
            ],
            parent: '50',
          },
          {
            id: '3',
            name: 'Brisbane',
            names: {
              en: 'Brisbane',
            },
            description: 'Brisbane',
            descriptions: {
              en: 'Brisbane',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '3',
                texts: {
                  en: '3',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Brisbane',
                texts: {
                  en: 'Brisbane',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_REGION(1.0.0).3',
                type: 'code',
              },
            ],
            parent: '50',
          },
          {
            id: '4',
            name: 'Adelaide',
            names: {
              en: 'Adelaide',
            },
            description: 'Adelaide',
            descriptions: {
              en: 'Adelaide',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '4',
                texts: {
                  en: '4',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Adelaide',
                texts: {
                  en: 'Adelaide',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_REGION(1.0.0).4',
                type: 'code',
              },
            ],
            parent: '50',
          },
          {
            id: '5',
            name: 'Perth',
            names: {
              en: 'Perth',
            },
            description: 'Perth',
            descriptions: {
              en: 'Perth',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '5',
                texts: {
                  en: '5',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Perth',
                texts: {
                  en: 'Perth',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_REGION(1.0.0).5',
                type: 'code',
              },
            ],
            parent: '50',
          },
          {
            id: '6',
            name: 'Hobart',
            names: {
              en: 'Hobart',
            },
            description: 'Hobart',
            descriptions: {
              en: 'Hobart',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '6',
                texts: {
                  en: '6',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Hobart',
                texts: {
                  en: 'Hobart',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_REGION(1.0.0).6',
                type: 'code',
              },
            ],
            parent: '50',
          },
          {
            id: '7',
            name: 'Darwin',
            names: {
              en: 'Darwin',
            },
            description: 'Darwin',
            descriptions: {
              en: 'Darwin',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '7',
                texts: {
                  en: '7',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Darwin',
                texts: {
                  en: 'Darwin',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_REGION(1.0.0).7',
                type: 'code',
              },
            ],
            parent: '50',
          },
          {
            id: '8',
            name: 'Canberra',
            names: {
              en: 'Canberra',
            },
            description: 'Canberra',
            descriptions: {
              en: 'Canberra',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '8',
                texts: {
                  en: '8',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Canberra',
                texts: {
                  en: 'Canberra',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_CPI_REGION(1.0.0).8',
                type: 'code',
              },
            ],
            parent: '50',
          },
        ],
      },
      {
        id: 'CL_FREQ',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ABS:CL_FREQ(1.0.0)',
            type: 'codelist',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'Frequency',
        names: {
          en: 'Frequency',
        },
        isPartial: false,
        codes: [
          {
            id: 'H',
            name: 'Hourly',
            names: {
              en: 'Hourly',
            },
            description: 'To be used for data collected or disseminated every hour.',
            descriptions: {
              en: 'To be used for data collected or disseminated every hour.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_FREQ(1.0.0).H',
                type: 'code',
              },
            ],
          },
          {
            id: 'D',
            name: 'Daily',
            names: {
              en: 'Daily',
            },
            description: 'To be used for data collected or disseminated every day.',
            descriptions: {
              en: 'To be used for data collected or disseminated every day.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_FREQ(1.0.0).D',
                type: 'code',
              },
            ],
          },
          {
            id: 'N',
            name: 'Minutely',
            names: {
              en: 'Minutely',
            },
            description:
              'While N denotes "minutely", usually, there may be no observations every minute (for several series the frequency is usually "irregular" within a day/days). And though observations may be sparse (not collected or disseminated every minute), missing values do not need to be given for the minutes when no observations exist: in any case the time stamp determines when an observation is observed.',
            descriptions: {
              en:
                'While N denotes "minutely", usually, there may be no observations every minute (for several series the frequency is usually "irregular" within a day/days). And though observations may be sparse (not collected or disseminated every minute), missing values do not need to be given for the minutes when no observations exist: in any case the time stamp determines when an observation is observed.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_FREQ(1.0.0).N',
                type: 'code',
              },
            ],
          },
          {
            id: 'B',
            name: 'Daily or businessweek',
            names: {
              en: 'Daily or businessweek',
            },
            description:
              'Similar to "daily", however there are no observations for Saturdays and Sundays (so, neither "missing values" nor "numeric values" should be provided for Saturday and Sunday). This treatment ("business") is one way to deal with such cases, but it is not the only option. Such a time series could alternatively be considered daily ("D"), thus, with missing values in the weekend.',
            descriptions: {
              en:
                'Similar to "daily", however there are no observations for Saturdays and Sundays (so, neither "missing values" nor "numeric values" should be provided for Saturday and Sunday). This treatment ("business") is one way to deal with such cases, but it is not the only option. Such a time series could alternatively be considered daily ("D"), thus, with missing values in the weekend.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_FREQ(1.0.0).B',
                type: 'code',
              },
            ],
          },
          {
            id: 'W',
            name: 'Weekly',
            names: {
              en: 'Weekly',
            },
            description: 'To be used for data collected or disseminated every week.',
            descriptions: {
              en: 'To be used for data collected or disseminated every week.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_FREQ(1.0.0).W',
                type: 'code',
              },
            ],
          },
          {
            id: 'S',
            name: 'Half-yearly, semester',
            names: {
              en: 'Half-yearly, semester',
            },
            description: 'To be used for data collected or disseminated every semester.',
            descriptions: {
              en: 'To be used for data collected or disseminated every semester.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_FREQ(1.0.0).S',
                type: 'code',
              },
            ],
          },
          {
            id: 'A',
            name: 'Annual',
            names: {
              en: 'Annual',
            },
            description: 'To be used for data collected or disseminated every year.',
            descriptions: {
              en: 'To be used for data collected or disseminated every year.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_FREQ(1.0.0).A',
                type: 'code',
              },
            ],
          },
          {
            id: 'M',
            name: 'Monthly',
            names: {
              en: 'Monthly',
            },
            description: 'To be used for data collected or disseminated every month.',
            descriptions: {
              en: 'To be used for data collected or disseminated every month.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_FREQ(1.0.0).M',
                type: 'code',
              },
            ],
          },
          {
            id: 'Q',
            name: 'Quarterly',
            names: {
              en: 'Quarterly',
            },
            description: 'To be used for data collected or disseminated every quarter.',
            descriptions: {
              en: 'To be used for data collected or disseminated every quarter.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_FREQ(1.0.0).Q',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_OBS_STATUS',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ABS:CL_OBS_STATUS(1.0.0)',
            type: 'codelist',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'Observation Status',
        names: {
          en: 'Observation Status',
        },
        isPartial: false,
        codes: [
          {
            id: 'b',
            name: 'Break in series between this quarter and preceding quarter',
            names: {
              en: 'Break in series between this quarter and preceding quarter',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_OBS_STATUS(1.0.0).b',
                type: 'code',
              },
            ],
          },
          {
            id: 'n',
            name: 'not available for publication but included in totals where applicable, unless otherwise indicated',
            names: {
              en: 'not available for publication but included in totals where applicable, unless otherwise indicated',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_OBS_STATUS(1.0.0).n',
                type: 'code',
              },
            ],
          },
          {
            id: 'o',
            name: 'nil or rounded to zero, including null cells',
            names: {
              en: 'nil or rounded to zero, including null cells',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_OBS_STATUS(1.0.0).o',
                type: 'code',
              },
            ],
          },
          {
            id: 'p',
            name: 'preliminary figure or series subject to revision',
            names: {
              en: 'preliminary figure or series subject to revision',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_OBS_STATUS(1.0.0).p',
                type: 'code',
              },
            ],
          },
          {
            id: 'q',
            name: 'not available',
            names: {
              en: 'not available',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_OBS_STATUS(1.0.0).q',
                type: 'code',
              },
            ],
          },
          {
            id: 'r',
            name: 'revised',
            names: {
              en: 'revised',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_OBS_STATUS(1.0.0).r',
                type: 'code',
              },
            ],
          },
          {
            id: 's',
            name: 'not currently available due to break in time series',
            names: {
              en: 'not currently available due to break in time series',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_OBS_STATUS(1.0.0).s',
                type: 'code',
              },
            ],
          },
          {
            id: 't',
            name: 'not yet available',
            names: {
              en: 'not yet available',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_OBS_STATUS(1.0.0).t',
                type: 'code',
              },
            ],
          },
          {
            id: 'u',
            name: 'not applicable',
            names: {
              en: 'not applicable',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_OBS_STATUS(1.0.0).u',
                type: 'code',
              },
            ],
          },
          {
            id: 'v',
            name: 'not available for publication but included in totals where applicable, unless otherwise indicated',
            names: {
              en: 'not available for publication but included in totals where applicable, unless otherwise indicated',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_OBS_STATUS(1.0.0).v',
                type: 'code',
              },
            ],
          },
          {
            id: 'w',
            name: 'estimate is subject to sampling variability too high for most practical purposes',
            names: {
              en: 'estimate is subject to sampling variability too high for most practical purposes',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_OBS_STATUS(1.0.0).w',
                type: 'code',
              },
            ],
          },
          {
            id: 'x',
            name: 'estimate has a relative standard error of 10% to less than 25% and should be used with caution',
            names: {
              en: 'estimate has a relative standard error of 10% to less than 25% and should be used with caution',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_OBS_STATUS(1.0.0).x',
                type: 'code',
              },
            ],
          },
          {
            id: 'y',
            name: 'estimate has a relative standard error between 25% and 50% and should be used with caution',
            names: {
              en: 'estimate has a relative standard error between 25% and 50% and should be used with caution',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_OBS_STATUS(1.0.0).y',
                type: 'code',
              },
            ],
          },
          {
            id: 'z',
            name:
              'estimate has a relative standard error greater than 50% and is considered too unreliable for general use',
            names: {
              en:
                'estimate has a relative standard error greater than 50% and is considered too unreliable for general use',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_OBS_STATUS(1.0.0).z',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_TSEST',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ABS:CL_TSEST(1.0.0)',
            type: 'codelist',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'Time Series Estimates',
        names: {
          en: 'Time Series Estimates',
        },
        isPartial: false,
        codes: [
          {
            id: '10',
            name: 'Original',
            names: {
              en: 'Original',
            },
            description: 'Original',
            descriptions: {
              en: 'Original',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '1',
                texts: {
                  en: '1',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Original',
                texts: {
                  en: 'Original',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_TSEST(1.0.0).10',
                type: 'code',
              },
            ],
          },
          {
            id: '20',
            name: 'Seasonally Adjusted',
            names: {
              en: 'Seasonally Adjusted',
            },
            description: 'Seasonally Adjusted',
            descriptions: {
              en: 'Seasonally Adjusted',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '2',
                texts: {
                  en: '2',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Seasonally Adjusted',
                texts: {
                  en: 'Seasonally Adjusted',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_TSEST(1.0.0).20',
                type: 'code',
              },
            ],
          },
          {
            id: '30',
            name: 'Trend',
            names: {
              en: 'Trend',
            },
            description: 'Trend',
            descriptions: {
              en: 'Trend',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '3',
                texts: {
                  en: '3',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Trend',
                texts: {
                  en: 'Trend',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_TSEST(1.0.0).30',
                type: 'code',
              },
            ],
          },
          {
            id: '40',
            name: 'Sensitivity Upper',
            names: {
              en: 'Sensitivity Upper',
            },
            description: 'Sensitivity Upper',
            descriptions: {
              en: 'Sensitivity Upper',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '4',
                texts: {
                  en: '4',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Sensitivity Upper',
                texts: {
                  en: 'Sensitivity Upper',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_TSEST(1.0.0).40',
                type: 'code',
              },
            ],
          },
          {
            id: '50',
            name: 'Sensitivity Lower',
            names: {
              en: 'Sensitivity Lower',
            },
            description: 'Sensitivity Lower',
            descriptions: {
              en: 'Sensitivity Lower',
            },
            annotations: [
              {
                type: 'ORDER',
                text: '5',
                texts: {
                  en: '5',
                },
              },
              {
                type: 'FULL_NAME',
                text: 'Sensitivity Lower',
                texts: {
                  en: 'Sensitivity Lower',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_TSEST(1.0.0).50',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_UNIT_MEASURE',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ABS:CL_UNIT_MEASURE(1.0.0)',
            type: 'codelist',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'Unit Measure',
        names: {
          en: 'Unit Measure',
        },
        isPartial: false,
        codes: [
          {
            id: 'AUD',
            name: 'Australian Dollars',
            names: {
              en: 'Australian Dollars',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_UNIT_MEASURE(1.0.0).AUD',
                type: 'code',
              },
            ],
          },
          {
            id: 'IN',
            name: 'Index Numbers',
            names: {
              en: 'Index Numbers',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_UNIT_MEASURE(1.0.0).IN',
                type: 'code',
              },
            ],
          },
          {
            id: 'IP',
            name: 'Index Points',
            names: {
              en: 'Index Points',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_UNIT_MEASURE(1.0.0).IP',
                type: 'code',
              },
            ],
          },
          {
            id: 'NUM',
            name: 'Number',
            names: {
              en: 'Number',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_UNIT_MEASURE(1.0.0).NUM',
                type: 'code',
              },
            ],
          },
          {
            id: 'PCT',
            name: 'Percent',
            names: {
              en: 'Percent',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_UNIT_MEASURE(1.0.0).PCT',
                type: 'code',
              },
            ],
          },
          {
            id: 'RATE',
            name: 'Rate',
            names: {
              en: 'Rate',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_UNIT_MEASURE(1.0.0).RATE',
                type: 'code',
              },
            ],
          },
          {
            id: 'RATIO',
            name: 'Ratio',
            names: {
              en: 'Ratio',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_UNIT_MEASURE(1.0.0).RATIO',
                type: 'code',
              },
            ],
          },
          {
            id: 'YEARS',
            name: 'Years',
            names: {
              en: 'Years',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_UNIT_MEASURE(1.0.0).YEARS',
                type: 'code',
              },
            ],
          },
          {
            id: 'HR',
            name: 'Hours',
            names: {
              en: 'Hours',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_UNIT_MEASURE(1.0.0).HR',
                type: 'code',
              },
            ],
          },
          {
            id: 'MTR',
            name: 'Meters',
            names: {
              en: 'Meters',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_UNIT_MEASURE(1.0.0).MTR',
                type: 'code',
              },
            ],
          },
          {
            id: 'PER',
            name: 'Persons',
            names: {
              en: 'Persons',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_UNIT_MEASURE(1.0.0).PER',
                type: 'code',
              },
            ],
          },
          {
            id: 'JOB',
            name: 'Jobs',
            names: {
              en: 'Jobs',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=ABS:CL_UNIT_MEASURE(1.0.0).JOB',
                type: 'code',
              },
            ],
          },
        ],
      },
    ],
    dataStructures: [
      {
        id: 'DS_CPI',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=ABS:DS_CPI(1.0.0)',
            type: 'datastructure',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'Consumer Price Index (CPI) 17th Series',
        names: {
          en: 'Consumer Price Index (CPI) 17th Series',
        },
        dataStructureComponents: {
          attributeList: {
            id: 'AttributeDescriptor',
            links: [
              {
                rel: 'self',
                urn:
                  'urn:sdmx:org.sdmx.infomodel.datastructure.AttributeDescriptor=ABS:DS_CPI(1.0.0).AttributeDescriptor',
                type: 'attributedescriptor',
              },
            ],
            attributes: [
              {
                id: 'UNIT_MEASURE',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ABS:DS_CPI(1.0.0).UNIT_MEASURE',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ATR(1.0.0).UNIT_MEASURE',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ABS:CL_UNIT_MEASURE(1.0.0)',
                },
                assignmentStatus: 'Mandatory',
                attributeRelationship: {
                  dimensions: ['MEASURE'],
                },
              },
              {
                id: 'BASE_PERIOD',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ABS:DS_CPI(1.0.0).BASE_PERIOD',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ATR(1.0.0).BASE_PERIOD',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ABS:CL_BASE_PERIOD(1.0.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: {
                  dimensions: ['MEASURE', 'TSEST'],
                },
              },
              {
                id: 'OBS_STATUS',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ABS:DS_CPI(1.0.0).OBS_STATUS',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ATR(1.0.0).OBS_STATUS',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ABS:CL_OBS_STATUS(1.0.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: {
                  primaryMeasure: 'OBS_VALUE',
                },
              },
              {
                id: 'OBS_COMMENT',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=ABS:DS_CPI(1.0.0).OBS_COMMENT',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ATR(1.0.0).OBS_COMMENT',
                localRepresentation: {
                  textFormat: {
                    textType: 'String',
                    isSequence: false,
                    isMultiLingual: false,
                  },
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: {
                  primaryMeasure: 'OBS_VALUE',
                },
              },
            ],
          },
          dimensionList: {
            id: 'DimensionDescriptor',
            links: [
              {
                rel: 'self',
                urn:
                  'urn:sdmx:org.sdmx.infomodel.datastructure.DimensionDescriptor=ABS:DS_CPI(1.0.0).DimensionDescriptor',
                type: 'dimensiondescriptor',
              },
            ],
            dimensions: [
              {
                id: 'MEASURE',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=ABS:DS_CPI(1.0.0).MEASURE',
                    type: 'dimension',
                  },
                ],
                position: 0,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_COM(1.0.0).MEAS',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ABS:CL_CPI_MEASURES(1.0.0)',
                },
              },
              {
                id: 'INDEX',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=ABS:DS_CPI(1.0.0).INDEX',
                    type: 'dimension',
                  },
                ],
                position: 1,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_ECO(1.0.0).IDX',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ABS:CL_CPI_INDEX_17(1.0.0)',
                },
              },
              {
                id: 'TSEST',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=ABS:DS_CPI(1.0.0).TSEST',
                    type: 'dimension',
                  },
                ],
                position: 2,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_COM(1.0.0).TSEST',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ABS:CL_TSEST(1.0.0)',
                },
              },
              {
                id: 'REGION',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=ABS:DS_CPI(1.0.0).REGION',
                    type: 'dimension',
                  },
                ],
                position: 3,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_GEO(1.0.0).REGION',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ABS:CL_CPI_REGION(1.0.0)',
                },
              },
              {
                id: 'FREQUENCY',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=ABS:DS_CPI(1.0.0).FREQUENCY',
                    type: 'dimension',
                  },
                ],
                position: 4,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_COM(1.0.0).FREQ',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=ABS:CL_FREQ(1.0.0)',
                },
              },
            ],
            timeDimensions: [
              {
                id: 'TIME_PERIOD',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.TimeDimension=ABS:DS_CPI(1.0.0).TIME_PERIOD',
                    type: 'timedimension',
                  },
                ],
                position: 5,
                type: 'TimeDimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_COM(1.0.0).TIME_PERIOD',
                localRepresentation: {
                  textFormat: {
                    textType: 'ObservationalTimePeriod',
                    isSequence: false,
                    isMultiLingual: false,
                  },
                },
              },
            ],
          },
          measureList: {
            id: 'MeasureDescriptor',
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.MeasureDescriptor=ABS:DS_CPI(1.0.0).MeasureDescriptor',
                type: 'measuredescriptor',
              },
            ],
            primaryMeasure: {
              id: 'OBS_VALUE',
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.PrimaryMeasure=ABS:DS_CPI(1.0.0).OBS_VALUE',
                  type: 'primarymeasure',
                },
              ],
              conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=ABS:CS_COM(1.0.0).OBS_VALUE',
            },
          },
        },
      },
    ],
    contentConstraints: [
      {
        id: 'CR_DF_CPI_bffcf021774a41eab922f76d55876c41',
        links: [
          {
            rel: 'self',
            urn:
              'urn:sdmx:org.sdmx.infomodel.registry.ContentConstraint=ABS:CR_DF_CPI_bffcf021774a41eab922f76d55876c41(1.0.0)',
            type: 'contentconstraint',
          },
        ],
        version: '1.0.0',
        agencyID: 'ABS',
        isFinal: true,
        name: 'CR_DF_CPI',
        names: {
          en: 'CR_DF_CPI',
        },
        type: 'Actual',
        cubeRegions: [
          {
            isIncluded: true,
            keyValues: [
              {
                id: 'MEASURE',
                values: ['1', '2', '3', '4', '5'],
              },
              {
                id: 'INDEX',
                values: [
                  '10001',
                  '102675',
                  '102676',
                  '104101',
                  '104102',
                  '104104',
                  '104105',
                  '104120',
                  '104122',
                  '114120',
                  '114121',
                  '114122',
                  '1144',
                  '115484',
                  '115485',
                  '115486',
                  '115488',
                  '115489',
                  '115492',
                  '115493',
                  '115495',
                  '115496',
                  '115497',
                  '115498',
                  '115500',
                  '115501',
                  '115520',
                  '115522',
                  '115524',
                  '115528',
                  '115529',
                  '115902',
                  '115922',
                  '115941',
                  '115961',
                  '117101',
                  '117104',
                  '117107',
                  '117121',
                  '117124',
                  '117127',
                  '117144',
                  '126670',
                  '128058',
                  '128061',
                  '131178',
                  '131179',
                  '131180',
                  '131181',
                  '131182',
                  '131183',
                  '131184',
                  '131185',
                  '131186',
                  '131187',
                  '131188',
                  '131189',
                  '131190',
                  '131191',
                  '131192',
                  '131193',
                  '131194',
                  '131195',
                  '131197',
                  '131199',
                  '20001',
                  '20002',
                  '20003',
                  '20004',
                  '20005',
                  '20006',
                  '30001',
                  '30002',
                  '30003',
                  '30007',
                  '30012',
                  '30014',
                  '30016',
                  '30022',
                  '30024',
                  '30025',
                  '30026',
                  '30027',
                  '30033',
                  '40001',
                  '40002',
                  '40004',
                  '40005',
                  '40006',
                  '40007',
                  '40008',
                  '40009',
                  '40010',
                  '40012',
                  '40014',
                  '40015',
                  '40025',
                  '40026',
                  '40027',
                  '40029',
                  '40030',
                  '40034',
                  '40045',
                  '40046',
                  '40047',
                  '40048',
                  '40053',
                  '40055',
                  '40058',
                  '40060',
                  '40066',
                  '40067',
                  '40072',
                  '40073',
                  '40077',
                  '40078',
                  '40080',
                  '40081',
                  '40083',
                  '40084',
                  '40085',
                  '40086',
                  '40087',
                  '40088',
                  '40089',
                  '40090',
                  '40091',
                  '40092',
                  '40093',
                  '40094',
                  '40095',
                  '40096',
                  '40098',
                  '40101',
                  '40102',
                  '40106',
                  '97549',
                  '97550',
                  '97551',
                  '97554',
                  '97555',
                  '97556',
                  '97557',
                  '97558',
                  '97559',
                  '97560',
                  '97561',
                  '97563',
                  '97564',
                  '97565',
                  '97567',
                  '97571',
                  '97572',
                  '97573',
                  '97574',
                  '999901',
                  '999902',
                  '999903',
                ],
              },
              {
                id: 'TSEST',
                values: ['10', '20'],
              },
              {
                id: 'REGION',
                values: ['1', '2', '3', '4', '5', '50', '6', '7', '8'],
              },
              {
                id: 'FREQUENCY',
                values: ['Q'],
              },
            ],
          },
        ],
      },
    ],
  },
  meta: {
    schema:
      'https://raw.githubusercontent.com/sdmx-twg/sdmx-json/develop/structure-message/tools/schemas/1.0/sdmx-json-structure-schema.json',
    contentLanguages: ['en'],
    id: 'IDREF1643',
    prepared: '2019-08-14T15:12:45.9045182Z',
    test: false,
    sender: {
      id: 'Unknown',
    },
    receiver: [
      {
        id: 'Unknown',
      },
    ],
  },
}
