module.exports = {
  data: {
    dataflows: [
      {
        id: 'IRS',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=ECB:IRS(1.0)',
            type: 'dataflow',
          },
          {
            rel: 'structure',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD:AIR_EMISSIONS(1.0)',
            type: 'datastructure',
          },
        ],
        version: '1.0',
        agencyID: 'ECB',
        isFinal: true,
        name: 'Emissions of air pollutants',
        names: {
          en: 'Emissions of air pollutants',
        },
        annotations: [
          {
            type: 'NonProductionDataflow',
            text: 'true',
            texts: {
              en: 'true',
            },
          },
        ],
        structure: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD:AIR_EMISSIONS(1.0)',
      },
    ],
    categorySchemes: [
      {
        id: 'ISTAT_DW',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=IT1:ISTAT_DW(1.0)',
            type: 'categoryscheme',
          },
        ],
        version: '1.0',
        agencyID: 'IT1',
        isFinal: true,
        name: 'I.STAT',
        names: {
          en: 'I.STAT',
          it: 'I.STAT',
        },
        annotations: [
          {
            type: 'CategoryScheme_node_order',
            text: '0',
            texts: {
              en: '0',
            },
          },
        ],
        isPartial: false,
        categories: [
          {
            id: '6',
            name: 'Enterprises',
            names: {
              en: 'Enterprises',
              it: 'Imprese',
            },
            annotations: [
              {
                type: 'CategoryScheme_node_order',
                text: '0',
                texts: {
                  en: '0',
                },
              },
            ],
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.Category=IT1:ISTAT_DW(1.0).6',
                type: 'category',
              },
            ],
          },
        ],
      },
      {
        id: 'SDG',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.categoryscheme.CategoryScheme=IT1:SDG(1.0)',
            type: 'categoryscheme',
          },
        ],
        version: '1.0',
        agencyID: 'IT1',
        isFinal: true,
        name: 'SDG',
        names: {
          en: 'SDG',
          it: 'SDG',
        },
        annotations: [
          {
            type: 'CategoryScheme_node_order',
            text: '0',
            texts: {
              en: '0',
            },
          },
        ],
        isPartial: false,
        categories: [],
      },
    ],
    conceptSchemes: [
      {
        id: 'AIR_EMISSIONS_CS',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.ConceptScheme=OECD:AIR_EMISSIONS_CS(1.0)',
            type: 'conceptscheme',
          },
        ],
        version: '1.0',
        agencyID: 'OECD',
        isFinal: true,
        name: 'AIR_EMISSIONS_CS',
        names: {
          en: 'AIR_EMISSIONS_CS',
        },
        isPartial: false,
        concepts: [
          {
            id: 'COU',
            name: 'Country',
            names: {
              en: 'Country',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).COU',
                type: 'concept',
              },
            ],
          },
          {
            id: 'POL',
            name: 'Pollutant',
            names: {
              en: 'Pollutant',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).POL',
                type: 'concept',
              },
            ],
          },
          {
            id: 'VAR',
            name: 'Variable',
            names: {
              en: 'Variable',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).VAR',
                type: 'concept',
              },
            ],
          },
          {
            id: 'YEA',
            name: 'Year',
            names: {
              en: 'Year',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).YEA',
                type: 'concept',
              },
            ],
          },
          {
            id: 'OBS_VALUE',
            name: 'Observation Value',
            names: {
              en: 'Observation Value',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).OBS_VALUE',
                type: 'concept',
              },
            ],
          },
          {
            id: 'TIME_FORMAT',
            name: 'Time Format',
            names: {
              en: 'Time Format',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).TIME_FORMAT',
                type: 'concept',
              },
            ],
          },
          {
            id: 'OBS_STATUS',
            name: 'Observation Status',
            names: {
              en: 'Observation Status',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).OBS_STATUS',
                type: 'concept',
              },
            ],
          },
          {
            id: 'UNIT',
            name: 'Unit',
            names: {
              en: 'Unit',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).UNIT',
                type: 'concept',
              },
            ],
          },
          {
            id: 'POWERCODE',
            name: 'Unit multiplier',
            names: {
              en: 'Unit multiplier',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).POWERCODE',
                type: 'concept',
              },
            ],
          },
          {
            id: 'REFERENCEPERIOD',
            name: 'Reference period',
            names: {
              en: 'Reference period',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).REFERENCEPERIOD',
                type: 'concept',
              },
            ],
          },
        ],
      },
    ],
    codelists: [
      {
        id: 'CL_AIR_EMISSIONS_COU',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD:CL_AIR_EMISSIONS_COU(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'OECD',
        isFinal: true,
        name: 'Country',
        names: {
          en: 'Country',
        },
        isPartial: true,
        codes: [
          {
            id: 'AUS',
            name: 'Australia',
            names: {
              en: 'Australia',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).AUS',
                type: 'code',
              },
            ],
          },
          {
            id: 'AUT',
            name: 'Austria',
            names: {
              en: 'Austria',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).AUT',
                type: 'code',
              },
            ],
          },
          {
            id: 'BEL',
            name: 'Belgium',
            names: {
              en: 'Belgium',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).BEL',
                type: 'code',
              },
            ],
          },
          {
            id: 'CAN',
            name: 'Canada',
            names: {
              en: 'Canada',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).CAN',
                type: 'code',
              },
            ],
          },
          {
            id: 'CZE',
            name: 'Czech Republic',
            names: {
              en: 'Czech Republic',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).CZE',
                type: 'code',
              },
            ],
          },
          {
            id: 'DNK',
            name: 'Denmark',
            names: {
              en: 'Denmark',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).DNK',
                type: 'code',
              },
            ],
          },
          {
            id: 'FIN',
            name: 'Finland',
            names: {
              en: 'Finland',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).FIN',
                type: 'code',
              },
            ],
          },
          {
            id: 'FRA',
            name: 'France',
            names: {
              en: 'France',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).FRA',
                type: 'code',
              },
            ],
          },
          {
            id: 'DEU',
            name: 'Germany',
            names: {
              en: 'Germany',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).DEU',
                type: 'code',
              },
            ],
          },
          {
            id: 'GRC',
            name: 'Greece',
            names: {
              en: 'Greece',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).GRC',
                type: 'code',
              },
            ],
          },
          {
            id: 'HUN',
            name: 'Hungary',
            names: {
              en: 'Hungary',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).HUN',
                type: 'code',
              },
            ],
          },
          {
            id: 'ISL',
            name: 'Iceland',
            names: {
              en: 'Iceland',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).ISL',
                type: 'code',
              },
            ],
          },
          {
            id: 'IRL',
            name: 'Ireland',
            names: {
              en: 'Ireland',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).IRL',
                type: 'code',
              },
            ],
          },
          {
            id: 'ITA',
            name: 'Italy',
            names: {
              en: 'Italy',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).ITA',
                type: 'code',
              },
            ],
          },
          {
            id: 'JPN',
            name: 'Japan',
            names: {
              en: 'Japan',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).JPN',
                type: 'code',
              },
            ],
          },
          {
            id: 'KOR',
            name: 'Korea',
            names: {
              en: 'Korea',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).KOR',
                type: 'code',
              },
            ],
          },
          {
            id: 'LUX',
            name: 'Luxembourg',
            names: {
              en: 'Luxembourg',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).LUX',
                type: 'code',
              },
            ],
          },
          {
            id: 'MEX',
            name: 'Mexico',
            names: {
              en: 'Mexico',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).MEX',
                type: 'code',
              },
            ],
          },
          {
            id: 'NLD',
            name: 'Netherlands',
            names: {
              en: 'Netherlands',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).NLD',
                type: 'code',
              },
            ],
          },
          {
            id: 'NZL',
            name: 'New Zealand',
            names: {
              en: 'New Zealand',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).NZL',
                type: 'code',
              },
            ],
          },
          {
            id: 'NOR',
            name: 'Norway',
            names: {
              en: 'Norway',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).NOR',
                type: 'code',
              },
            ],
          },
          {
            id: 'POL',
            name: 'Poland',
            names: {
              en: 'Poland',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).POL',
                type: 'code',
              },
            ],
          },
          {
            id: 'PRT',
            name: 'Portugal',
            names: {
              en: 'Portugal',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).PRT',
                type: 'code',
              },
            ],
          },
          {
            id: 'SVK',
            name: 'Slovak Republic',
            names: {
              en: 'Slovak Republic',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).SVK',
                type: 'code',
              },
            ],
          },
          {
            id: 'ESP',
            name: 'Spain',
            names: {
              en: 'Spain',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).ESP',
                type: 'code',
              },
            ],
          },
          {
            id: 'SWE',
            name: 'Sweden',
            names: {
              en: 'Sweden',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).SWE',
                type: 'code',
              },
            ],
          },
          {
            id: 'CHE',
            name: 'Switzerland',
            names: {
              en: 'Switzerland',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).CHE',
                type: 'code',
              },
            ],
          },
          {
            id: 'TUR',
            name: 'Turkey',
            names: {
              en: 'Turkey',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).TUR',
                type: 'code',
              },
            ],
          },
          {
            id: 'GBR',
            name: 'United Kingdom',
            names: {
              en: 'United Kingdom',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).GBR',
                type: 'code',
              },
            ],
          },
          {
            id: 'USA',
            name: 'United States',
            names: {
              en: 'United States',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).USA',
                type: 'code',
              },
            ],
          },
          {
            id: 'CHL',
            name: 'Chile',
            names: {
              en: 'Chile',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).CHL',
                type: 'code',
              },
            ],
          },
          {
            id: 'EST',
            name: 'Estonia',
            names: {
              en: 'Estonia',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).EST',
                type: 'code',
              },
            ],
          },
          {
            id: 'ISR',
            name: 'Israel',
            names: {
              en: 'Israel',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).ISR',
                type: 'code',
              },
            ],
          },
          {
            id: 'LVA',
            name: 'Latvia',
            names: {
              en: 'Latvia',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).LVA',
                type: 'code',
              },
            ],
          },
          {
            id: 'LTU',
            name: 'Lithuania',
            names: {
              en: 'Lithuania',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).LTU',
                type: 'code',
              },
            ],
          },
          {
            id: 'RUS',
            name: 'Russia',
            names: {
              en: 'Russia',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).RUS',
                type: 'code',
              },
            ],
          },
          {
            id: 'SVN',
            name: 'Slovenia',
            names: {
              en: 'Slovenia',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).SVN',
                type: 'code',
              },
            ],
          },
          {
            id: 'OECD',
            name: 'OECD - Total',
            names: {
              en: 'OECD - Total',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).OECD',
                type: 'code',
              },
            ],
          },
          {
            id: 'OECDE',
            name: 'OECD - Europe',
            names: {
              en: 'OECD - Europe',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_COU(1.0).OECDE',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_AIR_EMISSIONS_OBS_STATUS',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'OECD',
        isFinal: true,
        name: 'Observation Status',
        names: {
          en: 'Observation Status',
        },
        isPartial: false,
        codes: [
          {
            id: 'B',
            name: 'Break',
            names: {
              en: 'Break',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).B',
                type: 'code',
              },
            ],
          },
          {
            id: 'C',
            name: 'Non-publishable and confidential value',
            names: {
              en: 'Non-publishable and confidential value',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).C',
                type: 'code',
              },
            ],
          },
          {
            id: 'D',
            name: 'Difference in methodology',
            names: {
              en: 'Difference in methodology',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).D',
                type: 'code',
              },
            ],
          },
          {
            id: 'E',
            name: 'Estimated value',
            names: {
              en: 'Estimated value',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).E',
                type: 'code',
              },
            ],
          },
          {
            id: 'F',
            name: 'Forecast value',
            names: {
              en: 'Forecast value',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).F',
                type: 'code',
              },
            ],
          },
          {
            id: 'H',
            name: 'Missing value, holiday or weekend',
            names: {
              en: 'Missing value, holiday or weekend',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).H',
                type: 'code',
              },
            ],
          },
          {
            id: 'I',
            name: 'Incomplete data',
            names: {
              en: 'Incomplete data',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).I',
                type: 'code',
              },
            ],
          },
          {
            id: 'L',
            name: 'Missing value; data exist but were not collected',
            names: {
              en: 'Missing value; data exist but were not collected',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).L',
                type: 'code',
              },
            ],
          },
          {
            id: 'M',
            name: 'Missing value; data cannot exist',
            names: {
              en: 'Missing value; data cannot exist',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).M',
                type: 'code',
              },
            ],
          },
          {
            id: 'N',
            name: 'Non-publishable, but non-confidential value',
            names: {
              en: 'Non-publishable, but non-confidential value',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).N',
                type: 'code',
              },
            ],
          },
          {
            id: 'P',
            name: 'Provisional value',
            names: {
              en: 'Provisional value',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).P',
                type: 'code',
              },
            ],
          },
          {
            id: 'R',
            name: 'Confidential statistical information due to identifiable',
            names: {
              en: 'Confidential statistical information due to identifiable',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).R',
                type: 'code',
              },
            ],
          },
          {
            id: 'S',
            name: 'Strike',
            names: {
              en: 'Strike',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).S',
                type: 'code',
              },
            ],
          },
          {
            id: 'Z',
            name: 'Absolute zero observation',
            names: {
              en: 'Absolute zero observation',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).Z',
                type: 'code',
              },
            ],
          },
          {
            id: 'p',
            name: 'Partial data',
            names: {
              en: 'Partial data',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).p',
                type: 'code',
              },
            ],
          },
          {
            id: 'n',
            name: 'National estimates',
            names: {
              en: 'National estimates',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).n',
                type: 'code',
              },
            ],
          },
          {
            id: 'U',
            name: 'Low reliability',
            names: {
              en: 'Low reliability',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0).U',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_AIR_EMISSIONS_POL',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD:CL_AIR_EMISSIONS_POL(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'OECD',
        isFinal: true,
        name: 'Pollutant',
        names: {
          en: 'Pollutant',
        },
        isPartial: false,
        codes: [
          {
            id: 'SOX',
            name: 'Sulphur Oxides',
            names: {
              en: 'Sulphur Oxides',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POL(1.0).SOX',
                type: 'code',
              },
            ],
          },
          {
            id: 'NOX',
            name: 'Nitrogen Oxides',
            names: {
              en: 'Nitrogen Oxides',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POL(1.0).NOX',
                type: 'code',
              },
            ],
          },
          {
            id: 'PM10',
            name: 'Particulates (PM10)',
            names: {
              en: 'Particulates (PM10)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POL(1.0).PM10',
                type: 'code',
              },
            ],
          },
          {
            id: 'PM2-5',
            name: 'Particulates (PM2.5)',
            names: {
              en: 'Particulates (PM2.5)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POL(1.0).PM2-5',
                type: 'code',
              },
            ],
          },
          {
            id: 'CO',
            name: 'Carbon Monoxide',
            names: {
              en: 'Carbon Monoxide',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POL(1.0).CO',
                type: 'code',
              },
            ],
          },
          {
            id: 'NMVOC',
            name: 'Non-methane Volatile Organic Compounds',
            names: {
              en: 'Non-methane Volatile Organic Compounds',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POL(1.0).NMVOC',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_AIR_EMISSIONS_POWERCODE',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'OECD',
        isFinal: true,
        name: 'AIR_EMISSIONS_POWERCODE codelist',
        names: {
          en: 'AIR_EMISSIONS_POWERCODE codelist',
        },
        isPartial: false,
        codes: [
          {
            id: '0',
            name: 'Units',
            names: {
              en: 'Units',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).0',
                type: 'code',
              },
            ],
          },
          {
            id: '1',
            name: 'Tens',
            names: {
              en: 'Tens',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).1',
                type: 'code',
              },
            ],
          },
          {
            id: '2',
            name: 'Hundreds',
            names: {
              en: 'Hundreds',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).2',
                type: 'code',
              },
            ],
          },
          {
            id: '3',
            name: 'Thousands',
            names: {
              en: 'Thousands',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).3',
                type: 'code',
              },
            ],
          },
          {
            id: '4',
            name: 'Tens of thousands',
            names: {
              en: 'Tens of thousands',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).4',
                type: 'code',
              },
            ],
          },
          {
            id: '5',
            name: 'Hundreds of thousands',
            names: {
              en: 'Hundreds of thousands',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).5',
                type: 'code',
              },
            ],
          },
          {
            id: '6',
            name: 'Millions',
            names: {
              en: 'Millions',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).6',
                type: 'code',
              },
            ],
          },
          {
            id: '7',
            name: 'Tens of millions',
            names: {
              en: 'Tens of millions',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).7',
                type: 'code',
              },
            ],
          },
          {
            id: '8',
            name: 'Hundreds of millions',
            names: {
              en: 'Hundreds of millions',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).8',
                type: 'code',
              },
            ],
          },
          {
            id: '9',
            name: 'Billions',
            names: {
              en: 'Billions',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).9',
                type: 'code',
              },
            ],
          },
          {
            id: '10',
            name: 'Tens of billions',
            names: {
              en: 'Tens of billions',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).10',
                type: 'code',
              },
            ],
          },
          {
            id: '11',
            name: 'Hundreds of billions',
            names: {
              en: 'Hundreds of billions',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).11',
                type: 'code',
              },
            ],
          },
          {
            id: '12',
            name: 'Trillions',
            names: {
              en: 'Trillions',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).12',
                type: 'code',
              },
            ],
          },
          {
            id: '13',
            name: 'Tens of trillions',
            names: {
              en: 'Tens of trillions',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).13',
                type: 'code',
              },
            ],
          },
          {
            id: '14',
            name: 'Hundreds of trillions',
            names: {
              en: 'Hundreds of trillions',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).14',
                type: 'code',
              },
            ],
          },
          {
            id: '15',
            name: 'Quadrillions',
            names: {
              en: 'Quadrillions',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).15',
                type: 'code',
              },
            ],
          },
          {
            id: '-1',
            name: 'Tenths',
            names: {
              en: 'Tenths',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).-1',
                type: 'code',
              },
            ],
          },
          {
            id: '-2',
            name: 'Hundredths',
            names: {
              en: 'Hundredths',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).-2',
                type: 'code',
              },
            ],
          },
          {
            id: '-3',
            name: 'Thousandths',
            names: {
              en: 'Thousandths',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).-3',
                type: 'code',
              },
            ],
          },
          {
            id: '-4',
            name: 'Ten-thousandths',
            names: {
              en: 'Ten-thousandths',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).-4',
                type: 'code',
              },
            ],
          },
          {
            id: '-5',
            name: 'Hundred-thousandths',
            names: {
              en: 'Hundred-thousandths',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).-5',
                type: 'code',
              },
            ],
          },
          {
            id: '-6',
            name: 'Millionths',
            names: {
              en: 'Millionths',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).-6',
                type: 'code',
              },
            ],
          },
          {
            id: '-7',
            name: 'Ten-millionths',
            names: {
              en: 'Ten-millionths',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).-7',
                type: 'code',
              },
            ],
          },
          {
            id: '-8',
            name: 'Hundred-millionths',
            names: {
              en: 'Hundred-millionths',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).-8',
                type: 'code',
              },
            ],
          },
          {
            id: '-9',
            name: 'Billionths',
            names: {
              en: 'Billionths',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).-9',
                type: 'code',
              },
            ],
          },
          {
            id: '-10',
            name: 'Ten-billionths',
            names: {
              en: 'Ten-billionths',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).-10',
                type: 'code',
              },
            ],
          },
          {
            id: '-11',
            name: 'Hundred-billionths',
            names: {
              en: 'Hundred-billionths',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).-11',
                type: 'code',
              },
            ],
          },
          {
            id: '-12',
            name: 'Trillionths',
            names: {
              en: 'Trillionths',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).-12',
                type: 'code',
              },
            ],
          },
          {
            id: '-13',
            name: 'Ten-trillionths',
            names: {
              en: 'Ten-trillionths',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).-13',
                type: 'code',
              },
            ],
          },
          {
            id: '-14',
            name: 'Hundred-trillionths',
            names: {
              en: 'Hundred-trillionths',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).-14',
                type: 'code',
              },
            ],
          },
          {
            id: '-15',
            name: 'Quadrillionths',
            names: {
              en: 'Quadrillionths',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).-15',
                type: 'code',
              },
            ],
          },
          {
            id: '9999',
            name: '.',
            names: {
              en: '.',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0).9999',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_AIR_EMISSIONS_REFERENCEPERIOD',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'OECD',
        isFinal: true,
        name: 'AIR_EMISSIONS_REFERENCEPERIOD codelist',
        names: {
          en: 'AIR_EMISSIONS_REFERENCEPERIOD codelist',
        },
        isPartial: false,
        codes: [
          {
            id: '2013_100',
            name: '2013=100',
            names: {
              en: '2013=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2013_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2012_100',
            name: '2012=100',
            names: {
              en: '2012=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2012_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2011_100',
            name: '2011=100',
            names: {
              en: '2011=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2011_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2010_100',
            name: '2010=100',
            names: {
              en: '2010=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2010_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2009_100',
            name: '2009=100',
            names: {
              en: '2009=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2009_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2008_100',
            name: '2008=100',
            names: {
              en: '2008=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2008_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2007_100',
            name: '2007=100',
            names: {
              en: '2007=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2007_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2006_100',
            name: '2006=100',
            names: {
              en: '2006=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2006_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2005_100',
            name: '2005=100',
            names: {
              en: '2005=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2005_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2004_100',
            name: '2004=100',
            names: {
              en: '2004=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2004_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2003_100',
            name: '2003=100',
            names: {
              en: '2003=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2003_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2002_100',
            name: '2002=100',
            names: {
              en: '2002=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2002_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2001_100',
            name: '2001=100',
            names: {
              en: '2001=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2001_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2000_100',
            name: '2000=100',
            names: {
              en: '2000=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2000_100',
                type: 'code',
              },
            ],
          },
          {
            id: '1999_100',
            name: '1999=100',
            names: {
              en: '1999=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1999_100',
                type: 'code',
              },
            ],
          },
          {
            id: '1998_100',
            name: '1998=100',
            names: {
              en: '1998=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1998_100',
                type: 'code',
              },
            ],
          },
          {
            id: '1997_100',
            name: '1997=100',
            names: {
              en: '1997=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1997_100',
                type: 'code',
              },
            ],
          },
          {
            id: '1996_100',
            name: '1996=100',
            names: {
              en: '1996=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1996_100',
                type: 'code',
              },
            ],
          },
          {
            id: '1995_100',
            name: '1995=100',
            names: {
              en: '1995=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1995_100',
                type: 'code',
              },
            ],
          },
          {
            id: '1994_100',
            name: '1994=100',
            names: {
              en: '1994=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1994_100',
                type: 'code',
              },
            ],
          },
          {
            id: '1993_100',
            name: '1993=100',
            names: {
              en: '1993=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1993_100',
                type: 'code',
              },
            ],
          },
          {
            id: '1992_100',
            name: '1992=100',
            names: {
              en: '1992=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1992_100',
                type: 'code',
              },
            ],
          },
          {
            id: '1991_100',
            name: '1991=100',
            names: {
              en: '1991=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1991_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2013',
            name: '2013',
            names: {
              en: '2013',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2013',
                type: 'code',
              },
            ],
          },
          {
            id: '2012',
            name: '2012',
            names: {
              en: '2012',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2012',
                type: 'code',
              },
            ],
          },
          {
            id: '2011',
            name: '2011',
            names: {
              en: '2011',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2011',
                type: 'code',
              },
            ],
          },
          {
            id: '2010',
            name: '2010',
            names: {
              en: '2010',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2010',
                type: 'code',
              },
            ],
          },
          {
            id: '2009',
            name: '2009',
            names: {
              en: '2009',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2009',
                type: 'code',
              },
            ],
          },
          {
            id: '2008',
            name: '2008',
            names: {
              en: '2008',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2008',
                type: 'code',
              },
            ],
          },
          {
            id: '2007',
            name: '2007',
            names: {
              en: '2007',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2007',
                type: 'code',
              },
            ],
          },
          {
            id: '2006',
            name: '2006',
            names: {
              en: '2006',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2006',
                type: 'code',
              },
            ],
          },
          {
            id: '2005',
            name: '2005',
            names: {
              en: '2005',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2005',
                type: 'code',
              },
            ],
          },
          {
            id: '2004',
            name: '2004',
            names: {
              en: '2004',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2004',
                type: 'code',
              },
            ],
          },
          {
            id: '2003',
            name: '2003',
            names: {
              en: '2003',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2003',
                type: 'code',
              },
            ],
          },
          {
            id: '2002',
            name: '2002',
            names: {
              en: '2002',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2002',
                type: 'code',
              },
            ],
          },
          {
            id: '2001',
            name: '2001',
            names: {
              en: '2001',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2001',
                type: 'code',
              },
            ],
          },
          {
            id: '2000',
            name: '2000',
            names: {
              en: '2000',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2000',
                type: 'code',
              },
            ],
          },
          {
            id: '1999',
            name: '1999',
            names: {
              en: '1999',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1999',
                type: 'code',
              },
            ],
          },
          {
            id: '1998',
            name: '1998',
            names: {
              en: '1998',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1998',
                type: 'code',
              },
            ],
          },
          {
            id: '1997',
            name: '1997',
            names: {
              en: '1997',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1997',
                type: 'code',
              },
            ],
          },
          {
            id: '1996',
            name: '1996',
            names: {
              en: '1996',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1996',
                type: 'code',
              },
            ],
          },
          {
            id: '1995',
            name: '1995',
            names: {
              en: '1995',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1995',
                type: 'code',
              },
            ],
          },
          {
            id: '1994',
            name: '1994',
            names: {
              en: '1994',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1994',
                type: 'code',
              },
            ],
          },
          {
            id: '1993',
            name: '1993',
            names: {
              en: '1993',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1993',
                type: 'code',
              },
            ],
          },
          {
            id: '1992',
            name: '1992',
            names: {
              en: '1992',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1992',
                type: 'code',
              },
            ],
          },
          {
            id: '1991',
            name: '1991',
            names: {
              en: '1991',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1991',
                type: 'code',
              },
            ],
          },
          {
            id: '1990_100',
            name: '1990=100',
            names: {
              en: '1990=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1990_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2011_12',
            name: '2011-12',
            names: {
              en: '2011-12',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2011_12',
                type: 'code',
              },
            ],
          },
          {
            id: '2004_05',
            name: '2004-05',
            names: {
              en: '2004-05',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2004_05',
                type: 'code',
              },
            ],
          },
          {
            id: '1995_96',
            name: '1995-96',
            names: {
              en: '1995-96',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1995_96',
                type: 'code',
              },
            ],
          },
          {
            id: '2012_13',
            name: '2012-13',
            names: {
              en: '2012-13',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2012_13',
                type: 'code',
              },
            ],
          },
          {
            id: '2009_10',
            name: '2009-10',
            names: {
              en: '2009-10',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2009_10',
                type: 'code',
              },
            ],
          },
          {
            id: '2014_100',
            name: '2014=100',
            names: {
              en: '2014=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2014_100',
                type: 'code',
              },
            ],
          },
          {
            id: 'USA_100',
            name: 'USA=100',
            names: {
              en: 'USA=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).USA_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2014',
            name: '2014',
            names: {
              en: '2014',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2014',
                type: 'code',
              },
            ],
          },
          {
            id: '2007Q1_100',
            name: '2007Q1=100',
            names: {
              en: '2007Q1=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2007Q1_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2013_14',
            name: '2013-14',
            names: {
              en: '2013-14',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2013_14',
                type: 'code',
              },
            ],
          },
          {
            id: '2011-12_100',
            name: '2011-12=100',
            names: {
              en: '2011-12=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2011-12_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2010M12_100',
            name: '2010M12=100',
            names: {
              en: '2010M12=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2010M12_100',
                type: 'code',
              },
            ],
          },
          {
            id: '1982-84_100',
            name: '1982-84=100',
            names: {
              en: '1982-84=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1982-84_100',
                type: 'code',
              },
            ],
          },
          {
            id: '1980_100',
            name: '1980=100',
            names: {
              en: '1980=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1980_100',
                type: 'code',
              },
            ],
          },
          {
            id: '1993M12_100',
            name: '1993M12=100',
            names: {
              en: '1993M12=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).1993M12_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2000M12_100',
            name: '2000M12=100',
            names: {
              en: '2000M12=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2000M12_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2006M12_100',
            name: '2006M12=100',
            names: {
              en: '2006M12=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2006M12_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2006Q2_100',
            name: '2006Q2=100',
            names: {
              en: '2006Q2=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2006Q2_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2008M12_100',
            name: '2008M12=100',
            names: {
              en: '2008M12=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2008M12_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2011M12_100',
            name: '2011M12=100',
            names: {
              en: '2011M12=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2011M12_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2012M12_100',
            name: '2012M12=100',
            names: {
              en: '2012M12=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2012M12_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2015',
            name: '2015',
            names: {
              en: '2015',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2015',
                type: 'code',
              },
            ],
          },
          {
            id: '2015_100',
            name: '2015=100',
            names: {
              en: '2015=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2015_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2015M12_100',
            name: '2015M12=100',
            names: {
              en: '2015M12=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2015M12_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2015M06_100',
            name: '2015M06=100',
            names: {
              en: '2015M06=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2015M06_100',
                type: 'code',
              },
            ],
          },
          {
            id: 'LTAVG_100',
            name: 'Long-term average=100',
            names: {
              en: 'Long-term average=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).LTAVG_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2014M12_100',
            name: '2014M12=100',
            names: {
              en: '2014M12=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2014M12_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2014_15',
            name: '2014-15',
            names: {
              en: '2014-15',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2014_15',
                type: 'code',
              },
            ],
          },
          {
            id: '2016M12_100',
            name: '2016M12=100',
            names: {
              en: '2016M12=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2016M12_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2016',
            name: '2016',
            names: {
              en: '2016',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2016',
                type: 'code',
              },
            ],
          },
          {
            id: '2016_100',
            name: '2016=100',
            names: {
              en: '2016=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2016_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2015_16',
            name: '2015-16',
            names: {
              en: '2015-16',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2015_16',
                type: 'code',
              },
            ],
          },
          {
            id: '2017',
            name: '2017',
            names: {
              en: '2017',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2017',
                type: 'code',
              },
            ],
          },
          {
            id: '2017_100',
            name: '2017=100',
            names: {
              en: '2017=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2017_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2017M12_100',
            name: '2017M12=100',
            names: {
              en: '2017M12=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2017M12_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2011Q3_100',
            name: '2011Q3=100',
            names: {
              en: '2011Q3=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2011Q3_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2008M1_100',
            name: '2008M1=100',
            names: {
              en: '2008M1=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2008M1_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2017Q2_100',
            name: '2017Q2=100',
            names: {
              en: '2017Q2=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2017Q2_100',
                type: 'code',
              },
            ],
          },
          {
            id: '2013M12_100',
            name: '2013M12=100',
            names: {
              en: '2013M12=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0).2013M12_100',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_AIR_EMISSIONS_TIME_FORMAT',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD:CL_AIR_EMISSIONS_TIME_FORMAT(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'OECD',
        isFinal: true,
        name: 'AIR_EMISSIONS_TIME_FORMAT codelist',
        names: {
          en: 'AIR_EMISSIONS_TIME_FORMAT codelist',
        },
        isPartial: false,
        codes: [
          {
            id: 'P1Y',
            name: 'Annual',
            names: {
              en: 'Annual',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_TIME_FORMAT(1.0).P1Y',
                type: 'code',
              },
            ],
          },
          {
            id: 'P1M',
            name: 'Monthly',
            names: {
              en: 'Monthly',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_TIME_FORMAT(1.0).P1M',
                type: 'code',
              },
            ],
          },
          {
            id: 'P3M',
            name: 'Quarterly',
            names: {
              en: 'Quarterly',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_TIME_FORMAT(1.0).P3M',
                type: 'code',
              },
            ],
          },
          {
            id: 'P6M',
            name: 'Half-yearly',
            names: {
              en: 'Half-yearly',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_TIME_FORMAT(1.0).P6M',
                type: 'code',
              },
            ],
          },
          {
            id: 'P1D',
            name: 'Daily',
            names: {
              en: 'Daily',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_TIME_FORMAT(1.0).P1D',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_AIR_EMISSIONS_UNIT',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD:CL_AIR_EMISSIONS_UNIT(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'OECD',
        isFinal: true,
        name: 'AIR_EMISSIONS_UNIT codelist',
        names: {
          en: 'AIR_EMISSIONS_UNIT codelist',
        },
        isPartial: false,
        codes: [
          {
            id: '1',
            name: 'RATIOS',
            names: {
              en: 'RATIOS',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).1',
                type: 'code',
              },
            ],
          },
          {
            id: 'GRWH',
            name: 'Growth rate',
            names: {
              en: 'Growth rate',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).GRWH',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'AVGRW',
            name: 'Average growth rate',
            names: {
              en: 'Average growth rate',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).AVGRW',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'IDX',
            name: 'Index',
            names: {
              en: 'Index',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).IDX',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'P50P10',
            name: 'Interdecile ratio P50/P10',
            names: {
              en: 'Interdecile ratio P50/P10',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).P50P10',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'P90P10',
            name: 'Interdecile ratio P90/P10',
            names: {
              en: 'Interdecile ratio P90/P10',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).P90P10',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'P90P50',
            name: 'Interdecile ratio P90/P50',
            names: {
              en: 'Interdecile ratio P90/P50',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).P90P50',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'AVSCORE',
            name: 'Average score',
            names: {
              en: 'Average score',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).AVSCORE',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'NATUSD',
            name: 'National currency per US dollar',
            names: {
              en: 'National currency per US dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).NATUSD',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'OECDIDX',
            name: 'OECD=100',
            names: {
              en: 'OECD=100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).OECDIDX',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'PC',
            name: 'Percentage',
            names: {
              en: 'Percentage',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).PC',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'RATIO',
            name: 'Ratio',
            names: {
              en: 'Ratio',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).RATIO',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'TOEUSD',
            name: 'To be deleted-Toe per 1 000 US dollars',
            names: {
              en: 'To be deleted-Toe per 1 000 US dollars',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TOEUSD',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'TOE_1000USD',
            name: 'TOE per 1 000 US dollars',
            names: {
              en: 'TOE per 1 000 US dollars',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TOE_1000USD',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'USD_KG',
            name: 'US dollars per kilogram',
            names: {
              en: 'US dollars per kilogram',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).USD_KG',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'USD_L',
            name: 'US dollars per litre',
            names: {
              en: 'US dollars per litre',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).USD_L',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'USD_CO2',
            name: 'US dollars per unit of CO2',
            names: {
              en: 'US dollars per unit of CO2',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).USD_CO2',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'USD_BAR',
            name: 'US dollars per barrel',
            names: {
              en: 'US dollars per barrel',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).USD_BAR',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'USD_HAB',
            name: 'To be deleted',
            names: {
              en: 'To be deleted',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).USD_HAB',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'EUIDX',
            name: 'European Union = 100',
            names: {
              en: 'European Union = 100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).EUIDX',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: 'NATEU',
            name: 'National currency per Euro',
            names: {
              en: 'National currency per Euro',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).NATEU',
                type: 'code',
              },
            ],
            parent: '1',
          },
          {
            id: '2',
            name: 'NUMBERS',
            names: {
              en: 'NUMBERS',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).2',
                type: 'code',
              },
            ],
          },
          {
            id: '0_EXIST_1_DONTEXIST',
            name: '0 = Data do not exist / 1 = Data exist',
            names: {
              en: '0 = Data do not exist / 1 = Data exist',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).0_EXIST_1_DONTEXIST',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: 'DEATH_1000BIRTH',
            name: 'Deaths per 1 000 live births',
            names: {
              en: 'Deaths per 1 000 live births',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).DEATH_1000BIRTH',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: 'EMPLOYED',
            name: 'Employed',
            names: {
              en: 'Employed',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).EMPLOYED',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: 'ENTR',
            name: 'Enterprises',
            names: {
              en: 'Enterprises',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ENTR',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: 'ESTB',
            name: 'Establishments',
            names: {
              en: 'Establishments',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ESTB',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: '1000000HAB',
            name: 'Per 1 000 000 inhabitants',
            names: {
              en: 'Per 1 000 000 inhabitants',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).1000000HAB',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: 'VKM',
            name: 'Vehicle-kilometres',
            names: {
              en: 'Vehicle-kilometres',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).VKM',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: 'FTE',
            name: 'Full time equivalent',
            names: {
              en: 'Full time equivalent',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).FTE',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: '_1000',
            name: 'Per thousand',
            names: {
              en: 'Per thousand',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0)._1000',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: 'SE',
            name: 'Standard-error',
            names: {
              en: 'Standard-error',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SE',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: '0_TO_1',
            name: '0-1 scale',
            names: {
              en: '0-1 scale',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).0_TO_1',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: 'CHLD',
            name: 'Children',
            names: {
              en: 'Children',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CHLD',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: 'DEATH1000',
            name: 'To be deleted-Deaths per 1 000 live births',
            names: {
              en: 'To be deleted-Deaths per 1 000 live births',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).DEATH1000',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: 'EMPLOYEE',
            name: 'Employee',
            names: {
              en: 'Employee',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).EMPLOYEE',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: 'HAB',
            name: 'Inhabitants',
            names: {
              en: 'Inhabitants',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).HAB',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: 'JOB',
            name: 'Jobs',
            names: {
              en: 'Jobs',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).JOB',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: 'NBR',
            name: 'Number',
            names: {
              en: 'Number',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).NBR',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: 'PKM',
            name: 'Passenger-kilometres',
            names: {
              en: 'Passenger-kilometres',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).PKM',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: '1000HAB',
            name: 'Per 1 000 inhabitants',
            names: {
              en: 'Per 1 000 inhabitants',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).1000HAB',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: '100000HAB',
            name: 'Per 100 000 inhabitants',
            names: {
              en: 'Per 100 000 inhabitants',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).100000HAB',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: '100HAB',
            name: 'Per 100 inhabitants',
            names: {
              en: 'Per 100 inhabitants',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).100HAB',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: 'PC_PNT',
            name: 'Percentage points',
            names: {
              en: 'Percentage points',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).PC_PNT',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: 'PER',
            name: 'Persons',
            names: {
              en: 'Persons',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).PER',
                type: 'code',
              },
            ],
            parent: '2',
          },
          {
            id: '3',
            name: 'TIME',
            names: {
              en: 'TIME',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).3',
                type: 'code',
              },
            ],
          },
          {
            id: 'DAY',
            name: 'Days',
            names: {
              en: 'Days',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).DAY',
                type: 'code',
              },
            ],
            parent: '3',
          },
          {
            id: 'HOUR',
            name: 'Hours',
            names: {
              en: 'Hours',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).HOUR',
                type: 'code',
              },
            ],
            parent: '3',
          },
          {
            id: 'HPYPP',
            name: 'Hours per year per person',
            names: {
              en: 'Hours per year per person',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).HPYPP',
                type: 'code',
              },
            ],
            parent: '3',
          },
          {
            id: 'MONTH',
            name: 'Months',
            names: {
              en: 'Months',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MONTH',
                type: 'code',
              },
            ],
            parent: '3',
          },
          {
            id: 'YR',
            name: 'Years',
            names: {
              en: 'Years',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).YR',
                type: 'code',
              },
            ],
            parent: '3',
          },
          {
            id: 'MPD',
            name: 'Minutes per day',
            names: {
              en: 'Minutes per day',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MPD',
                type: 'code',
              },
            ],
            parent: '3',
          },
          {
            id: 'WEEK',
            name: 'Weeks',
            names: {
              en: 'Weeks',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).WEEK',
                type: 'code',
              },
            ],
            parent: '3',
          },
          {
            id: 'DPYPP',
            name: 'Days per year per person',
            names: {
              en: 'Days per year per person',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).DPYPP',
                type: 'code',
              },
            ],
            parent: '3',
          },
          {
            id: '4',
            name: 'PHYSICAL MEASURES',
            names: {
              en: 'PHYSICAL MEASURES',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).4',
                type: 'code',
              },
            ],
          },
          {
            id: 'LT_HAB',
            name: 'Litres per capita',
            names: {
              en: 'Litres per capita',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).LT_HAB',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'KCAL_HAB',
            name: 'Kilocalories per capita',
            names: {
              en: 'Kilocalories per capita',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KCAL_HAB',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'G_HAB',
            name: 'Grams per capita',
            names: {
              en: 'Grams per capita',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).G_HAB',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'BAR_DAY',
            name: 'Barrels per day',
            names: {
              en: 'Barrels per day',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BAR_DAY',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'BAR',
            name: 'Barrels',
            names: {
              en: 'Barrels',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BAR',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'KOE_HAB',
            name: 'Kilograms of oil equivalent per capita',
            names: {
              en: 'Kilograms of oil equivalent per capita',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KOE_HAB',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'MICRO_M3',
            name: 'Micrograms per cubic metre',
            names: {
              en: 'Micrograms per cubic metre',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MICRO_M3',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'TONNE_1000USD',
            name: 'Tonnes per 1 000 US dollars',
            names: {
              en: 'Tonnes per 1 000 US dollars',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TONNE_1000USD',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'GWH',
            name: 'Gigawatt hours',
            names: {
              en: 'Gigawatt hours',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).GWH',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'KG_1000USD',
            name: 'Kilograms per 1 000 US dollars',
            names: {
              en: 'Kilograms per 1 000 US dollars',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KG_1000USD',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'MICRO_L',
            name: 'Micrograms per litre',
            names: {
              en: 'Micrograms per litre',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MICRO_L',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'MILLI_L',
            name: 'Milligrams per litre',
            names: {
              en: 'Milligrams per litre',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MILLI_L',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'T_CO2_EQVT',
            name: 'Tonnes of CO2 equivalent',
            names: {
              en: 'Tonnes of CO2 equivalent',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).T_CO2_EQVT',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'CAR',
            name: 'Carats',
            names: {
              en: 'Carats',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CAR',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'CUR',
            name: 'Curies',
            names: {
              en: 'Curies',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CUR',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'KG',
            name: 'Kilograms',
            names: {
              en: 'Kilograms',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KG',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'KG_HAB',
            name: 'Kilograms per capita',
            names: {
              en: 'Kilograms per capita',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KG_HAB',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'KM',
            name: 'Kilometres',
            names: {
              en: 'Kilometres',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KM',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'KM2',
            name: 'Square kilometres',
            names: {
              en: 'Square kilometres',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KM2',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'HA',
            name: 'Hectares',
            names: {
              en: 'Hectares',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).HA',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'KW',
            name: 'Kilowatts',
            names: {
              en: 'Kilowatts',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KW',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'KWH',
            name: 'Kilowatt hours',
            names: {
              en: 'Kilowatt hours',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KWH',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'LT',
            name: 'Litres',
            names: {
              en: 'Litres',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).LT',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'M2',
            name: 'Square metres',
            names: {
              en: 'Square metres',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).M2',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'M3',
            name: 'Cubic metres',
            names: {
              en: 'Cubic metres',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).M3',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'M3CAP',
            name: 'Cubic metres per capita',
            names: {
              en: 'Cubic metres per capita',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).M3CAP',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'METRE',
            name: 'Metres',
            names: {
              en: 'Metres',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).METRE',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'TWH',
            name: 'Terawatt hours',
            names: {
              en: 'Terawatt hours',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TWH',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'TONNE',
            name: 'Tonnes',
            names: {
              en: 'Tonnes',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TONNE',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'TONNEKM',
            name: 'Tonnes-kilometres',
            names: {
              en: 'Tonnes-kilometres',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TONNEKM',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: 'TOE',
            name: 'Tonnes of oil equivalent (toe)',
            names: {
              en: 'Tonnes of oil equivalent (toe)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TOE',
                type: 'code',
              },
            ],
            parent: '4',
          },
          {
            id: '5',
            name: 'CURRENCIES',
            names: {
              en: 'CURRENCIES',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).5',
                type: 'code',
              },
            ],
          },
          {
            id: 'AED',
            name: 'Dirham from the United Arab Emirates',
            names: {
              en: 'Dirham from the United Arab Emirates',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).AED',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'AFA',
            name: 'Afghani (old)',
            names: {
              en: 'Afghani (old)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).AFA',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'AFN',
            name: 'Afghani',
            names: {
              en: 'Afghani',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).AFN',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ALL',
            name: 'Lek',
            names: {
              en: 'Lek',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ALL',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'AMD',
            name: 'Armenian Dram',
            names: {
              en: 'Armenian Dram',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).AMD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ANG',
            name: 'Netherlands Antillian Guilder',
            names: {
              en: 'Netherlands Antillian Guilder',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ANG',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'AOA',
            name: 'Kwanza',
            names: {
              en: 'Kwanza',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).AOA',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ARS',
            name: 'Argentine Peso',
            names: {
              en: 'Argentine Peso',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ARS',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ATS',
            name: 'Austrian Schilling',
            names: {
              en: 'Austrian Schilling',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ATS',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'AUD',
            name: 'Australian Dollar',
            names: {
              en: 'Australian Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).AUD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'AWG',
            name: 'Aruban Guilder',
            names: {
              en: 'Aruban Guilder',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).AWG',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'AZM',
            name: 'Azerbaijanian Manat (old)',
            names: {
              en: 'Azerbaijanian Manat (old)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).AZM',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'AZN',
            name: 'Azerbaijanian Manat',
            names: {
              en: 'Azerbaijanian Manat',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).AZN',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BAM',
            name: 'Convertible Marks',
            names: {
              en: 'Convertible Marks',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BAM',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BBD',
            name: 'Barbados Dollar',
            names: {
              en: 'Barbados Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BBD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BDT',
            name: 'Taka',
            names: {
              en: 'Taka',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BDT',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BEF',
            name: 'Belgian Franc',
            names: {
              en: 'Belgian Franc',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BEF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BGN',
            name: 'Bulgarian Lev',
            names: {
              en: 'Bulgarian Lev',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BGN',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BHD',
            name: 'Bahraini Dinar',
            names: {
              en: 'Bahraini Dinar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BHD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BIF',
            name: 'Burundi Franc',
            names: {
              en: 'Burundi Franc',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BIF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BMD',
            name: 'Bermudian Dollar',
            names: {
              en: 'Bermudian Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BMD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BND',
            name: 'Brunei Dollar',
            names: {
              en: 'Brunei Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BND',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BOB',
            name: 'Boliviano',
            names: {
              en: 'Boliviano',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BOB',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BOV',
            name: 'Mvdol',
            names: {
              en: 'Mvdol',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BOV',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BRL',
            name: 'Brazilian Real',
            names: {
              en: 'Brazilian Real',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BRL',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BSD',
            name: 'Bahamian Dollar',
            names: {
              en: 'Bahamian Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BSD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BTN',
            name: 'Ngultrum',
            names: {
              en: 'Ngultrum',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BTN',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BWP',
            name: 'Pula',
            names: {
              en: 'Pula',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BWP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BYR',
            name: 'Belarussian Ruble',
            names: {
              en: 'Belarussian Ruble',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BYR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'BZD',
            name: 'Belize Dollar',
            names: {
              en: 'Belize Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).BZD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'CAD',
            name: 'Canadian Dollar',
            names: {
              en: 'Canadian Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CAD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'CDF',
            name: 'Congolese franc',
            names: {
              en: 'Congolese franc',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CDF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'CFA',
            name: 'CFA Franc',
            names: {
              en: 'CFA Franc',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CFA',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'CHE',
            name: 'WIR Euro',
            names: {
              en: 'WIR Euro',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CHE',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'CHF',
            name: 'Swiss Franc',
            names: {
              en: 'Swiss Franc',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CHF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'CHW',
            name: 'WIR Franc',
            names: {
              en: 'WIR Franc',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CHW',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'CLF',
            name: 'Unidad de formento (UF)',
            names: {
              en: 'Unidad de formento (UF)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CLF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'CLP',
            name: 'Chilean Peso',
            names: {
              en: 'Chilean Peso',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CLP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'CNY',
            name: 'Yuan Renminbi',
            names: {
              en: 'Yuan Renminbi',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CNY',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'COP',
            name: 'Colombian Peso',
            names: {
              en: 'Colombian Peso',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).COP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'COU',
            name: 'Unidad de valor real',
            names: {
              en: 'Unidad de valor real',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).COU',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'CRC',
            name: 'Costa Rican Colon',
            names: {
              en: 'Costa Rican Colon',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CRC',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'CSD',
            name: 'Dinar of Serbia and Montenegro',
            names: {
              en: 'Dinar of Serbia and Montenegro',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CSD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'CUP',
            name: 'Cuban Peso',
            names: {
              en: 'Cuban Peso',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CUP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'CVE',
            name: 'Cape Verde Escudo',
            names: {
              en: 'Cape Verde Escudo',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CVE',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'CYP',
            name: 'Cyprus Pound',
            names: {
              en: 'Cyprus Pound',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CYP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'CZK',
            name: 'Czech Koruna',
            names: {
              en: 'Czech Koruna',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).CZK',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'DEM',
            name: 'German Mark',
            names: {
              en: 'German Mark',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).DEM',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'DJF',
            name: 'Djibouti Franc',
            names: {
              en: 'Djibouti Franc',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).DJF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'DKK',
            name: 'Danish Krone',
            names: {
              en: 'Danish Krone',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).DKK',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'DOP',
            name: 'Dominican Peso',
            names: {
              en: 'Dominican Peso',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).DOP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'DZD',
            name: 'Algerian Dinar',
            names: {
              en: 'Algerian Dinar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).DZD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ECV',
            name: 'Unidad de Valor Constante',
            names: {
              en: 'Unidad de Valor Constante',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ECV',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'EEK',
            name: 'Kroon',
            names: {
              en: 'Kroon',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).EEK',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'EGP',
            name: 'Egyptian Pound',
            names: {
              en: 'Egyptian Pound',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).EGP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ERN',
            name: 'Nakfa',
            names: {
              en: 'Nakfa',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ERN',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ESP',
            name: 'Spanish Peseta',
            names: {
              en: 'Spanish Peseta',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ESP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ETB',
            name: 'Ethiopian Birr',
            names: {
              en: 'Ethiopian Birr',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ETB',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'EUR',
            name: 'Euro',
            names: {
              en: 'Euro',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).EUR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'FIM',
            name: 'Finnish Markka',
            names: {
              en: 'Finnish Markka',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).FIM',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'FJD',
            name: 'Fiji Dollar',
            names: {
              en: 'Fiji Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).FJD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'FKP',
            name: 'Falkland Islands Pound',
            names: {
              en: 'Falkland Islands Pound',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).FKP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'FRF',
            name: 'French Franc',
            names: {
              en: 'French Franc',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).FRF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'GBP',
            name: 'Pound Sterling',
            names: {
              en: 'Pound Sterling',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).GBP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'GEL',
            name: 'Lari',
            names: {
              en: 'Lari',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).GEL',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'GHC',
            name: 'Ghana Cedi (old)',
            names: {
              en: 'Ghana Cedi (old)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).GHC',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'GHS',
            name: 'Ghana Cedi',
            names: {
              en: 'Ghana Cedi',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).GHS',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'GIP',
            name: 'Gibraltar Pound',
            names: {
              en: 'Gibraltar Pound',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).GIP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'GMD',
            name: 'Dalasi',
            names: {
              en: 'Dalasi',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).GMD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'GNF',
            name: 'Guinea Franc',
            names: {
              en: 'Guinea Franc',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).GNF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'GRD',
            name: 'Greek Drachma',
            names: {
              en: 'Greek Drachma',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).GRD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'GTQ',
            name: 'Quetzal',
            names: {
              en: 'Quetzal',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).GTQ',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'GWP',
            name: 'Guinea-Bissau Peso',
            names: {
              en: 'Guinea-Bissau Peso',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).GWP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'GYD',
            name: 'Guyana Dollar',
            names: {
              en: 'Guyana Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).GYD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'HKD',
            name: 'Hong Kong Dollar',
            names: {
              en: 'Hong Kong Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).HKD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'HNL',
            name: 'Lempira',
            names: {
              en: 'Lempira',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).HNL',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'HRK',
            name: 'Croatian Kuna',
            names: {
              en: 'Croatian Kuna',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).HRK',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'HTG',
            name: 'Gourde',
            names: {
              en: 'Gourde',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).HTG',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'HUF',
            name: 'Forint',
            names: {
              en: 'Forint',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).HUF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'IDR',
            name: 'Rupiah',
            names: {
              en: 'Rupiah',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).IDR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'IEP',
            name: 'Irish Pound',
            names: {
              en: 'Irish Pound',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).IEP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ILS',
            name: 'New Israeli Sheqel',
            names: {
              en: 'New Israeli Sheqel',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ILS',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'INR',
            name: 'Indian Rupee',
            names: {
              en: 'Indian Rupee',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).INR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'IQD',
            name: 'Iraqi Dinar',
            names: {
              en: 'Iraqi Dinar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).IQD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'IRR',
            name: 'Iranian Rial',
            names: {
              en: 'Iranian Rial',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).IRR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ISK',
            name: 'Iceland Krona',
            names: {
              en: 'Iceland Krona',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ISK',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ITL',
            name: 'Italian Lira',
            names: {
              en: 'Italian Lira',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ITL',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'JMD',
            name: 'Jamaican Dollar',
            names: {
              en: 'Jamaican Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).JMD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'JOD',
            name: 'Jordanian Dinar',
            names: {
              en: 'Jordanian Dinar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).JOD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'JPY',
            name: 'Yen',
            names: {
              en: 'Yen',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).JPY',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'KES',
            name: 'Kenyan Shilling',
            names: {
              en: 'Kenyan Shilling',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KES',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'KGS',
            name: 'Som',
            names: {
              en: 'Som',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KGS',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'KHR',
            name: 'Riel',
            names: {
              en: 'Riel',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KHR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'KMF',
            name: 'Comoro Franc',
            names: {
              en: 'Comoro Franc',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KMF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'KPW',
            name: 'North Korean Won',
            names: {
              en: 'North Korean Won',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KPW',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'KRW',
            name: 'Won',
            names: {
              en: 'Won',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KRW',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'KWD',
            name: 'Kuwaiti Dinar',
            names: {
              en: 'Kuwaiti Dinar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KWD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'KYD',
            name: 'Cayman Islands Dollar',
            names: {
              en: 'Cayman Islands Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KYD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'KZT',
            name: 'Tenge',
            names: {
              en: 'Tenge',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).KZT',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'LAK',
            name: 'Kip',
            names: {
              en: 'Kip',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).LAK',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'LBP',
            name: 'Lebanese Pound',
            names: {
              en: 'Lebanese Pound',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).LBP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'LKR',
            name: 'Sri Lanka Rupee',
            names: {
              en: 'Sri Lanka Rupee',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).LKR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'LRD',
            name: 'Liberian Dollar',
            names: {
              en: 'Liberian Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).LRD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'LSL',
            name: 'Loti',
            names: {
              en: 'Loti',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).LSL',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'LTL',
            name: 'Lithuanian Litas',
            names: {
              en: 'Lithuanian Litas',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).LTL',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'LUF',
            name: 'Luxembourg Franc',
            names: {
              en: 'Luxembourg Franc',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).LUF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'LVL',
            name: 'Latvian Lats',
            names: {
              en: 'Latvian Lats',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).LVL',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'LYD',
            name: 'Libyan Dinar',
            names: {
              en: 'Libyan Dinar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).LYD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MAD',
            name: 'Moroccan Dirham',
            names: {
              en: 'Moroccan Dirham',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MAD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MDL',
            name: 'Moldovan Leu',
            names: {
              en: 'Moldovan Leu',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MDL',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MGA',
            name: 'Malagasy Ariary',
            names: {
              en: 'Malagasy Ariary',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MGA',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MGF',
            name: 'Malagasy Franc',
            names: {
              en: 'Malagasy Franc',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MGF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MKD',
            name: 'Denar',
            names: {
              en: 'Denar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MKD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MMK',
            name: 'Kyat',
            names: {
              en: 'Kyat',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MMK',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MNT',
            name: 'Tugrik',
            names: {
              en: 'Tugrik',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MNT',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MOP',
            name: 'Pataca',
            names: {
              en: 'Pataca',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MOP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MRO',
            name: 'Ouguiya',
            names: {
              en: 'Ouguiya',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MRO',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MTL',
            name: 'Maltese Lira',
            names: {
              en: 'Maltese Lira',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MTL',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MUR',
            name: 'Mauritius Rupee',
            names: {
              en: 'Mauritius Rupee',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MUR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MVR',
            name: 'Rufiyaa',
            names: {
              en: 'Rufiyaa',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MVR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MWK',
            name: 'Kwacha',
            names: {
              en: 'Kwacha',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MWK',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MXN',
            name: 'Mexican Peso',
            names: {
              en: 'Mexican Peso',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MXN',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MXP',
            name: 'Mexican peso (old)',
            names: {
              en: 'Mexican peso (old)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MXP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MXV',
            name: 'Mexican Unidad de Inversion (UDI)',
            names: {
              en: 'Mexican Unidad de Inversion (UDI)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MXV',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MYR',
            name: 'Malaysian Ringgit',
            names: {
              en: 'Malaysian Ringgit',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MYR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MZM',
            name: 'Metical (old)',
            names: {
              en: 'Metical (old)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MZM',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'MZN',
            name: 'Metical',
            names: {
              en: 'Metical',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).MZN',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'NAD',
            name: 'Namibia Dollar',
            names: {
              en: 'Namibia Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).NAD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'NGN',
            name: 'Naira',
            names: {
              en: 'Naira',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).NGN',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'NIO',
            name: 'Cordoba Oro',
            names: {
              en: 'Cordoba Oro',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).NIO',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'NLG',
            name: 'Netherlands Guilder',
            names: {
              en: 'Netherlands Guilder',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).NLG',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'NOK',
            name: 'Norwegian Krone',
            names: {
              en: 'Norwegian Krone',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).NOK',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'NPR',
            name: 'Nepalese Rupee',
            names: {
              en: 'Nepalese Rupee',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).NPR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'NZD',
            name: 'New Zealand Dollar',
            names: {
              en: 'New Zealand Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).NZD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'OMR',
            name: 'Rial Omani',
            names: {
              en: 'Rial Omani',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).OMR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'PAB',
            name: 'Balboa',
            names: {
              en: 'Balboa',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).PAB',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'PEN',
            name: 'Nuevo Sol',
            names: {
              en: 'Nuevo Sol',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).PEN',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'PGK',
            name: 'Kina',
            names: {
              en: 'Kina',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).PGK',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'PHP',
            name: 'Philippine Peso',
            names: {
              en: 'Philippine Peso',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).PHP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'PKR',
            name: 'Pakistan Rupee',
            names: {
              en: 'Pakistan Rupee',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).PKR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'PLN',
            name: 'Zloty',
            names: {
              en: 'Zloty',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).PLN',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'PTE',
            name: 'Portugese Escudo',
            names: {
              en: 'Portugese Escudo',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).PTE',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'PYG',
            name: 'Guarani',
            names: {
              en: 'Guarani',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).PYG',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'QAR',
            name: 'Qatari Rial',
            names: {
              en: 'Qatari Rial',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).QAR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ROL',
            name: 'Romanian Leu (old)',
            names: {
              en: 'Romanian Leu (old)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ROL',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'RON',
            name: 'Romanian Leu',
            names: {
              en: 'Romanian Leu',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).RON',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'RSD',
            name: 'Serbian Dinar',
            names: {
              en: 'Serbian Dinar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).RSD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'RUB',
            name: 'Russian Ruble',
            names: {
              en: 'Russian Ruble',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).RUB',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'RWF',
            name: 'Rwanda Franc',
            names: {
              en: 'Rwanda Franc',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).RWF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SAR',
            name: 'Saudi Riyal',
            names: {
              en: 'Saudi Riyal',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SAR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SBD',
            name: 'Solomon Islands Dollar',
            names: {
              en: 'Solomon Islands Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SBD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SCR',
            name: 'Seychelles Rupee',
            names: {
              en: 'Seychelles Rupee',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SCR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SDD',
            name: 'Sudanese Dinar',
            names: {
              en: 'Sudanese Dinar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SDD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SDG',
            name: 'Sudanese Pound',
            names: {
              en: 'Sudanese Pound',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SDG',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SEK',
            name: 'Swedish Krona',
            names: {
              en: 'Swedish Krona',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SEK',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SGD',
            name: 'Singapore Dollar',
            names: {
              en: 'Singapore Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SGD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SHP',
            name: 'Saint Helena Pound',
            names: {
              en: 'Saint Helena Pound',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SHP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SIT',
            name: 'Slovenian Tolar',
            names: {
              en: 'Slovenian Tolar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SIT',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SKK',
            name: 'Slovak Koruna',
            names: {
              en: 'Slovak Koruna',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SKK',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SLL',
            name: 'Leone',
            names: {
              en: 'Leone',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SLL',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SOS',
            name: 'Somali Shilling',
            names: {
              en: 'Somali Shilling',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SOS',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SRD',
            name: 'Surinam Dollar',
            names: {
              en: 'Surinam Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SRD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SSP',
            name: 'South-Sudanese Pound',
            names: {
              en: 'South-Sudanese Pound',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SSP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'STD',
            name: 'Dobra',
            names: {
              en: 'Dobra',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).STD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SVC',
            name: 'El Salvador Colon',
            names: {
              en: 'El Salvador Colon',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SVC',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SYP',
            name: 'Syrian Pound',
            names: {
              en: 'Syrian Pound',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SYP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SZL',
            name: 'Lilangeni',
            names: {
              en: 'Lilangeni',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SZL',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'THB',
            name: 'Baht',
            names: {
              en: 'Baht',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).THB',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'TJS',
            name: 'Somoni',
            names: {
              en: 'Somoni',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TJS',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'TMM',
            name: 'Turkmen Manat (old)',
            names: {
              en: 'Turkmen Manat (old)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TMM',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'TMT',
            name: 'Turkmen Manat',
            names: {
              en: 'Turkmen Manat',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TMT',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'TND',
            name: 'Tunisian Dinar',
            names: {
              en: 'Tunisian Dinar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TND',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'TOP',
            name: "Pa'anga",
            names: {
              en: "Pa'anga",
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TOP',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'TRL',
            name: 'Turkish Lira (old)',
            names: {
              en: 'Turkish Lira (old)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TRL',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'TRY',
            name: 'Turkish Lira',
            names: {
              en: 'Turkish Lira',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TRY',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'TTD',
            name: 'Trinidad and Tobago Dollar',
            names: {
              en: 'Trinidad and Tobago Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TTD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'TWD',
            name: 'New Taiwan Dollar',
            names: {
              en: 'New Taiwan Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TWD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'TZS',
            name: 'Tanzanian Shilling',
            names: {
              en: 'Tanzanian Shilling',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).TZS',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'UAH',
            name: 'Hryvnia',
            names: {
              en: 'Hryvnia',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).UAH',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'UGX',
            name: 'Uganda Shilling',
            names: {
              en: 'Uganda Shilling',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).UGX',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'USD',
            name: 'US Dollar',
            names: {
              en: 'US Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).USD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'USN',
            name: 'Dollar (Next day)',
            names: {
              en: 'Dollar (Next day)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).USN',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'USS',
            name: 'Dollar (Same day)',
            names: {
              en: 'Dollar (Same day)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).USS',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'UYI',
            name: 'Uruguay Peso en Unidades Indexadas',
            names: {
              en: 'Uruguay Peso en Unidades Indexadas',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).UYI',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'UYU',
            name: 'Peso Uruguayo',
            names: {
              en: 'Peso Uruguayo',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).UYU',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'UZS',
            name: 'Uzbekistan Sum',
            names: {
              en: 'Uzbekistan Sum',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).UZS',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'VEB',
            name: 'Bolivar (old)',
            names: {
              en: 'Bolivar (old)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).VEB',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'VEF',
            name: 'Bolivar fuerte',
            names: {
              en: 'Bolivar fuerte',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).VEF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'VND',
            name: 'Dong',
            names: {
              en: 'Dong',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).VND',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'VUV',
            name: 'Vatu',
            names: {
              en: 'Vatu',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).VUV',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'WST',
            name: 'Tala',
            names: {
              en: 'Tala',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).WST',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XAF',
            name: 'CFA Franc - BEAC',
            names: {
              en: 'CFA Franc - BEAC',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XAF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XAG',
            name: 'Silver',
            names: {
              en: 'Silver',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XAG',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XAU',
            name: 'Gold',
            names: {
              en: 'Gold',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XAU',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XBA',
            name: 'Bond Markets Units European Composite Unit (EURCO)',
            names: {
              en: 'Bond Markets Units European Composite Unit (EURCO)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XBA',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XBB',
            name: 'European Monetary Unit (E.M.U.-6)',
            names: {
              en: 'European Monetary Unit (E.M.U.-6)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XBB',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XBC',
            name: 'European Unit of Account 9 (E.U.A.-9)',
            names: {
              en: 'European Unit of Account 9 (E.U.A.-9)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XBC',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XBD',
            name: 'European Unit of Account 17 (E.U.A.-17)',
            names: {
              en: 'European Unit of Account 17 (E.U.A.-17)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XBD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XCD',
            name: 'East Caribbean Dollar',
            names: {
              en: 'East Caribbean Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XCD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XDR',
            name: 'Special Drawing Rights',
            names: {
              en: 'Special Drawing Rights',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XDR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XEU',
            name: 'European Currency Unit (ECU)',
            names: {
              en: 'European Currency Unit (ECU)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XEU',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XFO',
            name: 'Gold-Franc',
            names: {
              en: 'Gold-Franc',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XFO',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XFU',
            name: 'UIC-Franc',
            names: {
              en: 'UIC-Franc',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XFU',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XOF',
            name: 'CFA Franc - BCEAO',
            names: {
              en: 'CFA Franc - BCEAO',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XOF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XPD',
            name: 'Palladium',
            names: {
              en: 'Palladium',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XPD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XPF',
            name: 'CFP Franc',
            names: {
              en: 'CFP Franc',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XPF',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XPT',
            name: 'Platinum',
            names: {
              en: 'Platinum',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XPT',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XTS',
            name: 'Code specifically reserved for testing purposes',
            names: {
              en: 'Code specifically reserved for testing purposes',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XTS',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'XXX',
            name: 'Transactions whithout currency',
            names: {
              en: 'Transactions whithout currency',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).XXX',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'YER',
            name: 'Yemeni Rial',
            names: {
              en: 'Yemeni Rial',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).YER',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ZAR',
            name: 'Rand',
            names: {
              en: 'Rand',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ZAR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ZMK',
            name: 'Kwacha (old)',
            names: {
              en: 'Kwacha (old)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ZMK',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ZMW',
            name: 'Kwacha',
            names: {
              en: 'Kwacha',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ZMW',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ZWD',
            name: 'Zimbabwe Dollar (before Sept.2008)',
            names: {
              en: 'Zimbabwe Dollar (before Sept.2008)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ZWD',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ZWL',
            name: 'Zimbabwe Dollar (before Feb.2009)',
            names: {
              en: 'Zimbabwe Dollar (before Feb.2009)',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ZWL',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'ZWR',
            name: 'Zimbabwe Dollar',
            names: {
              en: 'Zimbabwe Dollar',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).ZWR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'NATCUR',
            name: 'National currency',
            names: {
              en: 'National currency',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).NATCUR',
                type: 'code',
              },
            ],
            parent: '5',
          },
          {
            id: 'SUBS',
            name: 'Subscriptions',
            names: {
              en: 'Subscriptions',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_UNIT(1.0).SUBS',
                type: 'code',
              },
            ],
          },
        ],
      },
      {
        id: 'CL_AIR_EMISSIONS_VAR',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD:CL_AIR_EMISSIONS_VAR(1.0)',
            type: 'codelist',
          },
        ],
        version: '1.0',
        agencyID: 'OECD',
        isFinal: true,
        name: 'Variable',
        names: {
          en: 'Variable',
        },
        isPartial: true,
        codes: [
          {
            id: 'TOT',
            name: 'Total man-made emissions',
            names: {
              en: 'Total man-made emissions',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).TOT',
                type: 'code',
              },
            ],
          },
          {
            id: 'MOB_TOT',
            name: 'Total Mobile Sources',
            names: {
              en: 'Total Mobile Sources',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).MOB_TOT',
                type: 'code',
              },
            ],
            parent: 'TOT',
          },
          {
            id: 'MOB_ROAD',
            name: 'Road Transport',
            names: {
              en: 'Road Transport',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).MOB_ROAD',
                type: 'code',
              },
            ],
            parent: 'MOB_TOT',
          },
          {
            id: 'MOB_OTHER',
            name: 'Other Mobile Sources',
            names: {
              en: 'Other Mobile Sources',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).MOB_OTHER',
                type: 'code',
              },
            ],
            parent: 'MOB_TOT',
          },
          {
            id: 'STAT_TOT',
            name: 'Total Stationary Sources',
            names: {
              en: 'Total Stationary Sources',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).STAT_TOT',
                type: 'code',
              },
            ],
            parent: 'TOT',
          },
          {
            id: 'STAT_POW',
            name: 'Power stations',
            names: {
              en: 'Power stations',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).STAT_POW',
                type: 'code',
              },
            ],
            parent: 'STAT_TOT',
          },
          {
            id: 'STAT_COMB',
            name: 'Combustion',
            names: {
              en: 'Combustion',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).STAT_COMB',
                type: 'code',
              },
            ],
            parent: 'STAT_TOT',
          },
          {
            id: 'STAT_COMB_IND',
            name: 'Industrial combustion',
            names: {
              en: 'Industrial combustion',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).STAT_COMB_IND',
                type: 'code',
              },
            ],
            parent: 'STAT_COMB',
          },
          {
            id: 'STAT_COMB_OTHER',
            name: 'Other combustion',
            names: {
              en: 'Other combustion',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).STAT_COMB_OTHER',
                type: 'code',
              },
            ],
            parent: 'STAT_COMB',
          },
          {
            id: 'STAT_IND_PROC',
            name: 'Industrial processes and product use',
            names: {
              en: 'Industrial processes and product use',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).STAT_IND_PROC',
                type: 'code',
              },
            ],
            parent: 'STAT_TOT',
          },
          {
            id: 'MISC',
            name: 'Miscellaneous',
            names: {
              en: 'Miscellaneous',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).MISC',
                type: 'code',
              },
            ],
            parent: 'STAT_TOT',
          },
          {
            id: 'MISC_AGR',
            name: 'Agriculture',
            names: {
              en: 'Agriculture',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).MISC_AGR',
                type: 'code',
              },
            ],
            parent: 'STAT_TOT',
          },
          {
            id: 'MISC_WASTE',
            name: 'Waste',
            names: {
              en: 'Waste',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).MISC_WASTE',
                type: 'code',
              },
            ],
            parent: 'STAT_TOT',
          },
          {
            id: 'INDEX_1990',
            name: 'Total emissions, Index 1990 = 100',
            names: {
              en: 'Total emissions, Index 1990 = 100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).INDEX_1990',
                type: 'code',
              },
            ],
          },
          {
            id: 'TOT_CAP',
            name: 'Total emissions per capita',
            names: {
              en: 'Total emissions per capita',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).TOT_CAP',
                type: 'code',
              },
            ],
          },
          {
            id: 'TOT_GDP',
            name: 'Total emissions per unit of GDP, Kg per 1000 USD',
            names: {
              en: 'Total emissions per unit of GDP, Kg per 1000 USD',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).TOT_GDP',
                type: 'code',
              },
            ],
          },
          {
            id: 'INDEX_2000',
            name: 'Total emissions, Index 2000 = 100',
            names: {
              en: 'Total emissions, Index 2000 = 100',
            },
            links: [
              {
                rel: 'self',
                urn: 'urn:sdmx:org.sdmx.infomodel.codelist.Code=OECD:CL_AIR_EMISSIONS_VAR(1.0).INDEX_2000',
                type: 'code',
              },
            ],
          },
        ],
      },
    ],
    dataStructures: [
      {
        id: 'AIR_EMISSIONS',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataStructure=OECD:AIR_EMISSIONS(1.0)',
            type: 'datastructure',
          },
        ],
        version: '1.0',
        agencyID: 'OECD',
        isFinal: true,
        name: 'Emissions of air pollutants',
        names: {
          en: 'Emissions of air pollutants',
        },
        dataStructureComponents: {
          attributeList: {
            id: 'AttributeDescriptor',
            links: [
              {
                rel: 'self',
                urn:
                  'urn:sdmx:org.sdmx.infomodel.datastructure.AttributeDescriptor=OECD:AIR_EMISSIONS(1.0).AttributeDescriptor',
                type: 'attributedescriptor',
              },
            ],
            attributes: [
              {
                id: 'OBS_STATUS',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=OECD:AIR_EMISSIONS(1.0).OBS_STATUS',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity:
                  'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).OBS_STATUS',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD:CL_AIR_EMISSIONS_OBS_STATUS(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: {
                  primaryMeasure: 'OBS_VALUE',
                },
              },
              {
                id: 'TIME_FORMAT',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=OECD:AIR_EMISSIONS(1.0).TIME_FORMAT',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity:
                  'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).TIME_FORMAT',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD:CL_AIR_EMISSIONS_TIME_FORMAT(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: {
                  dimensions: ['COU', 'POL', 'VAR'],
                },
              },
              {
                id: 'UNIT',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=OECD:AIR_EMISSIONS(1.0).UNIT',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).UNIT',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD:CL_AIR_EMISSIONS_UNIT(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: {
                  dimensions: ['COU', 'POL', 'VAR'],
                },
              },
              {
                id: 'REFERENCEPERIOD',
                links: [
                  {
                    rel: 'self',
                    urn:
                      'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=OECD:AIR_EMISSIONS(1.0).REFERENCEPERIOD',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity:
                  'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).REFERENCEPERIOD',
                localRepresentation: {
                  enumeration:
                    'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD:CL_AIR_EMISSIONS_REFERENCEPERIOD(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: {
                  dimensions: ['COU', 'POL', 'VAR'],
                },
              },
              {
                id: 'POWERCODE',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.DataAttribute=OECD:AIR_EMISSIONS(1.0).POWERCODE',
                    type: 'dataattribute',
                  },
                ],
                conceptIdentity:
                  'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).POWERCODE',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD:CL_AIR_EMISSIONS_POWERCODE(1.0)',
                },
                assignmentStatus: 'Conditional',
                attributeRelationship: {
                  dimensions: ['COU', 'POL', 'VAR'],
                },
              },
            ],
          },
          dimensionList: {
            id: 'DimensionDescriptor',
            links: [
              {
                rel: 'self',
                urn:
                  'urn:sdmx:org.sdmx.infomodel.datastructure.DimensionDescriptor=OECD:AIR_EMISSIONS(1.0).DimensionDescriptor',
                type: 'dimensiondescriptor',
              },
            ],
            dimensions: [
              {
                id: 'COU',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=OECD:AIR_EMISSIONS(1.0).COU',
                    type: 'dimension',
                  },
                ],
                position: 0,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).COU',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD:CL_AIR_EMISSIONS_COU(1.0)',
                },
              },
              {
                id: 'POL',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=OECD:AIR_EMISSIONS(1.0).POL',
                    type: 'dimension',
                  },
                ],
                position: 1,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).POL',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD:CL_AIR_EMISSIONS_POL(1.0)',
                },
              },
              {
                id: 'VAR',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.Dimension=OECD:AIR_EMISSIONS(1.0).VAR',
                    type: 'dimension',
                  },
                ],
                position: 2,
                type: 'Dimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).VAR',
                localRepresentation: {
                  enumeration: 'urn:sdmx:org.sdmx.infomodel.codelist.Codelist=OECD:CL_AIR_EMISSIONS_VAR(1.0)',
                },
              },
            ],
            timeDimensions: [
              {
                id: 'TIME_PERIOD',
                links: [
                  {
                    rel: 'self',
                    urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.TimeDimension=OECD:AIR_EMISSIONS(1.0).TIME_PERIOD',
                    type: 'timedimension',
                  },
                ],
                position: 3,
                type: 'TimeDimension',
                conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).YEA',
                localRepresentation: {
                  textFormat: {
                    textType: 'ObservationalTimePeriod',
                    isSequence: false,
                    isMultiLingual: false,
                  },
                },
              },
            ],
          },
          measureList: {
            id: 'MeasureDescriptor',
            links: [
              {
                rel: 'self',
                urn:
                  'urn:sdmx:org.sdmx.infomodel.datastructure.MeasureDescriptor=OECD:AIR_EMISSIONS(1.0).MeasureDescriptor',
                type: 'measuredescriptor',
              },
            ],
            primaryMeasure: {
              id: 'OBS_VALUE',
              links: [
                {
                  rel: 'self',
                  urn: 'urn:sdmx:org.sdmx.infomodel.datastructure.PrimaryMeasure=OECD:AIR_EMISSIONS(1.0).OBS_VALUE',
                  type: 'primarymeasure',
                },
              ],
              conceptIdentity: 'urn:sdmx:org.sdmx.infomodel.conceptscheme.Concept=OECD:AIR_EMISSIONS_CS(1.0).OBS_VALUE',
              localRepresentation: {
                textFormat: {
                  textType: 'Double',
                  isSequence: false,
                  isMultiLingual: false,
                },
              },
            },
          },
        },
      },
    ],
    contentConstraints: [
      {
        id: 'CR_A_AIR_EMISSIONS_DF',
        links: [
          {
            rel: 'self',
            urn: 'urn:sdmx:org.sdmx.infomodel.registry.ContentConstraint=OECD:CR_A_AIR_EMISSIONS_DF(1.0)',
            type: 'contentconstraint',
          },
        ],
        version: '1.0',
        agencyID: 'OECD',
        isFinal: false,
        name: 'Availability (A) for AIR_EMISSIONS_DF',
        names: {
          en: 'Availability (A) for AIR_EMISSIONS_DF',
        },
        annotations: [
          {
            title: 'A',
            type: 'ReleaseVersion',
          },
        ],
        type: 'Actual',
        constraintAttachment: {
          dataflows: ['urn:sdmx:org.sdmx.infomodel.datastructure.Dataflow=OECD:AIR_EMISSIONS_DF(1.0)'],
        },
        cubeRegions: [
          {
            isIncluded: true,
            keyValues: [
              {
                id: 'COU',
                values: ['AUS', 'AUT', 'BEL', 'CAN', 'CHE'],
              },
              {
                id: 'VAR',
                values: ['TOT', 'STAT_TOT', 'STAT_COMB_OTHER', 'MOB_ROAD', 'STAT_POW', 'TOT_CAP'],
              },
              {
                id: 'TIME_PERIOD',
                timeRange: {
                  startPeriod: {
                    period: '1990-01-01T00:00:00Z',
                    isInclusive: true,
                  },
                  endPeriod: {
                    period: '2017-12-31T23:59:59Z',
                    isInclusive: true,
                  },
                },
              },
            ],
          },
        ],
      },
    ],
  },
  meta: {
    schema:
      'https://raw.githubusercontent.com/sdmx-twg/sdmx-json/develop/structure-message/tools/schemas/1.0/sdmx-json-structure-schema.json',
    contentLanguages: ['en'],
    id: 'IDREF12276',
    prepared: '2020-05-26T12:18:22.4628466Z',
    test: false,
    sender: {
      id: 'Unknown',
    },
    receiver: [
      {
        id: 'Unknown',
      },
    ],
  },
};
