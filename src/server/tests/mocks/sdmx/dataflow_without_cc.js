import { over, omit, lensProp, lensPath, concat, pipe, flip } from 'ramda';
import dataflow from './dataflow';

module.exports = id =>
  pipe(
    over(lensProp('data'), omit(['contentConstraints'])),
    over(lensPath(['data', 'dataflows', 0, 'id']), flip(concat)('_without_cc')),
  )(dataflow(id));
