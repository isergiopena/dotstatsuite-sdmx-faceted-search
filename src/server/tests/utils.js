import { createContext } from 'jeto'
import nock from 'nock'
import ConfigProvider from '../configProvider'

const CONFIG_URL = 'http://configProvider'
const tenants = require('./tenants')
const SETTINGS = {
  spaces: {
    overwrite: {
      url: 'overwrite_url',
      headers: {
        header2: 'value22',
        header3: 'value33',
      },
    },
  },
}

export const TENANT = tenants['test']

export const tearContext = async CTX => {
  try {
    await CTX().mongo.dropDatabase()
    await CTX().http.close()
    await CTX().mongo.close()
    const solrClient = CTX().solr.getClient(TENANT)
    await solrClient.deleteAll()
  } catch (err) {
    console.error(err) // eslint-disable-line
    throw err
  }
}

export const initConfig = async (ctx = createContext({}), params = {}, options = {}) => {
  const server = { host: 'localhost' }
  const config = {
    configUrl: CONFIG_URL,
    gitHash: 'None',
    env: 'production',
    apiKey: 'secret',
    server,
    mongo: {
      url: process.env.CI ? 'mongodb://mongo:27017' : 'mongodb://localhost:27017',
      // dbName: `test-sfs-${uuid()}`,
      dbName: `test`,
    },
    solr: {
      host: process.env.CI ? 'solr' : '0.0.0.0',
      port: 8983,
      collection: 'test',
      logLevel: 0,
    },
  }

  nock(CONFIG_URL)
    .persist()
    .get(`/configs/tenants.json`)
    .reply(200, options.tenants || tenants)

  nock(CONFIG_URL)
    .persist()
    .get(`/configs/sfs.json`)
    .reply(200, params)

  nock(CONFIG_URL)
    .persist()
    .get(`/configs/test/sfs/settings.json`)
    .reply(200, SETTINGS)

  const configProvider = ConfigProvider(config)

  return ctx({ startTime: new Date(), config, configProvider })
}

export const waitFor = async fn => {
  return new Promise(resolve => {
    const tryFn = async () => {
      try {
        await fn()
        resolve()
      } catch (e) {
        setImmediate(tryFn)
      }
    }
    tryFn()
  })
}
