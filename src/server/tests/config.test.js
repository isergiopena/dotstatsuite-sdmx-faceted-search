import { prop } from 'ramda'
import axios from 'axios'
import { createContext } from 'jeto'
import initHttp from '../init/http'
import initServices from '../services'
import initCachedfacets from '../init/cachedFacets'
import initSolr from '../solr'
import initParams from '../init/loadParams'
import initConfigManager from '../init/configManager'
import initMongo from '../init/mongo'
import { TENANT, initConfig } from './utils'

let CTX

const tearContext = async () => {
  if (!CTX) return
  await CTX().mongo.dropDatabase()
  await CTX().http.close()
  await CTX().nsi.close()
  await CTX().mongo.close()
  await CTX().mongo.close()
  const solrClient = CTX().solr.getClient(TENANT)
  await solrClient.deleteAll()
}

const initContext = async () => {
  try {
    CTX = await initConfig(createContext)
      .then(initMongo)
      .then(initSolr)
      .then(initConfigManager)
      .then(initParams)
      .then(initCachedfacets)
      .then(initServices)
      .then(initHttp)
    await CTX().configManager.updateSchema(TENANT, CTX().config.fields)
  } catch (err) {
    console.error(err) // eslint-disable-line
  }
}

describe('API status service', () => {
  beforeAll(initContext)
  afterAll(tearContext)

  it('should get right config', async () => {
    const { http } = CTX()
    await axios({ method: 'get', url: `${http.url}/api/config`, headers: { 'x-tenant': TENANT.id } }).then(prop('data'))
  })
})
