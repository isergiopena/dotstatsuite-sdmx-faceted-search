import { localizeConfig } from '../dataflow';

const lang = 'fr';
const config = {
  outFields: [],
  searchFields: [],
  schema: [
    { name: 'prop1', field: lang => `prop1_${lang}` },
    { name: 'prop2', field: lang => `prop2_${lang}` },
  ],
};

describe('localize config helper', function() {
  it('should localize highlighting', function() {
    const { outHighlightingMapper } = localizeConfig(lang, { dataflows: config });
    const HP1 = 'HP1';
    const HP2 = 'HP2';
    const highlighting = {
      ID1: {
        prop1_fr: [HP1],
        prop2_fr: [HP2],
      },
      ID2: {
        prop1_fr: [],
      },
    };
    const lh = outHighlightingMapper(highlighting);
    expect(lh).toEqual({ ID1: { prop1: [HP1], prop2: [HP2] }, ID2: {} });
  });
});
