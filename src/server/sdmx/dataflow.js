import { path, toPairs, map, reduce, isNil, or, equals, isEmpty } from 'ramda'

const LIST = 'list'
const TREE = 'tree'

export const makeId = (datasourceId, { id }) => `${datasourceId}:${id}`
const ID = ({ id, agencyID, version, itemId }) =>
  itemId ? `${agencyID}:${id}:${version}:${itemId}` : `${agencyID}:${id}:${version}`

const getHasData = (hasNoContraints, hasData) => or(hasNoContraints, equals(hasData, true))

export const makeValues = ({ hasNoContraints }) => values => {
  const hnodes = reduce(
    (acc, { id, names, parentId, hasData }) => {
      acc[id] = { id, name: names, children: [], parent: parentId, hasData: getHasData(hasNoContraints, hasData) }
      return acc
    },
    {},
    values,
  )
  const roots = reduce(
    /* eslint-disable no-unused-vars */
    (acc, [_, n]) => {
      const parentNode = hnodes[n.parent]
      if (!n.parent) return [...acc, n]
      else {
        parentNode.children.push(n)
        return acc
      }
    },
    [],
    toPairs(hnodes),
  )
  return roots
}

const makeDimension = options => ({ id, names, values }) => {
  const children = makeValues(options)(values)
  return {
    id,
    name: names,
    facet: children.length === values.length ? LIST : TREE,
    children,
  }
}

const urn2Ref = urn => {
  if (!urn) return
  const res = urn.match(/^urn:sdmx:org\.sdmx\.infomodel\.(\w*).(\w*)=(.*):(.*)\((.*)\)(?:\.(.*))?$/)
  if (!res) throw new Error(400, `Malformed urn: ${urn}`)
  const [, group, type, agencyID, id, version, itemId] = res
  return { agencyID, id, version, itemId }
}

const getCodeListValues = (constraints, codelists) => {
  const res = []
  for (const code of codelists.children) {
    res.push({
      ...code,
      name: code.names,
      hasData: constraints?.has(code.id),
      children: getCodeListValues(constraints, code),
    })
  }
  return res
}

const getDimensions = (
  { dimensionValuesLimit = Infinity },
  { dataStructure, codelists, conceptSchemes, categories, constraints, dataflow },
) => {
  if (!dataStructure) return []
  const res = []
  for (const { id, conceptIdentity, localRepresentation: { enumeration } = {} } of dataStructure
    ?.dataStructureComponents?.dimensionList?.dimensions || []) {
    const codelist = codelists[ID(urn2Ref(enumeration))]
    const conceptScheme = conceptSchemes[ID(urn2Ref(conceptIdentity))]
    if (constraints && !constraints[id]) continue
    const codeListValues = getCodeListValues(constraints?.[id], codelist)
    if (codeListValues.length > dimensionValuesLimit) continue
    res.push({
      id,
      name: conceptScheme.names,
      children: codeListValues,
      valuesCount: constraints?.[id]?.size,
    })
  }
  return res
}

const getTimeDimensions = ({ dataStructure, conceptSchemes, dataflow }) => {
  if (!dataStructure) return []
  const res = []
  for (const { id, conceptIdentity } of dataStructure?.dataStructureComponents?.dimensionList?.timeDimensions || []) {
    const conceptScheme = conceptSchemes[ID(urn2Ref(conceptIdentity))]
    res.push({ id, name: conceptScheme.names })
  }
  return res
}
const makeDataflow = (
  { dimensionValuesLimit, datasourceId, externalUrl },
  structure,
  { codelists, conceptSchemes, categories, constraints },
) => dataflow => {
  const hasNoContraints = isNil(constraints)
  const dataStructure = structure.data.dataStructures?.[0]
  const dimensions = getDimensions(
    { dimensionValuesLimit },
    { dataStructure, codelists, conceptSchemes, categories, constraints, dataflow },
  )
  const timeDimensions = getTimeDimensions({ dataStructure, conceptSchemes, dataflow })

  const res = {
    fields: {
      id: makeId(datasourceId, dataflow),
      externalUrl,
      dataflowId: dataflow.id,
      version: dataflow.version,
      agencyId: dataflow.agencyID,
      datasourceId,
      name: dataflow.names, // SDMX std update
      description: dataflow.descriptions, // SDMX std update
      indexationDate: new Date(),
    },
    dimensions,
    timeDimensions,
    categories,
    hasNoContraints,
    hasEmptyConstraints: isEmpty(constraints),
  }

  const orderAnnotations = getAnnotations(structure.data.dataflows[0], { type: 'SEARCH_WEIGHT' })
  if (orderAnnotations.length) {
    const order = orderAnnotations[0]
    if (order.title || order.title === 0) res.fields.gorder = order.title
    if (order.texts) res.fields.lorder = order.texts
  }

  return res
}

const includesDateTimeRange = ({ validFrom, validTo }, date) => {
  if (validFrom && validTo) return new Date(validFrom) >= date && new Date(validTo) <= date
  if (validFrom) return new Date(validFrom) >= date
  if (validTo) return new Date(validTo) <= date
  return true
}

const getActualContentConstraints = structure => {
  let res
  if (!structure.data.contentConstraints) return res
  for (const contentConstraint of structure.data.contentConstraints) {
    if (contentConstraint.type !== 'Actual') continue
    if (!includesDateTimeRange(structure, new Date())) continue
    if (!res) res = {}
    for (const constraint of contentConstraint.cubeRegions || []) {
      if (!constraint.isIncluded) continue
      for (const { id, values } of constraint.keyValues || []) {
        res[id] = new Set([...(res[id] || []), ...(values || [])])
      }
    }
  }
  return res
}

const getConceptSchemes = structure => {
  const res = {}
  for (const conceptScheme of structure.data.conceptSchemes || []) {
    for (const { id, names } of conceptScheme.concepts || []) {
      res[`${ID(conceptScheme)}:${id}`] = { names }
    }
  }
  return res
}

const getCodeLists = structure => {
  const res = {}
  for (const codelist of structure.data.codelists || []) {
    const roots = {}
    const nodes = {}
    for (const { annotations, id, names, parent } of codelist.codes || []) {
      const node = { id, annotations, names, children: [] }
      if (!nodes[id]) nodes[id] = node
      else nodes[id].annotations = annotations
      if (!parent) roots[id] = node
      else {
        if (nodes[parent]) nodes[parent].children.push(node)
        else {
          nodes[parent] = { id: parent, children: [node] }
        }
      }
    }
    res[ID(codelist)] = { names: codelist.names, children: Object.values(roots) }
  }
  return res
}

const getCategorySchemes = structure => {
  const res = []
  const getCats = (scheme, res = []) => {
    for (const cat of scheme) {
      if (cat.categories) {
        const rescat = { id: cat.id, name: cat.names, annotations: cat.annotations, children: [] }
        res.push(rescat)
        getCats(cat.categories, rescat.children)
      } else {
        res.push({ id: cat.id, name: cat.names, annotations: cat.annotations })
      }
    }
    return res
  }
  return getCats(structure.data?.categorySchemes || [])
}

const makeDataflows = options => structure => {
  const constraints = getActualContentConstraints(structure)
  const categories = getCategorySchemes(structure)
  const codelists = getCodeLists(structure)
  const conceptSchemes = getConceptSchemes(structure)

  return map(
    makeDataflow(options, structure, { codelists, conceptSchemes, categories, constraints }),
    path(['data', 'dataflows'], structure),
  )
}

export default (input, options) => ({
  dataflows: () => makeDataflows(options)(input),
})

export const getAnnotations = (structure, { type, title } = {}) => {
  const res = []
  for (const anno of structure?.annotations || []) {
    if (type && anno.type !== type) continue
    if (title && anno.title !== title) continue
    res.push(anno)
  }
  return res
}
