import {
  curry,
  prop,
  and,
  has,
  pipe,
  pathOr,
  match,
  when,
  isNil,
  always,
  slice,
  map,
  any,
  anyPass,
  complement,
} from 'ramda'
import got from 'got'
import urljoin from 'url-join'
import debug from '../debug'

export function SDMXError(message) {
  this.message = message
}
SDMXError.prototype = Object.create(Error.prototype)
SDMXError.prototype.constructor = SDMXError

const toNumber = v => {
  const [major = 0, minor = 0, patch = 0] = v.split('.')
  return major * 100 + minor * 10 + patch * 1
}

export const greaterThan = (v1, v2) => toNumber(v1) > toNumber(v2)

export const getParentId = (ancestors, id) => {
  const parentId = prop(id, ancestors)
  if (and(isNil(parentId), has(id, ancestors))) return parentId // root
  if (isNil(parentId)) return id
  return getParentId(ancestors, parentId)
}

export const LANGS = 'en,fr,es,it,ar,km,de,th,nl'

export const ACCEPT_HEADER = {
  structure: 'application/vnd.sdmx.structure+json;version=1.0;urn=true',
  data: 'application/vnd.sdmx.data+json;version=1.0.0-wd;urn=true',
}

export const getStructure = async (
  {
    spaceUrl,
    resourceType,
    agencyId = 'all',
    resourceId = 'all',
    version = 'all',
    references = 'none',
    detail = 'allstubs',
  },
  options = {},
) => {
  const url = urljoin(
    spaceUrl,
    resourceType,
    agencyId,
    resourceId,
    version,
    `?references=${references}`,
    `?detail=${detail}`,
  )
  const headers = { Accept: ACCEPT_HEADER.structure, 'Accept-Language': LANGS, ...options.headers }

  try {
    debug.info(`request ${url}`)
    return await got(url, { headers }).json()
  } catch (err) {
    err.url = url
    err.code = err.response?.statusCode || err.code
    throw err
  }
}

export const ID = curry(
  (resourceType, { agencyId, id, version, agencyID }) => `${resourceType}:${agencyId || agencyID}:${id}:${version}`,
)

const parseNumber = when(complement(isNil), Number)

export const parseDataRange = response =>
  pipe(
    pathOr('', ['headers', 'content-range']),
    match(/values (\d+)-(\d+)\/(\d+)/),
    when(isNil, always([])),
    slice(1, 4),
    map(parseNumber),
    ([start, end, total]) => {
      if (any(anyPass([isNil, isNaN]))([start, end, total])) {
        return {}
      }
      const count = end - start + 1
      return { count, total }
    },
  )(response)
// export const rlabel = ({ dataSpaceId, resourceType, agencyID, agencyId, id, version }) => `${dataSpaceId}/${resourceType}@${agencyID || agencyId}:${id}(${version})`
export const rlabel = ({ resourceType, agencyID, agencyId, id, version }) => ({
  resourceType,
  agencyID: agencyID || agencyId,
  id,
  version,
})
