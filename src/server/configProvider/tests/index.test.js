import got from 'got'
import { createContext } from 'jeto'
import ConfigProvider from '..'

const CONFIG_URL = 'http://FAKE_URI'
const MEMBER = { id: 1 }

jest.spyOn(got, 'get').mockImplementation(() => ({ json: () => Promise.resolve({ [MEMBER.id]: MEMBER }) }))

describe('server | configProvider', () => {
  it('should get partner', async () => {
    const config = { configUrl: CONFIG_URL }
    const provider = ConfigProvider(createContext({ config }))
    const member = await provider.getTenant(MEMBER.id)
    expect(member).toEqual(MEMBER)
  })
})
