import { compose, filter, map, values, prop } from 'ramda';
import { isHighlight, isOutable, isSearchable, isLocalized, isFacet } from './types';

const makeFacet = facet => ({
  name: facet.name,
  _name: facet._name,
  encoded: facet.encoded,
  type: facet.subType,
  op: facet.op,
  localized: facet.localized,
});
const makeFacets = compose(values, map(makeFacet), filter(isFacet));

const makeAttr = attr => {
  const res = { name: attr.name };
  if (isLocalized(attr)) res.field = lang => `${attr.name}_${lang}_${attr.ext}`;
  else if (attr.ext) res.field = `${attr.name}_${attr.ext}`;
  else res.field = attr.name;
  if (isFacet(attr)) res.ID = attr.id;
  if (attr.weight) res.weight = attr.weight;
  return res;
};

const makeSchema = compose(values, map(makeAttr));

const makeHighlighting = schema => ({
  fields: compose(values, map(prop('name')), filter(isHighlight))(schema),
});

const makeOutable = compose(
  values,
  map(prop('name')),
  // filter(either(isOutable, isFacet)),
  filter(isOutable),
);
const makeSearchable = compose(values, map(prop('name')), filter(isSearchable));

const Schema = schema => ({
  facets: makeFacets(schema),
  schema: makeSchema(schema),
  highlighting: makeHighlighting(schema),
  outFields: makeOutable(schema),
  searchFields: makeSearchable(schema),
});

export default Schema;
