import { tokenize } from '../tokenizer';

describe('Tokenizer', () => {
  it('should get terms', () => {
    const text =
      'coucou ici "les bidibules" name:voici -token -"encore ici" "bonjour bidi" -name:toto -attr:"une phrase"';

    const result = [
      { term: 'coucou' },
      { term: 'ici' },
      { term: 'les bidibules', phrase: true },
      { term: 'voici', tag: 'name' },
      { term: 'token', exclude: true },
      { term: 'encore ici', phrase: true, exclude: true },
      { term: 'bonjour bidi', phrase: true },
      { exclude: true, tag: 'name', term: 'toto' },
      { exclude: true, tag: 'attr', phrase: true, term: 'une phrase' },
    ];
    expect(tokenize(text)).toEqual(result);
  });
});
