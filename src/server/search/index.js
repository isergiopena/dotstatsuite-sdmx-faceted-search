import R from 'ramda'
import { localizeConfig } from '../dataflow'
import { tokenize } from './tokenizer'
import { decode } from '../utils'

const DEFAULT_ORDER_SCORE = 1
const specials = ['+', '-', '&', '!', '(', ')', '{', '}', '[', ']', '^', '"', '~', '*', '?', ':', '\\']
const regexp = new RegExp('(\\' + specials.join('|\\') + ')', 'g')
const escapedSearch = search => search.replace(regexp, '\\$1')
const exclude = ({ exclude }) => (exclude ? '-' : '+')
const boost = weight => (weight ? `^${weight}` : '')
const getSearchFields = (fields, search) => {
  const { tag, term } = search
  if (tag) {
    if (R.contains(tag, fields)) return [[tag, search]]
    return R.map(field => [field, { ...search, term: `${tag}\\:${term}` }], fields)
  }
  return R.map(field => [field, search], fields)
}

const baseFilterQuery = ({ getWeight, localizeField, baseSearchFields, searchValue }, search) =>
  R.compose(
    R.join(' '),
    R.map(
      ([fieldName, updatedSearch]) =>
        `${localizeField(fieldName)}:${searchValue(fieldName, updatedSearch)}${boost(getWeight(fieldName))}`,
    ),
  )(getSearchFields(baseSearchFields, search))

const baseFilters = (config, searches) => {
  if (!searches.trim()) return []
  return R.map(
    search => `${exclude(search)}(${baseFilterQuery(config, { ...search, term: escapedSearch(search.term) })})`,
    tokenize(searches),
  )
}

const localizeLOrder = localizeField => {
  // Manage case where lorder is not a schema field to avoid solr Error
  const res = localizeField('lorder')
  return res === 'lorder' ? 'lorder_i' : res
}

const baseQuery = localizeField =>
  `{!boost b="def(def(${localizeLOrder(localizeField)}, gorder_i), ${DEFAULT_ORDER_SCORE})"}+type_s:dataflow`

const makeFacetFilter = op =>
  R.compose(
    R.join(` ${op} `),
    R.map(x => `"${x}"`),
  )
const makeFacetsFilter = (acc, { field, values = [], op = 'OR' }) => [
  ...acc,
  `${field}:(${makeFacetFilter(op)(values)})`,
  // `{!tag=${name}}${field}:(${makeFacetFilter(op)(values)})`,
]
const transformInputFacets = (inputFacets, configFacets) => {
  const hfacets = R.compose(
    R.fromPairs,
    R.map(f => [f.encoded ? decode(f.name) : f.name, f]),
  )(configFacets)

  const makeQueryFacet = (acc, [name, values]) => {
    const facet = R.prop(name, hfacets)
    if (!facet || !values || !values.length) return acc
    return [...acc, { ...facet, values }]
  }
  return R.reduce(makeQueryFacet, [], R.toPairs(inputFacets))
}

const makeFilter = (filters, inputFacets, configFacets) =>
  R.reduce(makeFacetsFilter, filters, transformInputFacets(inputFacets, configFacets))

const makeFacet = ({ minCount: mincount }) => (acc, { name, field }) => ({
  ...acc,
  [name]: {
    type: 'terms',
    field,
    limit: -1,
    mincount,
    // domain: { excludeTags: name },
  },
})
const makeFacetsQuery = (facets, options) => R.reduce(makeFacet(options), {}, facets)

const makeFacets = (configFacets, options) => makeFacetsQuery(configFacets, options)

const refineFacets = (facets = [], data) => {
  const hEncodedFacets = R.reduce(
    (acc, f) => {
      acc[f.name] = f.encoded
      return acc
    },
    {},
    facets,
  )
  return R.compose(
    R.reduce((acc, [encodedName, { buckets }]) => {
      if (!buckets.length) return acc
      const name = hEncodedFacets[encodedName] ? decode(encodedName) : encodedName
      acc[name] = { buckets }
      return acc
    }, {}),
    R.toPairs,
  )(data)
}

const localizeSort = (xform, params = '') =>
  R.compose(
    R.join(', '),
    R.map(([_, field, order]) => R.join(' ', [xform(field), order])), // eslint-disable-line
    R.map(R.match(/^(\w+)\s*(asc|desc)*$/)),
    R.map(R.trim),
    R.split(','),
  )(params)

const makeHighlightingRequest = (hquery, highlighting) => {
  if (!highlighting || !hquery) return
  return R.assoc('query', hquery, highlighting)
}

const getOrders = (locale, facetOrders) => {
  return facetOrders?.(locale)
}

const outOrders = (orders, facets) => {
  if (!orders) return facets
  for (const facetId in facets) {
    for (const x of facets[facetId].buckets) {
      x.order = orders[x.val]
    }
  }
  return facets
}
const filterRequestedFields = (fl, fields) => {
  if (!fl || R.isEmpty(fl)) return fields
  const requested = new Set(fl)
  return R.filter(name => requested.has(name), fields)
}

const filterRequestedFacets = (fl, facets) => {
  if (!fl || R.isEmpty(fl)) return facets
  const requested = new Set(fl)
  return R.filter(facet => requested.has(facet._name), facets)
}

export const doSearch = async (
  facets,
  search,
  highlighting,
  lang,
  solr,
  config,
  facetOrders,
  start,
  rows,
  sort,
  fl,
  minCount,
) => {
  const localizedConfig = localizeConfig(lang, config)
  const {
    localizeField,
    outFields,
    configFacets,
    outDataflowsMapper,
    outHighlightingMapper,
    localizeHighlighting,
  } = localizedConfig

  // Add score out field
  outFields.push('score')

  const extQuery = baseFilters(localizedConfig, search)
  const query = `${[baseQuery(localizeField), ...extQuery].join(' ')}`
  const filter = makeFilter('', facets, configFacets)
  const facet = makeFacets(filterRequestedFacets(fl, configFacets), { minCount })

  const request = {
    query,
    filter,
    fields: filterRequestedFields(fl, outFields),
    facet,
    params: { start, rows },
  }
  const h = makeHighlightingRequest(
    extQuery.join(' '),
    localizeHighlighting(highlighting || config.dataflows.highlighting),
  )

  if (h) {
    request.params.hl = 'on'
    request.params['hl.q'] = h.query
    request.params['hl.fl'] = h.fields.join(' ')
  }

  if (sort) request.sort = localizeSort(localizeField, sort)
  const data = await solr.select(request)
  const orders = await getOrders(localizedConfig.lang, facetOrders)
  return {
    numFound: R.path(['response', 'numFound'], data),
    start: R.path(['response', 'start'], data),
    dataflows: outDataflowsMapper(R.path(['response', 'docs'], data)),
    facets: outOrders(orders, refineFacets(configFacets, R.omit(['count'], R.path(['facets'], data)))),
    highlighting: outHighlightingMapper(R.pathOr({}, ['highlighting'], data)),
  }
}
