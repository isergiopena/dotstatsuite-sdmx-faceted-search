# SDMX Faceted Search (aka sfs)


## Server

### target

`sfs` is a JSON http server that offer faceted search on SDMX dataflows. Search is enable on a configurable list of fields and faceted search on 2 types of facets: `list` and dataflow's `tree`. `sfs` use [solr's dynamic fields](https://cwiki.apache.org/confluence/display/solr/Dynamic+Fields) to index dataflows and an internal `schema` to request searches on abstract field names (`name` and not `name_en_s`). Localization is managed by the schema.


### Stack

`sfs` server is a [NodeJS](https://nodejs.org/en/) server. [ExpressJS](http://expressjs.com/) and [evtX](https://www.npmjs.com/package/evtx) libraries are used to manage http requests and define services around solr's search engine.

![sfs stack](./docs/sfs.png)


### Principles

* Field's names are abstracted: client will not use field's names used by Solr, but abstracted ones. A field's name depends on it's Solr type and selected lang. ex: for `description` with lang='fr', Solr field's name will be `description_fr_t`. Server's setup will allow to define fields in such a way client (UI) will use generic names, in our ex, just `description`.
* Fields are typed, by their name construction (`description_<lang>_t`) and should mapped to solr's dynamc fields
* A static schema is defined in server's config
* A dynamic schema is derivated from dataflows loaded via /admin/dataflows, one can access to dynamic config with GET /admin/config?api-key=<secret>

### Changelog

see [Changelog](CHANGELOG.md)


### Install

Install [nodejs](https://nodejs.org/en/)

Clone repository `sdmx-facet-search`.

```
$ cd sdmx-facet-search
$ yarn
```

### Server Config

#### Deployment variables

Config files are located in .env

Config's data:

```
# host (String): sfs host ip or dns entry
SERVER_HOST
# port number
SERVER_PORT

# port number mongodb://mongo:27017
MONGODB_URL
# name of the database
MONGODB_DATABASE

SOLR_HOST
SOLR_PORT

# URL to config server
CONFIG_URL

# apiKey (String): key to access /admin services
API_KEY

```

#### Resource config

At boot time server get the resource `/configs/sfs.json` and merge it with `init/params.js`

Merge is done at 2 levels, root and `fields`

if params likes:

```
export default {
  defaultLocale: 'en',
  fields: {
    ...
    field1: {
      type: ID_TYPE,
      out: true,
      search: false,
      name: 'field1',
    },
  }
}
```

and `sfs.json`:

```
export default {
  prop1: 'val',
  fields: {
    ...
    field1: {
      weight: 4
    },
  }
}
```

Resulting config will be:

```
{
  defaultLocale: 'en',
  prop1: 'val',
  fields: {
    ...
    field1: {
      type: ID_TYPE,
      out: true,
      search: false,
      name: 'field1',
      weight: 4
    },
  }
}
```

### Solr

#### Install

It's out of the scope of this project to explain how to install / setup a solr server, [Apache Solr reference Guide](https://cwiki.apache.org/confluence/display/solr/Apache+Solr+Reference+Guide) may help!

Just a small recipe to get how we've done it for our development environment:

```
$ docker run -d --restart=always -p <IP>:8983:8983 --name solr solr8.7 -c
```

#### Setup

Solr use one collection per organisation (see new tenant model)

Collections must be created before launching solr, eg, create a siscc collection 'siscc':

```
$ curl 'http://0.0.0.0:8983/solr/admin/collections?action=CREATE&name=siscc&numShards=1&collection.configName=_default'

```


### Run Server

#### Context

A running `solr` and mongo servers must be reachable to run `sfs`.

```
$ docker run -d --restart=always -p 27017:27017 --name mongo mongo
$ docker run -d --restart=always -p 8983:8983 --name solr solr8.7 -c
```


#### Development mode

Fill .env file

then launch server:

```
$ yarn start:srv

  running config ...
  Solr is ready to serve U with member collections: [siscc]
  ready to index SDMX
  server started on http://0.0.0.0:7300
  Available members: [siscc]
  🚀 server started

```

check:

* running config
* available collections
* available members


#### Production mode


Fill .env file 

```
$ yarn dist:srv
$ yarn dist:run
```


In both envs, params can be overwriten by shell variables

ex: we want to change SOLR's port:

```
$ SOLR_PORT=8888 yarn dist:run
```


### Health checks

2 services are available to supervise `sfs` server.

#### ping

```
$ curl http://localhost:3004/api/ping
{ ping: 'pong'}
```

#### healthcheck


```
$ curl http://localhost:3004/healthcheck
{
  ...
}
```

### Test

`solr` and `mongo` servers defined in `testing` environment must be reachable to run tests.

Create a 'test' collection in solr:

```
$ curl 'http://0.0.0.0:8983/solr/admin/collections?action=CREATE&name=test&numShards=1&collection.configName=_default'

```

`sfs` is packaged with many tests at different layers of the stack:

To execute tests, run:
```
$ yarn test
```
To check coverage:
```
$ yarn coverage
```

### Functional tests

Tests are located under `tests` folders close to code to test.

Among tests, those under `src/server/tests` are functional ones, the goal is to test `sfs` as a black box.

`./fixtures/index.js` includes fixtures files that define tests run by `api.js`.

A fixture file is made of a schema definition, data to load and tests to run. See `data1.js` to get how to proceed and add new tests.

## API Documentation

`sfs` is multi organisations which means that each API call should have an organisationId.

If there is no organisation, a 400 Bad Request error will be returned.
Providing a organisation can be done in 2 ways:
- as a query param: `&tenant=test`
- as a header: `x-tenant=test`

An organisation requires a collection (no need to create core anymore):
`$ curl 'http://0.0.0.0:8983/solr/admin/collections?action=CREATE&name=test&numShards=1&collection.configName=_default'`
The organisationId is used as the name of the collection, without this matching, DE won't be able to request sfs properly (bad request since sfs won't be able to find the proper collection).

### Tenant Model

`sfs` uses the `new tenant model` describes below:

```
{
  "siscc": { // organisationId
    "label": "SIS-CC",
    "spaces": {
      "qa-stable":{
        "url": "https://sxs-staging-siscc.dbmx.io/qa-stable/sdmx"
      },
      "demo-stable":{
        "label": "SIS-CC-demo-stable",
        "url": "https://sxs-staging-siscc.dbmx.io/demo-stable/sdmx"
      }
    },
    "datasources": {
      "qa-stable": {
        "dataSpaceId": "qa-stable",
        "indexed": true,
        "dataqueries": [
          { "version": "1.0", "categorySchemeId": "OECDCS1", "agencyId": "OECD" }
        ]
      },
      "demo-stable": {
        "dataSpaceId": "demo-stable",
        "indexed": true,
        "dataqueries": [
          { "version": "1.0", "categorySchemeId": "OECDCS1", "agencyId": "OECD" }
        ]
      }
    },
  }
}
```

`sfs` offers 2 APIs:

* **HTTP**, client http API to requests searches
* **JS**, internal library to manipulate solr's data. `sfs` is directly using solr [JSON Request API](https://cwiki.apache.org/confluence/display/solr/JSON+Request+API), but to help a low level Javascript [API](#js-api) is supplied

### HTTP API

`GET /api/config`: returns all available langs, the default one, facets definitions and values to dynamically build facets UIs.

```
$ curl http://localhost:3004/api/config?tenant=siscc
{
  langs: [ "fr", "en" ],
  defaultLang: "en",
  facets: {
    category: {
      type: "tree",
        buckets: [
          {
            val: "1|Music|Cassettes|",
            count: 6
          },
          ... other values
        ]
    },
    frequency: {
      type: "list",
      buckets: [
        {
        val: "Monthly (M)",
        count: 53
        },
      ]
    },
    interest_rate_type: {
      type: "list",
      buckets: [
        {
          val: "Bank interest rates (B)",
          count: 52
        },
        ... other values
      ]
    }
  }
}
```


`POST /api/search`: Search API

**Body**

Name | Type | Default | Description
--- | :---: | :---: | ---
`lang` | String | 'en' | [lang](#lang) to be used
`search` | String | '' | search pattern on [searchFields](#searchFields)
`facets` | Object | null | facet's names / values (see below)
`start` | Integer | null | Result start index
`rows` | Integer | null | Result window size
`minCount` | Integer | null | specifies the minimum counts required for a facet field to be included in the response
`sort` | String | null |  arranges search results in either ascending (asc) or descending (desc) order (see https://lucene.apache.org/solr/guide/6_6/common-query-parameters.html#CommonQueryParameters-ThesortParameter)
`sortMode` | String | null |  ['name', 'date', 'score'] shortcuts equivalent to sort qp values ['name asc, indexationDate desc', 'indexationDate desc', 'score asc']
`fl` | Array | null | list of fields to filter, list can contain any kinf of fields (facet, prop, attr) 


NB: `lang` can also be a http request's parameter
NB: default search '' will be converted by '*' server side
NB: `start' and `rows` allow pagination

**Response**

Name | Type | Description
--- | --- | ---
`dataflows` | Array | List of selected dataflows made of [outFields](#outFields)
`facets` | Object | facet search results (see below)
`start` | Integer | First dataflow index in result
`numFound` | Integer | Result cardinality

*Example*

```
$ cat body.json
{
  "search": "*rate*",
  "facets": {
    "reference_area": ["Austria (AT)", "Belgium (BE)"]
  }
}
$ curl http://localhost:3004/api/search  -H "Content-Type: application/json" -d @body.json
```

*Response*
```
{
    "dataflows": [
        {
            "id": "IRS",
            "agency": "ECB",
            "version": "1.0",
            "name": "Interest rate statistics"
        }
    ],
    "facets": {
        "count": 1,
        "reference_area": {
            "buckets": [
                {
                    "val": "Austria (AT)",
                    "count": 1
                }
            ]
        },
        "interest_rate_type": {
            "buckets": [
                {
                    "val": "Bank interest rates (B)",
                    "count": 1
                },
                {
                    "val": "Long-term interest rate for convergence purposes (L)",
                    "count": 1
                },
                {
                    "val": "Money market interest rates (M)",
                    "count": 1
                }
            ]
        },
        "frequency": {
            "buckets": [
                {
                    "val": "Daily (D)",
                    "count": 1
                },
                {
                    "val": "Monthly (M)",
                    "count": 1
                },
                {
                    "val": "Yearly (Y)",
                    "count": 1
                }
            ]
        }
    }
}
```


`GET /api/search`: Search API

Request used only to ease testing.

Accept only one param in query string `q` equivalent of `search` for post

```
$ curl "http://localhost:3004/api/search?q=Monthly"
```

### HTTP Admin


This api is protected by an api key (see `src/server/params/[env] apiKey`

All requests need a header made of `x-api-key`: api key

NB: accept also a query param `api-key`

#### Logs

`GET or POST /admin/logs`

Query params:

* userEmail: email
* dataspace: string 
* action: [indexOne, indexAll, deleteAll,deleteOne]
* submissionStart: date 
* submissionEnd: date
* executionStatus: string - (queued, inProgress, completed)
* executionOutcome: string - (success, warning, error)
* artefact: object { resourceType, agencyID, id, version } 

Example:

```
   const body = {
      userEmail: 'EMAIL',
      dataspace: 'demo-stable',
      submissionStart: '2017-01-01T00:00:00.000Z',
      submissionEnd: '2023-01-01T00:00:00.000Z',
      executionStatus: 'completed',
      executionOutcome: 'error',
      artefact: {
        resourceType: 'categoryscheme',
        agencyID: 'OECD',
        id: 'TEST',
        version: '1.0'
      },
   }
   
   POST /admin/logs { body }
   [
        {
            "userEmail": "EMAIL",
            "submissionTime": "2022-05-04T10:06:24.189Z",
            "executionStart": "2022-05-04T10:06:24.189Z",
            "executionStatus": "completed",
            "organisationId": "test",
            "action": "indexAll",
            "dataSourceIds": [
                "qa-stable",
                "demo-stable"
            ],
            "dataSpaceIds": [
                "qa-stable",
                "demo-stable"
            ],
            "logs": [
                {
                    "executionStart": "2022-05-04T10:06:24.243Z",
                    "message": "Cannot load http://qa-stable/categoryscheme/OECD/OECDCS1/1.0/?references=dataflow",
                    "logger": "loader.loadDataflowsFromQuery",
                    "status": "error",
                    "server": "sfs",
                    "exception": "Error: getaddrinfo EAI_AGAIN qa-stable\n    at GetAddrInfoReqWrap.onlookup [as oncomplete] (node:dns:71:26)",
                    "artefact": {
                        "resourceType": "categoryscheme",
                        "agencyID": "OECD",
                        "id": "OECDCS1",
                        "version": "1.0"
                    },
                    "dataSpaceId": "qa-stable"
                },
            ],
            "executionEnd": "2022-05-04T10:06:24.390Z",
            "outcome": "error",
            "id": 1651658784178
        }
    ]

```

`GET or POST /admin/logs`

The same request is used to load one request, just use `id` query param

Query params:

* id: requestId
 
Example:

```
    GET /admin/logs&id=1651658784178
```

Returns the request.



#### Dataflows
requires tenant (organisationId) query param

`POST /admin/dataflows`: index dataflows

* get tenant.datasources
* add dataflows to solr collection
* update existing dynamic sfs schema depending on dataflows added

* accept `mode` query param with values: 
** 'async' (default): requests loading and responds immediatly, 
** 'sync': waits end of loading and returns report,
** 'stream': streams loading events

returns nothing, just lauch loading process...

`DELETE /admin/dataflows`: drop all dataflows from Solr

#### Dataflow

requires tenant (organisationId) query param

`DELETE /admin/dataflow {spaceId, agencyId, id, version }`: delete dataflow { agencyId, id, version} from { tenant, spaceId } for all existing datasources

`POST /admin/dataflow {spaceId, agencyId, id, version }`: upsert dataflow { agencyId, id, version} from { tenant, spaceId } for all existing datasources

#### Others

`GET /admin/config?api-key=<secret>`: returns sfs dynamic config

`GET /admin/report?api-key=<secret>`: returns sfs in memory loadings statuses

`DELETE /admin/config`: delete dynamic config from SFS


### JS API

Internal API with sole purpose to ease the use of solr JSON Request API, see `src/server/solr/index.js`

```
  import initSolr from '../src/server/solr';
  import data from '../data/data.json'; // 1
  const config = { // 2
    solr: {
      host: 'localhost',
      port: 8983,
      verbose: true,
    }
  };

  initSolr({ config })
    .then(({ solr }) => solr.deleteAll() // 3
      .then(() =>  solr.add(data)) // 4
      .then(() => solr.select({ query: '*:*' })) // 5
```

This example is very close to a way to load data in a solr index.

1. JS structure to be indexed (see 'Index structured data' below)
2. Object similar to global sfs's config
3. `solr` is a Promise value returned by `initSolr`. It could be created also with a synchronous constructor.
3. `solr#deleteAll()` delete all data from the configured solr's collection, use it carefully!
4. `solr#add(data)` index data inside solr's collection. Use data without using any schema, so structure should be ready to be indexed as it.
5. `solr#select(data)` low level api to request searched on index. Do not use schema to transform query and response.


## Index structured data

`sfs` can use a static config or a dynamic one derived from loaded dataflows.

### Dynamic config

`sfs` can derive its schema (see below) from dataflows loaded via POST /admin/dataflows.

We just have to supply fields definitions in static config.

```
{
  ...
  fields: {
    id: { // special case for ID, ID fiel is mandatory
      type: ID_TYPE,
      out: true,
    },
    names: { // field name, we must get the same in POST /admin/dataflows body
      type: ATTR_TYPE, // field with translations
      name: 'name', // targetedName
      ext: TEXTFIELD_EXT, // solr dynamic field extension
      search: true, // allow user requests to search on this field
      out: true, // will be returned by /api/search requests
      highlight: true, // will be highlighted
      localized: true, // is localized
      weight: 2, // field will be boosted by 2
    },
}
```


### Static config

when data where loaded outside of sfs, later as ne idea of them so we need to define a schema to allow users requests.

The schema is configurable in `server/params` folder and will be used by API's services.

A schema must be defined within `sfs`:

* to help interface code to build the input structure
* to instruct solr how to index/search data
* to analyze and answer to API requests
* to manage localization of some fields

#### Schema definition

Schema is defined in config files under `dataflows` key, and read at server's boot time. Only the http API is aware of the schema not the JS one.

Name | Type  | Description
--- | --- | ---
`outFields` | Array | List of fields returned for each dataflow found
`searchFields` | Array | List of fields to search on
`schema` | Array of { `name`: String, `field`: (String or Function) } | Mapping Object `name` to index `field`
`facets` | Array of { `name`: String, `type`: ['list' or 'tree'], `op`: ['OR' or 'AND']} | Facet definition


`schema` is a mapping between indexed fields and abstract names used in `outFields`, `searchFields` and `facets.name` entries. `schema.field` can be String or a Function, latter will receive a `lang` parameter.

Sample

We have to manage 3 languages : ['en', 'cz', 'fr'] for the field `name`, an indexed dataflow looks like:

```
{
  id: 'IRS',
  name_en_s: 'Interest rate statistics',
  name_fr_s: 'Statistiques des taux d'intérêt',
  name_cz_s: 'Statistika úrokových sazeb',
}

```
Schema will be defined as:

```
dataflows: {
  outFields: ['id, 'name'],
  searchFields: ['name'],
  schema: [
    {
      name: 'name',
      field: lang => `name_${lang}_s`,
    }
  ]
}
```

On a search request:

```
$ cat body.json
{
  "lang": "fr",
  "search": "*taux*",
}
$ curl http://localhost:3004/api/search  -H "Content-Type: application/json" -d @body.json
```

We will get:

```
{
    "dataflows": [
        {
            "id": "IRS",
            "name": "Statistiques des taux d'intérêt"
        }
    ],
    "facets": {
        ...
    }
}
```

Indexed field names are never directly returned by `sfs`, but only corresponding names defined by schema depending on `lang` parameter.

Fields in this sample use [solr's dynamic fields](https://cwiki.apache.org/confluence/display/solr/Dynamic+Fields) definition, but it's up to you thanks to the schema to implement your own type's strategy.

We have a similar use case for facets. Let's say we would like to have a faceted search on field `reference_area` without localization. This time `reference_area` is a multi string field type named `reference_area_ss`.

Schema will be defined as:

```
dataflows: {
  outFields: ['id, 'name'],
  searchFields: ['name'],
  schema: [
    {
      name: 'name',
      field: lang => `name_${lang}_s`,
    },
    {
      name: 'reference_area',
      field: 'reference_area_ss',
    },
  ],
  facets: [
    {
      name: 'reference_area',
      type: 'list'
      op: 'OR',
    }
  ]
}
```

NB:

* `op` define the logical operator to use in case of a multiselection for one facet.
* `type` help to split facets in 2 worlds: 'list' and 'tree'. Latter implies to deal with hierarchical facets search, so we need to identify facets of that type.

### Client Queries Syntax

A search query can be expressed on one or many single terms, on phrase for all searchable fields.

Instead of searching on all searchable fields, it's possible to precise the field to search in.

All individual searches can be negated.


#### Single term search

```
{
    search: "one two three"
}
```

Server will execute for all searchable fields (here ['field1', 'field2']):

```
   +(<field1>:*one* <field2>:*one*) +(<field1>:*two* <field2>:*two*) +(<field1>:*three* <field2>:*three*)
```

#### Phrase term search

To match on an exact string, we must use single or double quotes in search field:

```
{
    search: "\"Et blanditiis quasi reprehenderit"\"
}
```


#### Tagged search

Previous searches were trying to match on all searchable fields, We can precise a field using a tag like:

```
{
    search: "name:\"Et blanditiis quasi reprehenderit"\"
    // or
    search: "description:blandi name:one"
}
```

#### Negate

We can negate all search terms by adding '-' in front of the term:

```
{
    search: "-interest ID564 -name:\"Et blanditiis"\"
}
```

### Highlighting results

see Dynamic config > highlight


#### Managing categories

Searching on `caterogy` fields implies to deal with hierarchical facets search, a concept which can mean different things to different people depending on data.
`sfs` tries to be more generic as possible and implements a basic approach that works well for most use cases by encoding the facet fields at index time in a specific manner.

Let's have an example:

```
Dataflow#1: C1 > C11
Dataflow#2: C1 > C11 > C111
Dataflow#3: C1 > C12, C2

```

In this example, we have dataflows associated with multiple categories, like Dataflow#3.

We must perform some index time processing on this flattened data in order to create the tokens needed for a hierarchical approach. When we index the data we create specially formatted fields that encode the depth information for each node that appears as part of the path, and include the hierarchy separated by a common separator (“depth/first level term/second level term/etc”). We also add additional terms for every ancestors in the original data.

Indexed fields

```
Dataflow#1: { cat: ['0|C1/', '1|C1|C11|'] }
Dataflow#2: { cat: ['0|C1/', '1|C1|C11|', '2|C1|C11|C111|'] }
Dataflow#3: { cat: ['0|C1/', '1|C1|C12|', '0|C2|'] }
```

if we define `cat` in our schema as a `tree` type facet like this:

```
dataflows: {
  outFields: ['id'],
  schema: [
    {
      name: 'cat',
      field: cat_ss,
    },
  ],
  facets: [
    {
      name: 'cat',
      type: 'tree'
    }
  ]
}
```

We can query like this:

```
$ cat body.json
{
  "facets": {
    "cat": ["0|C2|"]
  }
}
$ curl http://localhost:3004/api/search  -H "Content-Type: application/json" -d @body.json
```
And get:

```
{
    "dataflows": [
        {
            "id": "3",
        }
    ],
    "facets": {
        "count": 1,
        "cat": {
            "buckets": [
                {
                    "val": "0|C1|",
                    "count": 3
                },
                {
                    "val": "1|C1|C11|",
                    "count": 2
                },
                {
                    "val": "0|C2|",
                    "count": 1
                },
                {
                    "val": "1|C1|C12|",
                    "count": 1
                },
                {
                    "val": "2|C1|C11|C111|",
                    "count": 1
                }
            ]
        },
      }
  }
```

### Use Case

* Start with an empty mongo DB and an empty solr collection
* Load dataflows with POST /admin/dataflows (mind api key)
`sfs` will insert dataflows within solr and build a quite new dynamic config
* Check new schema with GET /admin/config
Your SPA is ready to use `sfs`
* First it needs to call /api/config, to get locales, i18n, searchFields and facets 
* Then search, search and search with POST /api/search


### Features

#### Order Annotations

When a dataflow has an order annotation:

```
{ type: 'SEARCH_WEIGHT', title: { 1 }, text: { en: 2, fr: 3 } }
```

SFS will create a document with 3 fields:

```
  gorder_i: 1
  lorder_en_i: 2
  lorder_fr_i: 3

```
if a search({ locale: 'fr' } match this document, document's score will equal solr score * lorder_fr_i

the generale score fct equals document.score * boost, where:

```
{!boost b="def(def(${lorder_<locale>_i}, gorder_i), 1)"}

```

That's all folks.
