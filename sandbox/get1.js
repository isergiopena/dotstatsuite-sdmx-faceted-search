var request = require('request');

const stripBorm = input => {
  if (input.charCodeAt(0) === 0xfeff) {
    return input.slice(1);
  }
  return input;
};

request(
  {
    url: 'https://nsi-qa-stable.siscc.org/rest/dataflow/OECD/DF_TEST_MONTH/1.0/?references=none&detail=allstubs',
    // url: 'https://nsi-qa-stable.siscc.org/rest/categoryscheme/OECD/OECDCS1/1.0?references=dataflow&detail=allstubs',
    headers: { Accept: 'application/vnd.sdmx.structure+json;version=1.0;urn=true' },
  },
  (err, res, body) => {
    console.log(err, res.statusCode);
    if (err) return console.error(err); // eslint-disable-line no-console
    const input = body.slice(0, 10);
    //for( let i = 0; i < 10; i++) console.log([input.charCodeAt(i),input.charAt(i)]);
    console.dir(JSON.parse(stripBorm(body)), { depth: null });
  },
);
