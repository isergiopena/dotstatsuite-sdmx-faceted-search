import request from 'axios';

request(
  {
    method: 'POST',
    // url: 'http://sfs-qa-oecd.redpelicans.com/admin/dataflows',
    url: 'http://localhost:7200/admin/dataflows?tenant=urox',
    //headers: { 'x-api-key': 'REakEtYP' },
    headers: { 'x-api-key': 'secret' },
    // strictSSL: true,
  },
  (err, res, body) => {
    if (err) return console.log(err);
    console.log(body);
  },
);
