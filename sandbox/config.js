import axios from 'axios';
import { prop } from 'ramda';

// const run = () => axios.get('http://localhost:7200/api/config?tenant=urox').then(prop('data'));
// const run = () => axios.post('http://localhost:7200/api/config?tenant=urox', { facets: {datasourceId: ['qa:stable'] } }).then(prop('data'));
const run = () => axios.post('http://localhost:7200/api/config?tenant=urox', {}).then(prop('data'));
run().then(console.log);
