module.exports = {
  env: {
    browser: false,
    jest: true,
    node: true,
    es6: true,
  },
  settings: {},
  extends: ['eslint:recommended'],
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
    },
    sourceType: 'module',
  },
  plugins: ['prettier', 'import'],
  rules: {
    'no-console': 'warn',
    'no-unused-vars': 'warn',
    'react/display-name': 'off',
  },
};
